# Descripción

Este repositorio engloba el contenido del Trabajo Fin de Grado «Razonamiento espacio-temporal basado en sentido común para la Smart City». Se trata de un proyecto dedicado a la implementación de mecanismos que dan soporte al razonamiento del espacio y del tiempo dentro de una Smart City. 

# Estructura

Su contenido se encuentra dividido en dos carpetas principalmente. La primera de ellas, denominada «doc», almacena el contenido del anteproyecto y de la memoria de este TFG.

Por otro lado se encuentra la carpeta que contiene todos los archivos fuente, llamada «source», clasificados a su vez en subcarpetas según al módulo al que pertenecen. 

Dentro de «source» se encuentran las siguientes carpetas.

- Carpeta OSM: contiene los archivos referentes al tratamiento de datos de OpenStreetMap.

- Carpeta Scone: almacena toda la información relacionada con la base de conocimiento Scone. Se pueden ver los distintos archivos .lisp que modelan la ciudad. 

- Carpeta icegridServers: esta a su vez se divide en otras subcarpetas, una para cada servicio creado dentro de Civitas. Por un lado se encuentra la carpeta que contiene el servicio que realiza el seguimiento del vehículo («vehicleTracker»), por otro la carpeta que contiene el servicio que inicia la búsqueda de la siguiente cámara que debe grabar («getNextCameraClient»).

# Ejecución 

La ejecución del de la memoria se lleva a cabo a través del archivo Makefile. 

```shell
$ make
```

Para ejecutar los archivos de OSM simplemente hay que introducir el siguiente comando:

```shell
$ python osm1.py -all
$ python osm2.py -all
```