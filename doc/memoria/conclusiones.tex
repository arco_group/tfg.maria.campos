\chapter{Conclusiones y propuestas}

\drop{E}{l} presente capítulo resume las conclusiones más relevantes
derivadas de la realización de este \acs{TFG} así como las líneas de trabajo
futuras que se abren a partir de aquí. Este capítulo se encuentra por
lo tanto dividido en dos partes. La primera de ellas se centra de
recoger las conclusiones obtenidas organizadas en base a las tres
áreas que han sido abordadas para la realización de este trabajo,
presentando el alcance logrado en base a los objetivos iniciales
planteados. La segunda parte expone varias propuestas para la
ampliación y mejora del sistema, derivadas de la experiencia y los
conocimientos adquiridos durante la realización de este
proyecto. Estas propuestas están principalmente orientadas a mejorar o
agregar nuevas funcionalidades.

\section{Conclusiones}

En esta sección se expone detalladamente las conclusiones obtenidas
tras la realización del proyecto. Teniendo en cuenta que este proyecto
abordaba tres áreas bien diferenciadas, a saber el modelado de
conocimiento, sistemas de razonamiento y sistemas distribuidos, las
conclusiones se presentarán organizadas en estos tres grandes
bloques. De manera más específica, cada una de estas áreas abordaba
objetivos específicos expuestos al inicio del proyecto (ver sección
~\ref{chap:objetivos}). A continuación se explica cada uno de estos
apartados.

\subsection{Modelado de conocimiento}
Este proyecto partía de la necesidad de modelar el conocimiento
relacionado con la Smart City en general y, con una ciudad específica
en concreto (en este caso Ciudad Real). Por lo tanto, uno de los
principales objetivos era no sólo identificar una ontología que
hubiera formalizado este conocimiento sino hacer que ese conocimiento
estuviera disponible en el sistema elegido para el desarrollo de este
proyecto. Así, podemos concluir que:

\begin{itemize}
\item El modelado del conocimiento del dominio de la Smart City ha
  sido llevado a cabo a partir de la formalización propuesta por SOFIA
  y utilizando la herramienta Scone. En este sentido, la herramienta
  Scone ha demostrado ser versátil, flexible, expresiva y potente a la
  hora de generar respuestas a las preguntas planteadas por el
  sistema.
\item El modelado del conocimiento específico de una ciudad concreta,
  en este caso Ciudad Real, también ha sido satisfactoriamente
  abordado con Scone.
\item En base al servicio ofrecido durante la realización del
  proyecto, se puede concluir que la utilización de esta herramienta
  puede ofrecer un gran soporte en lo que respecta a la realización de
  tareas de inferencia y a la búsqueda de información debido al gran
  potencial que posee.
\end{itemize}

\subsection{Sistema de razonamiento}
La tarea de modelado de conocimiento sólo sentaba las bases para que
posteriormente, tras el desarrollo de un mecanismo de búsquedas
avanzado, pudiera derivarse información que aunque implícita en la
información proporcionada a la base de conocimiento no se hubiera
dicho expresamente. Así, en este bloque de trabajo, lo que se
perseguía en líneas generales era derivar nueva información a partir
de la ya existente en la base de conocimiento, de lo cual puede concluirse que:

\begin{itemize}
\item En este proyecto se ha demostrado como Scone, a partir del
  conocimiento que contiene, es capaz de realizar tareas de herencia
  de propiedades o aplicar relaciones transitivas del tipo «Si A es
  adyacente de B, B es adyacente de A». El nuevo tipo de conocimiento
  generado no se encuentra de manera explícita en Scone, sino que es
  deducido por el mismo. Esto demuestra el gran potencial de Scone a
  la hora de inferir conocimiento que a priori no ha sido establecido
  en la base de conocimiento.
\item Una vez lanzado Scone, esta herramienta muestra la cantidad de
  nodos que contiene cargados y que se encuentran disponibles (ver
  Figura~\ref{figs/numNodos}). Cuando el sistema comienza a realizar peticiones a Scone, este genera la
  respuesta en un breve instante de tiempo, demostrando que la
  respuesta generada no se ve afectada por el tamaño de la base de
  conocimiento. Scone prueba que la cantidad de nodos que alberga no
  resulta un impedimento a la hora de procesar y contestar las
  peticiones de manera eficiente gracias al algoritmo de paso de
  marcadores implementado para realizar búsquedas. Este algoritmo sólo
  se ve afectado por la profundidad del árbol (niveles de jerarquía)
  que de media no suele superar los 4 niveles.
  
\begin{figure}[!h]
\begin{center}
\includegraphics[width=0.8\textwidth]{numNodos.png}
\caption{Ejemplo de la carga de nodos de algunos archivos en la \acs{KB} Scone}
\label{figs/numNodos}
\end{center}
\end{figure}
  
\item Uno de los riesgos que, previamente a la realización de este
  proyecto, se había contemplado era la posibilidad de que el
  despliegue del servidor Scone como servicio distribuido pudiera
  suponer un cuello de botella en la ejecución del sistema. El
  principal problema si esto hubiera sido así estaba a la hora de
  realizar peticiones al servidor distribuido de Scone, pero aún así
  éste ha demostrado ser sólido tanto en la generación de una
  respuesta en tiempo real como en la validación del sistema
  realizado.
\item El servidor de Scone ha sido desplegado como un servicio
  distribuido ICE. Sin embargo hay que tener en cuenta que el servidor
  Scone está basado en el uso de sockets por lo que fueron necesarios
  algunos ajustes para poder desplegar éste sobre el middleware de
  comunicaciones ICE. Así, se desarrollo un \textit{Wrapper} para la
  traducción de invocaciones ICE en llamadas a sockets. Este
  desarrollo no presenta ningún problema aparentemente a pesar de que
  el número de peticiones invocadas eran numerosas y de gran
  complejidad. En cualquier caso, si el número de coches en
  seguimiento aumentara considerablemente y resultara un problema,
  estos servicios podrían emplear la técnica de replicación de nodos
  para solventarlo, ya que se trata de servicios de IceGrid.
\end{itemize}

\subsection{Sistema distribuido}
Este proyecto se plantea en el contexto de la Smart City y, por lo
tanto, debe asegurar que el sistema desarrollado pueda desplegarse en
una infraestructura de dónde obtener información y a la que volcar los
resultados obtenidos tras el procesamiento de la misma. En ese
sentido, podemos concluir que:


\begin{itemize}
\item Todos los servicios desarrollados durante este \acs{TFG} se encuentran
  desplegados sobre el middleware ICE, lo que da lugar a que el
  proyecto en general pueda ser visto como un verdadero servicio
  inteligente dentro del ámbito de la Smart City. Los servicios
  realizados interactúan por medio del middleware con los distintos
  servicios distribuidos en la ciudad inteligente. Así, el sistema
  desarrollado consume información de servicios como las pisaderas o
  los semáforos y actúan sobre ella mediante la activación y
  desactivación de las cámaras donde el flujo de vídeo debe esperarse.
\item El sistema de validación del proyecto se encuentra desarrollado
  sobre un escenario realista, simulado mediante la herramienta
  Blender\footnote{Esta simulación ha sido desarrollada por el grupo
    de investigación Arco.}, el cual ha permitido validar el sistema
  bajo las mismas condiciones que se pueden dar en un entorno real.
  Al estar trabajando sobre un sistema distribuido, las conclusiones
  obtenidas en la simulación del escenario son directamente
  extrapolables a un entorno real. Por lo tanto, a efectos del sistema
  a desarrollar, para éste será transparente que la fuente de eventos
  sea un objeto del mundo real u objetos del entorno de virtualización
  de la Smart City que se ejecuta sobre Blender.
\end{itemize}

\subsection{Sistema de validación}
Finalmente, aunque el objetivo principal del proyecto era el
desarrollo de un algoritmo de razonamiento espacio-temporal para la
Smart City es cierto que dicho objetivo no podía ser validado si no
era a través del desarrollo de un caso de uso concreto en el que
fueran necesarias capacidades de este tipo. Así, se planteo el
escenario del seguimiento automático de vehículos como sistema de
validación, concluyéndose lo siguiente:

\begin{itemize}
\item La validación del sistema desarrollado ha consistido en el
  trazado de varias rutas en el callejero de Ciudad Real, obviando
  aquellos casos donde se presentan fallos en el plano de
  OpenStreetMap. Este sistema de validación ha desarrollado una
  búsqueda lógica y razonable donde, a partir del último punto en el
  que el vehículo fue visualizado, activa las posibles cámaras donde
  se puede encontrar el vehículo al cual se le realiza el
  vídeoseguimiento. Esta operación de activación de cámaras se realiza
  cada vez que ha transcurrido un cierto espacio de tiempo sin haber
  recibido notificación por parte del canal de eventos en el que el
  sistema de reconocimiento de matrículas publica el número de
  matrícula reconocida del vídeo que las cámaras activas le están
  proporcionando.\\\\
\end{itemize}


Por último, es importante mencionar que sobre este proyecto se ha llevado a cabo la redacción de un artículo denominado \textit{Implementing a holistic approach for the Smart City}. Dicho artículo se centra en proporcionar soluciones para poder llevar a cabo la integración de nuevos servicios y dispositivos en las Smart Cities de manera sencilla. Propone el empleo de una plataforma middleware que de lugar a la abstracción entre los distintos estándares y protocolos que pueden darse en la Smart City para que las comunicaciones sean llevadas a cabo de manera independiente a las tecnologías empleadas, mediante la realización de un instanciación de los servicios. Este artículo se encuentra aceptado para su publicación en \textit{The 2014 IEEE/WIC/ACM International Conference on Intelligent Agent Technology} celebrado en Varsovia del 11 al 14 de Agosto de 2014\cite{articulo}.

\section{Propuestas}

Tras haber concluido el desarrollo del sistema propuesto al comienzo
de este trabajo, consideramos que son varias las líneas que se abren
para profundizar en este área tan amplia como es la del razonamiento
espacio-temporal. Además, el impacto de este área en la Smart City ha
quedado patente con la realización de este trabajo. Enriquecer la
ciudad con capacidad de razonar sobre como la relación espacio-tiempo
afecta a determinados agentes de la ciudad inteligente puede ser la
base sobre la que construir servicios más elaborados e inteligentes y
por extensión, servicios más útiles para la ciudadanía en su conjunto.

Esta sección por lo tanto describe una serie de ideas con las que se
podrían realizar mejoras en el sistema actual. A pesar de que el
desarrollo del proyecto ha cumplido con los objetivos marcados al
comienzo y ha dado lugar a los resultados esperados, este sistema es
susceptible de ser mejorado para incorporar nuevas funcionalidades o
mejoras respecto al sistema actual. A continuación se muestra una
breve lista con las posibles mejoras que probablemente sean llevadas a
cabo en el Trabajo Fin de Máster.

\begin{enumerate}
\item Realización de una comparación del sistema actual con otros
  razonadores basados en ontologías. Algunos de los razonadores con
  los que se podría realizar la comparación son
  «RacerPro\footnote{\url{http://franz.com/agraph/racer/}}»,
  «Pellet\footnote{\url{http://clarkparsia.com/pellet/}}» o
  «Hermit\footnote{\url{http://www.hermit-reasoner.com/}}».
\item En el sistema actual no se considera la ruta anterior que venía
  siguiendo el coche al cual se le está realizando el
  vídeoseguimiento. La consideración de esta ruta permitiría acotar
  más aún el algoritmo de búsqueda que proporciona la solución.
\item Realizar una retroalimentación visual marcando en el mapa las
  líneas de dirección probables a las que se puede dirigir el vehículo
  en seguimiento.
\item Aplicar el sistema actual a otro tipo de agentes móviles del
  ámbito de la ciudad, no sólo a coches, sino también a personas,
  bicicletas, etc. La base del sistema sería la misma, los cambios se
  encontrarían en los distintos matices que se deben aplicar a cada
  tipo de agente dependiendo de las restricciones que ese conjunto de
  agentes móviles.
\item Aplicación de razonamientos más elaborados en base a situaciones
  donde sólo la mente puede inferir correctamente ante la nueva
  situación. Por ejemplo, los mecanismos de razonamiento que dan lugar
  a la inferencia por analogía, por defecto, etc.
\end{enumerate}



\chapter{Conclusions}

\drop{T}{he} present chapter  summarizes the most relevant conclusions
derived from  the realization of this  Finaly Year Project  as well as
future  work that  are opened  from here.   This chapter  is therefore
divided  into  two  parts.    The  first  one  focuses  on  collecting
conclusions  organized on  the basis  of  three areas  that have  been
approached for  the accomplishment of this work,  presenting the range
achieved on  the basis of  the initial objectives raised.   The second
part presents  future work  for the extension  and improvement  of the
system,  from the  experience and  the knowledge  acquired  during the
course  of this  project.  These  proposals are  mainly  orientated to
improve and add new functionalities to the system.


\section{Conclusions}

This section  presents, in detail, the conclusions  obtained after the
completion of  the project.  Given  that this project  addressed three
well-differentiated areas,  namely modeling knowledge,  reasoning, and
distributed systems,  the conclusions  will appear organized  in three
main  sections.   More specifically,  each  of  these areas  addressed
specific  set of  objectives  at  the beginning  of  the project  (see
section~\ref{chap:objetivos}). Later on  this chapter, the conclusions
drawn from these achivevemtns will be explained.

\subsection{Knowledge modeling}

This project started  from the need to model  the knowledge related to
the Smart City  in general and, with a specific city  in mind (in this
case Ciudad  Real). Therefore, one of  the main goals was  not only to
identify an  ontology that had  formalized this knowledge but  also to
make that knowledge  available to the chosen system.  We can therefore
conclude that:

\begin{itemize}
\item Smart City domain knowledge modeling has been conducted from the
  formalization proposed  by SOFIA and based  on the use  of the Scone
  tool.  In  this  regard,  the  Scone tool  has  demonstrated  to  be
  versatile,  flexible,  expressive  and  powerful at  the  moment  of
  generating answers to the questions raised by the system.
\item The modeling of the  specific knowledge of a particular city, in
  this case Ciudad Real, also it has been satisfactorily approached by
  Scone.
\item Based on the service  offered during the project, it is possible
  to conclude  that the use of  this tool can provide  a great support
  when it  comes to  performing inference tasks  and to the  search of
  information due to the potential that it possesses.
\end{itemize}

\subsection{Reasoning system}

The task  of knowledge modeling was  only inteded to  ground the bases
for the  development of an  advanced search mechanism.   This advanced
mechanism should be capable  of deriving information from the implicit
information  held in  the knowledge  base and  expressly  stated. This
block of work is especifically  devoted to derive new information from
the knowledge already existing in the knowledge base. It can therefore
be concluded that:

\begin{itemize}
\item  This project has  demonstrated that  Scone, from  the knowledge
  that  it contains,  it is  capable of  performing tasks  of property
  inheritance or  applying transitive relations  of the type "If  A is
  adjacent  to B,  B is  adjacent  to A".  The new  type of  generated
  knowledge is not explicit in Scone, but it has been deduced from the
  existing one.  This demonstrates  the Scone's great potential at the
  moment of  inferring knowledge that  \textit{a priori} has  not been
  established in the  knowledge base.
  
\item Once Scone has been started,  this tool shows that the number of
  nodes  loaded into the  system (see Figure ~\ref{figs/numNodos}).  When  the system  starts triggerint
  queries  to Scone,  it has  been noticed  that Scone  provides quick
  answers,  demonstrating that  the  generated response  does not  get
  affected by  the size of the  knowledge base. Scone  proves that the
  quantity  of nodes  does not  turn out  to be  an impediment  at the
  moment  of  processing  and   answering  the  stated  queries.  This
  efficiency  in answering  is  due to  the  marker passing  algorithm
  implemented for  searching. This algorithm  is only affected  by the
  depth of the  tree (number of levels in  the hierarchy) that usually
  does not exceed 4 levels.
\item  One of the  risks that,  before to  the accomplishment  of this
  project,  had  been  contemplated   was  the  possibility  that  the
  deployment of the Scone server as distributed service could reveal a
  bottleneck in the execution of the system. The main problem, if this
  had  been occurred,  would have  been  at the  moment of  triggering
  queries to the  Scone server. However, the implementation  of a case
  scenario  has demonstrated  to  be  robust in  the  generation of  a
  response in real-time.
\item Scone  server has been  deployed as an ICE  distributed service.
  However, it cannot be obviated  that the Scone server is implemented
  using sockets so some adjustments in order to its deploymentt on the
  ICE  middleware  platform  were  needed.  A  \textit  {Wrapper}  was
  developed  in  the  context  of  the Arco  research  group  for  the
  translation of ICE invocations  into socket calls. This wrapper does
  not apparently  present any problem  since the number  of invocation
  requests were numerous and of  great complexity. In any case, if the
  number of cars on track will considerably increase and it turns into
  a problem, these services  could employ the technique of nod replication
  tackle the problem since it has been deployed as an IceGrid service.
\end{itemize}

\subsection{Distributed system}

This  project  takes place  in  the context  of  the  Smart City  and,
therefore,  it must  be  assured  that the  proposed  solution can  be
deployed upon an infrastructure from where information can be obtained
and to results applied.  In this sense, it can be concluded that:

\begin{itemize}
\item  All  services developed  during  this  Final  Year Project  are
  deployed on  the ICE middleware,  which supports the  integration of
  the  proposed service as  one more  of a  Smart City  platform.  The
  developed system  consumes information of services  as the footrests
  or  traffic lights  and act  on it  by means  of the  activation and
  deactivation  of  the cameras  where  the  video  stream expects  to
  capture the tracked car.
\item The validation system for  this project has been deployed on top
  of  a realistic  scenario simulated  by Blender  tool\footnote {This
    simulation has been developed by the group of investigation Arco},
  which  has supported  the validation  of the  system under  the same
  conditions as though it had been given in a real environment.  Since
  the  propose  service  is  working  on  a  distributed  system,  the
  conclusions obtained in the simulation  of the scene can be directly
  extrapolated  to a  real  environment.  Therefore,  from the  system
  perspective it  will be  transparent than the  source of  events are
  objects of the real world  or objects of the virtualised environment
  of the Smart City run on Blender.
\end{itemize}

\subsection{Validation system}

Finally, although the main objective of the project was the develop an
algorithm for spatio-time reasoning for the Smart City it is also true
that this objective  could not be validated if it  was not through the
development of a  concrete case scenario in which  they were necessary
capacities  of  this type.  Therefore,  the  considered case  scenario
undertakes automatic vehicle tracking concluding that:

\begin{itemize}
\item The validation of the  developed system has consisted in tracing
  several  routes on the  street directory  of Ciudad  Real, obviating
  those   cases  where  they   present  failures   in  the   plane  of
  OpenStreetMap. This  validation system  has developed a  logical and
  rational search where, from the  last point in which the vehicle was
  visualized, possible  cameras are enabled where the  car is expected
  to be.  This camera  activation operation takes  place every  time a
  period of time has elapsed without having received notification from
  the event  channel where the plate recognition  system publishes the
  number of  plates being  recognized from the  analyzed video  of the
  activated cameras.\\\\
\end{itemize}

Finally, it is important to mention that this project was carried out writing an article called \textit{Implementing a holistic approach for the Smart City}. This article centres on providing solutions to be able to carry out the integration of new services and devices in the Smart Cities of a simple way. Proposes the employment of a middleware platform that of place to the abstraction between the different standards and protocols that can be given in the Smart City in order that the communications are carried out in an independent way to the used technologies, by conducting a instantiation of services. This article is accepted for his publication in \textit{The 2014 IEEE/WIC/ACM International Conference on Intelligent Agent Technology} celebrated in Warsaw from 11th to 14th August, 2014\cite{articulo}.


% LocalWords:  virtualised
