\chapter{Introducción}
\drop{L}{as} ciudades son plataformas donde los ciudadanos desarrollan
su actividad, a través de los múltiples servicios que esta ofrece y
accediendo para ello a una gran cantidad de recursos. Actualmente las
ciudades están experimentando un gran aumento en el número de
habitantes, produciéndose una reducción considerable en la población
de las zonas rurales.

La creciente demanda que se está produciendo ha dado lugar a que
ciertas entidades públicas se replanteen un nuevo modelo de ciudad, un
modelo que gestione dicho aumento proporcionando eficiencia,
desarrollo sostenible, calidad de vida y una correcta gestión de los
recursos disponibles. Esto ha dado lugar a la aparición de un nuevo
concepto, el de Smart City\footnote{En este documento se empleará
  indistintamente los términos \textit{Ciudad Inteligente} y
  \textit{Smart City}.}, la cual supone la integración de \acf{TIC} en la ciudad y en su relación
con los ciudadanos.

Se conoce como ciudad inteligente a aquella que persigue no sólo la
mejora de la calidad de vida de sus ciudadanos, sino que también
pretende un progreso en el ámbito social, económico y empresarial,
articulado todo ello sobre la base de las \acs{TIC}, el capital social
y el medio ambiente.

Una Smart City consigue que tanto su infraestructura, como los
servicios que ofrece resulten más eficientes e interactivos hacia el
ciudadano. La combinación de las distintas tecnologías que presenta
dan lugar a la gestión automática y eficiente de sus servicios e
infraestructuras, produciéndose una reducción del impacto ambiental,
así como una mejora de la calidad de vida de los ciudadanos, los
cuales son dotados con mayor información para poder tomar decisiones
más inteligentes.

El proyecto de Smart City abarca diversos ámbitos de una ciudad, como
pueden ser la movilidad urbana, la gestión energética o la seguridad
pública, con el fin de construir una excelente plataforma innovadora
proveedora de servicios. Sin embargo, todo este desafío no sólo
plantea retos técnicos o tecnológicos, sino que conlleva un cambio
organizacional tanto en los gobiernos como en la sociedad en general.

Según el informe de la \acf{IDC}\cite{IDC}, el concepto de Smart City
crea la necesidad de definir un nuevo modelo de ciudad en el que se
rediseñen algunos componentes típicos de la misma y se añadan otros
conceptos nuevos con la motivación siempre presente de alcanzar una
gestión más sostenible. Este nuevo paradigma implica por lo tanto una
transformación múltiple requiriendo desde cambios en el propio diseño
de nuevos edificios hasta cambios en el ámbito del gobierno.

El sistema complejo que representa una ciudad inteligente se encuentra
formado por distintos agentes y por múltiples procesos estrechamente
ligados. A la hora de crear una Smart City, se parte de una
infraestructura constituida por tecnologías y servicios básicos
proporcionados por una entidad pública, generalmente un
Ayuntamiento. Una vez formada la infraestructura básica, la
construcción de nuevos servicios por parte de distintos organismos
(tanto públicos como privados) irán dando forma a una ciudad cada vez
más inteligente.

El hecho de disponer de una ciudad inteligente posibilita una gestión
más eficiente y automática de las infraestructuras y tecnologías que
esta posee, aportando la ventaja de mejorar los servicios que
contiene.  El desarrollo de una Smart City constituye la construcción
de un entorno que fomenta el empleo de servicios avanzados que
resuelvan los problemas actuales, así como los futuros.

Sin embargo, el paradigma propuesto por la Smart City no se fundamenta
únicamente en el uso de nuevos dispositivos tecnológicos, sino que su
núcleo central se basa en la información que dichos dispositivos
puedan proporcionar y en cómo se trate dicha información. Los
distintos sensores desplegados por toda la ciudad son los encargados
de recopilar la información del entorno que les rodea para
posteriormente almacenarla y analizarla, con el fin de crear servicios
inteligentes en la ciudad.

Se puede definir por tanto la Smart City como un sistema de
conocimiento, cuyas fuentes de información serán todos y cada uno de
los sensores o dispositivos desplegados a lo largo de la ciudad. Por
medio de estos, se consigue «escuchar» y «comprender» a la ciudad,
dando lugar a la toma de decisiones más «inteligentes», para
así poder ofrecer la información y los servicios adecuados a sus
habitantes.

Es por ello que para abordar el tema de la Smart City surge, en primer
lugar, la necesidad de identificar los conceptos que componen este
dominio así como las relaciones entre ellos, para dotarlos de una
semántica común y conocida de antemano para cualquier entidad que
quiera desarrollar o utilizar un servicio de la Smart City. En segundo
lugar, surge también la necesidad de establecer un vocabulario común
que estandarice la manera en la que los distintos servicios de la
Smart City accederán a la información disponible en la misma.

Ya que el uso de un único lenguaje de programación no es posible, se
hace necesario ascender en el nivel de abstracción para proveer a los
conceptos manejados con el suficiente contenido semántico para poder
ser entendido, tratado y utilizado por cualquier parte que quiera
integrarse en esa infraestructura genérica que es la Smart City.

La necesidad de modelar información semántica en la Smart City, o en
general en cualquier otro ámbito en el que se pretenda automatizar la
fase de generación respuestas a determinados eventos, surge por el
creciente número de aplicaciones que hacen uso de los mismos datos, de
distinta manera y con distintas finalidades. El objetivo que se desea
alcanzar en la Smart City consiste en gestionar los múltiples datos
desectruturados que genera una ciudad, los cuales en su mayoría, no
siguen estándares y suelen poseer formatos de difícil manejo.

Estas labores de estandarización del vocabulario y etiquetado
semántico se realizan tradicionalmente a través de la definición de
una ontología, en este caso una ontología para Smart City.  El empleo
de una ontología dentro de la ciudad inteligente realiza una
especificación formal de los objetos en base a conceptos y relaciones
que contiene, es decir, establece un vocabulario común para los
ciudadanos, servicios y tecnologías que se encuentran dentro de la
Smart City.

La ontología elegida para este proyecto es SOFIA, una ontología para
ciudades inteligentes escrita en el lenguaje \acf{OWL}. La elección de
SOFIA se debe a la completitud que presenta dentro del ámbito de las
ciudades inteligentes, ya que por medio de ella es posible conseguir
la interoperabilidad semántica entre Smart Cities. A través de SOFIA,
los servicios que ofrece la ciudad inteligente adquieren notables
mejoras, tanto por la posesión de un conocimiento más detallado, como
por una mejor comunicación e integración entre los distintos servicios
que contiene. Para poder trabajar con SOFIA, es necesario el empleo de
una herramienta que recoja los distintos conceptos y relaciones que
contiene. Protégé es en este caso la herramienta seleccionada para
poder analizar y trabajar con la ontología.

Sin embargo, la estandarización o etiquetado semántico no bastan para
soportar tareas de deducción o inferencia de conocimientos implícitos
en el término «Smart». Para poder hablar de Smart City, es necesario
hacer un uso más elaborado de la información que se recoge de la
ciudad, para que una vez procesada se puedan tomar decisiones
inteligentes que verdaderamente den la impresión de que la ciudad en
sí misma está razonando, como haría una persona.

Hablar de un verdadero razonamiento semántico implica tener la
capacidad de poder inferir nuevo conocimiento que explícitamente no se
haya descrito anteriormente. El uso de ontologías es útil y
proporciona una herramienta semánticamente superior a lo que una base
de datos podría ofrecer. Sin embargo, a la hora de describir el
contexto de ciudad inteligente no basta sólo con el empleo de la
lógica de primer orden. SOFIA dota de una semántica común a la Smart
City, pero no tiene en cuenta ciertas cuestiones que para el ser
humano resultan lógicas y evidentes.

Usar únicamente una ontología resulta insuficiente a la hora de
modelar la semántica implícita en contextos abiertos como los de la
Smart City, donde no siempre es posible encontrar la solución más
óptima a la cuestión planteada ya que puede que esta no exista. Por el
contrario, sí que el sistema de razonamiento o búsqueda avanzada debe
ser capaz de dar una respuesta apropiada, o la menos mala, en un
tiempo asumible si tenemos en cuenta que este tipo de sistemas (Smart
Cities) deben trabajar bajo condiciones de tiempo real. Así, el
énfasis no debe estar tanto en encontrar la mejor respuesta, ya que
puede que esto suponga no poder encontrarla a tiempo, como en dar una
respuesta apropiada dentro de unos márgenes aceptables de tiempo
transcurrido.

Debido a que el empleo de una ontología no es suficiente, se requiere
que los elementos que intervienen en la Smart City sean dotados de un
mayor contenido semántico, para poder habilitar tareas de razonamiento
lo más cercano posible a los distintos mecanismos de razonamiento
humano. Así, el sentido común es una de las características más
diferenciadora del ser humano, que básicamente se puede definir como
el conocimiento que poseen las personas y que describe cómo funciona
el mundo.

El razonamiento de sentido común es fundamental a la hora de dotar con
inteligencia a los distintos agentes y servicios contenidos en la
ciudad, considerándose necesario a la hora de construir sistemas
verdaderamente inteligentes. Este tipo de razonamiento, el cual es
adquirido por las personas desde la infancia de forma inconsciente,
permite analizar una situación en función del contexto en el que se
encuentra y elegir la opción más apropiada dependiendo de la función a
realizar.

En este sentido, hablar de razonamiento basado en sentido común
implica dar soporte a numerosas cuestiones (acciones y eventos,
efectos de esos eventos, modelos mentales, tiempo, espacio, etc. ). De
entre todas estas cuestiones, las del tiempo y el espacio son
fundamentales para articular el razonamiento en la Smart City y por
eso este proyecto aborda la cuestión del razonamiento
espacio-temporal.

Ambos conceptos se encuentran estrechamente ligados ya que cualquier
objeto se encuentra situado en un espacio determinado en un instante
de tiempo concreto. Este tipo de razonamiento resulta esencial para
que cualquier persona pueda comprender lo que sucede en su
entorno. Una persona percibe las entidades del contexto en el que se
encuentra y los movimientos y cambios que producen, pero no es
consciente de la relación y razonamiento espacio-temporal que está
realizando.

En este proyecto, el razonamiento espacio-temporal es abordado
inicialmente proporcionando la información necesaria, a través de los
conceptos y relaciones descritas en un ontología así como de la
información contenida en mapas (callejeros), para que después, sobre
esa misma información, se puedan implementar mecanismos de
razonamiento que hagan un uso más elaborado de dicho conocimiento.

La implementación de razonamiento espacio-temporal en una ciudad
requiere, por lo tanto, no sólo conocimiento de sentido común acerca
del comportamiento típico de los agentes presentes en este contexto,
sino también información precisa sobre cómo está distribuido el
espacio en cada contexto concreto, entendiendo por contexto aquí el de
una ciudad concreta. Hay que resaltar aquí que, aunque este proyecto
se ha elaborado sobre la información geolocalizada de Ciudad Real, la
solución que se plantea es independiente de cada caso
concreto, es decir, no consiste en un etiquetado manual de las
distintas vías presentes en una ciudad, sino que se ofrecerá un
mecanismo que se encargará de recopilar esa información
automáticamente para después proporcionársela a la base de
conocimiento.

La distribución de la ciudad es obtenida a través de \acf{OSM}, un
sistema que permite trabajar con los mapas que posee sin
restricciones. Se encarga de ofrecer información tanto de la
distribución de los distintos tipos de vía que se encuentran en una
ciudad, como cualquier otro tipo de información espacial relevante. La
información espacial de una ciudad concreta ofrecida por \acs{OSM} es
representada semánticamente para, posteriormente, poder ser almacenada
en una base de conocimiento.

La información obtenida de la ontología SOFIA junto con la información
espacial que ofrece el sistema OpenStreetMap constituyen la base sobre
la que se realiza el razonamiento espacio-temporal para la Smart City.

Respecto a la base de conocimiento utilizada, este proyecto hace uso
de Scone\cite{Sco10}. El motor de Scone da soporte a tareas simples de
inferencia sobre elementos y sentencias de su base de
conocimiento, ofreciendo además la ventaja de manejar razonamientos de
lógica de orden superior necesarios en una ciudad inteligente. Las
respuestas que proporciona son ofrecidas en tiempo real a los
distintos algoritmos de razonamiento desplegados en la Smart City.

El motor de inferencia Scone permite almacenar el conocimiento de
mundo real, en este caso obtenido por medio de la ontología y de la
distribución de la ciudad con \acs{OSM}, para posteriormente realizar
inferencias sobre dicho conocimiento a través de algoritmos basados en
paso de marcadores. El empleo de Scone da lugar a un conocimiento de
más alto nivel.

En la base de conocimiento Scone, con la información almacenada y
utilizando un lenguaje de programación basado en LISP, se lleva a cabo
la implementación de mecanismos de razonamiento más elaborados. Tanto
la ontología como la información obtenida de OpenStreetMap se traducen
a a este lenguaje basado en LISP y se almacenan en la base de
conocimiento Scone. Una vez se encuentra ahí, se lleva a cabo la
ejecución de algoritmos de razonamiento, los cuales se encargan de dar
soporte a las funciones de toma de decisiones en las que intervenga
factores espacio-temporales.

El objetivo principal de este proyecto es proponer un sistema para la
toma de decisiones en las que intervengan elementos
espacio-temporales. Para ello, se propone el diseño de un servicio
para la Smart City destinado al vídeo-seguimiento de coches. Con este
sistema se pretende reducir el número de cámaras cuyo vídeo se está
analizando concurrentemente en busca de la matrícula identificativa
del vehículo en seguimiento. La reducción de este número es posible en
base al análisis espacio-temporal de las posibles vías que ha podido
seguir un coche, en base a las direcciones de las calles y la
velocidad máxima de la vía. Así una vez identificado el coche cuyo
seguimiento se pretender realizar, el servicio propuesto en este
proyecto guiará en la selección de cámaras cuyo flujo de vídeo debe
ser enviado al algoritmo de reconocimiento de matrículas, reduciendo
así la carga computacional del sistema que sólo analizará flujos de
vídeos en los que potencialmente debería encontrarse el coche.

La búsqueda de las cámaras se implementará utilizando ciertos
conocimientos y restricciones espacio-temporales. Cada cámara tiene un
límite que determina el rango en el que puede grabar, si el coche no
se encuentra dentro de dicho rango, por medio del razonamiento de
sentido común se llega a la conclusión de que hay que buscar otra
cámara que localice al coche para poder seguir grabándolo. Al disponer
de información contextual almacenada en la base de conocimiento (esta
es proporcionada por SOFIA y por \acs{OSM}), se puede llevar a cabo la
implementación de un algoritmo que detecte las cámaras cercanas a la
actual. A dicho algoritmo se le puede aplicar un razonamiento
espacio-temporal que ofrezca la solución de la cámara que debe seguir
grabando. De esta manera es posible averiguar la trayectoria del
coche, pudiendo realizar un seguimiento de este.

Además, para poder hablar de un verdadero servicio de la Smart City es
necesario que el mismo satisfaga una serie de requisitos. Así, este
proyecto se encuentra enmarcado dentro de la plataforma
Civitas. Esta plataforma representa un entorno heterogéneo
formado por distintos servicios que se adaptan a la Smart
City. Civitas se despliega de forma distribuida por toda la ciudad,
recogiendo los valores que esta genera por medio de sensores, en
tiempo real.  Los valores obtenidos, son almacenados en la base de
conocimiento de Scone, siendo esta la encargada de realizar
inferencias que parten de un conocimiento general y que concluyen en
conocimiento específico para los distintos servicios de la Smart
City. Las deducciones que genera Scone dan lugar a completar el
entorno de la ciudad con conocimientos más complejos y elaborados, es
decir, dota con inteligencia al entorno Civitas.

Las ciudades inteligentes se componen de servicios caracterizados
principalmente por su pervasividad y heterogeneidad, que además,
generalmente han sido realizados por distintos desarrolladores
empleando diversas tecnologías. Para resolver este problema surge la
necesidad de emplear un middleware de comunicaciones, para abstraer
así al programador de servicios de todas estas cuestiones. En este
caso, Civitas, utiliza el middleware ICE de ZeroC, que se encarga de
proporcionar un conjunto de servicios que comunican las distintas
aplicaciones distribuidas encontradas en la plataforma heterogénea.

La diversidad de servicios y tecnologías que se encuentran en una
ciudad inteligente es uno de los problemas a tratar, debido a que una
ciudad no se puede considerar verdaderamente inteligente si los
servicios que ofrece no se pueden comunicar, ya que de otra forma, se
estarían creando entornos fragmentados. El empleo de un middleware en
este proyecto hace que se produzca una abstracción de la diversidad
que se encuentra en la ciudad, pudiendo conseguir la información
necesaria y pudiéndola integrar en las distintas tecnologías.

El hecho de que ZeroC ICE se encuentre en Civitas hace que la
plataforma ofrezca servicios distribuidos fáciles de gestionar y
comunicar, donde la información que transmiten y obtienen sigue un
formato de datos común. La estandarización del sistema se lleva a cabo
aplicando a la información de la Smart City unas características
semánticas concretas, en este caso, utilizando el modelo semántico de
Civitas.

De este modo, crear un servicio en Civitas es crear un servicio en
ICE, que cumple el modelo semántico de Civitas. Por lo tanto, el
servicio realizado para el seguimiento de coches ha sido realizado
empleando dicho modelo semántico, permitiendo así su interoperabilidad
e incluso versatilidad para poder ser utilizado en servicios más
complejos.


% Local Variables:
%  coding: utf-8
%  mode: latex
%  mode: flyspell
%  ispell-local-dictionary: "castellano8"
% End:
