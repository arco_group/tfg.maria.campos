\chapter{Método y Fases de Trabajo}
\label{chap:fases}


\drop{E}{n} este capítulo  se describe el método de  trabajo llevado a
cabo  para  la   realización  de  este  proyecto.  En   este  caso  la
planificación  del   proyecto  ha  sido   desarrollada  siguiendo  una
metodología ágil \cite{agilAnteproyecto}.

Una  metodología  ágil  es  un  método  donde  los  requisitos  y  las
soluciones van  evolucionando a partir  de la división del  trabajo en
módulos  minimizando riesgos y  resultando más  sencillo. Con  esto se
consigue que la  toma de decisiones de la  planificación inicial pueda
ser modificada con el menor impacto posible en el proyecto.

Las  metodologías   tradicionales  resultan  efectivas   en  numerosos
proyectos, sin  embargo no resultan  adecuadas para entornos  donde el
sistema es  cambiante. El hecho  de emplear una metodología  ágil hace
que este tipo de entornos sea contemplado, ofreciendo una capacidad de
respuesta mayor  que las metodologías  tradicionales a los  cambios no
contemplados  en  un primer  momento.   Dentro  de  la diversidad  que
presenta este tipo de metodología, se ha escogido el método de trabajo
de  \textit{«Scrum»}\footnote{\url{https://www.scrum.org/}},  el  cual
define  unas  pautas  específicas   a  seguir  para  conseguir  buenos
resultados dado un planteamiento.

En lugar  de llevar a cabo  la ejecución completa  del proyecto, Scrum
sigue  una  estrategia  de  desarrollo iterativo  e  incremental.   La
planificación del proyecto se realiza mediante bloques, donde cada uno
de ellos es  llevado a cabo en una  iteración, generando un resultado.
La prioridad  de cada iteración viene  dada según el  beneficio que su
resultado   genera  al  proyecto   completo,  generando   una  entrega
parcial. Al final de cada  iteración el resultado obtenido es mostrado
al cliente para que pueda tomar las decisiones que considere oportunas
en  función de  lo  observado, pudiendo  replanificar  algunas de  las
iteraciones futuras.

Este método  de trabajo contempla  diversos factores cambiantes  en el
tiempo (requisitos, tiempo,  recursos y tecnología), por lo  que es un
buen  motivo  elegir  trabajar   con  esta  metodología  debido  a  la
flexibilidad que  presenta a  la hora de  responder a los  cambios que
pueden surgir. Esta es una de las principales diferencias con el resto
de metodologías,  en las cuales  el proceso de desarrollo  es definido
por completo desde el inicio \cite{agilScrum}.

Dentro  del proceso  de Scrum,  los bloques  en los  que se  divide el
proyecto son  cortos y fijos,  con una duración  entre 1 y  4 semanas,
según  se requiera. Cada  iteración ofrece  un resultado  completo, el
cual aporta un  incremento a la funcionalidad del  proyecto final, por
lo que los resultados de los bloques anteriores resultan fundamentales
para realizar  los siguientes. Al  presentar un resultado  completo al
final de  cada iteración se reduce  el número de  errores del proyecto
general.

El  plan del proyecto  dentro de  Scrum se  encuentra formado  por una
lista de  objetivos/requisitos priorizada, la  cual se reparte  en las
distintas  iteraciones y entregas.  El número  de objetivos/requisitos
simultáneos en  los que se encuentra  trabajando el equipo  no debe de
ser muy grande  para poder tener una capacidad  de reacción mayor ante
situaciones o cambios inesperados.

Otra metodología  ágil con  bastante éxito es  \acf{XP}. Al  igual que
Scrum,  se trata de  una metodología  enfocada a  la adaptación  a los
diferentes cambios  que pueden  surgir a lo  largo del  desarrollo del
proyecto.  A  grandes  rasgos,  son metodologías  muy  similares,  sin
embargo presentan  algunas diferencias. A continuación  se muestra una
comparativa entre  las principales diferencias entre  Scrum y \acs{XP}
(ver Cuadro~\ref{tab:comparativaScrum-XP}) \cite{scrumXP}.

\begin{table}[hp]
  \centering
  {\small
  \input{tables/comparativaScrum-XP.tex}
  }
  \caption[Comparativa entre las metodologías ágiles Scrum y \acs{XP}]
  {Comparativa entre las metodologías ágiles Scrum y XP}
  \label{tab:comparativaScrum-XP}
\end{table}


Las diferencias expuestas  en la tabla anterior hace  que Scrum sea el
método de trabajo elegido para  el desarrollo de este proyecto, debido
a  que  presenta una  mayor  adaptación  con  el contexto  considerado
aquí.  Para la  elaboración de  este  proyecto es  conveniente que  el
resultado generado en cada iteración  no pueda ser alterado. Además, a
pesar de tener  unos requisitos iniciales definidos, no  se sabe si en
cualquier momento intervendrá algún factor que altere alguno de estos,
por  lo que  es necesario  que la  lista de  los requisitos  pueda ser
cambiada si  la situación  lo requiere.  Por  último, en este  caso el
equipo de  desarrollo está formado  únicamente por un miembro,  por lo
que resulta imposible la realización del proyecto por parejas.

En las siguientes secciones se  explican las fases que compnen Scrum y
los  roles y responsabilidades  que se  dan dentro  de este  método de
trabajo.

\section{Fases de Scrum}

Scrum  se  desarrolla básicamente  en  tres  fases:  pre-game, game  y
post-game.

\begin{itemize}
\item  \textbf{Pre-game  phase.}  Esta  fase  cuenta  con dos  partes.
  \subitem La primera sub-fase consiste en la definición del sistema a
  desarrollar  y la  planificación de  este.  El  cliente  presenta al
  equipo el \textit{«Product Backlog list»}, que consiste en una lista
  de requisitos priorizada para  desarrollar el proyecto. El equipo se
  compromete con el cliente a llevar  a cabo esa lista y preguntan las
  dudas que  les puedan  surgir.  \subitem En  la segunda  sub-fase el
  equipo  elabora el  \textit{«Sprint Planning»},  que consiste  en la
  planificación de las tareas que  se van a realizar en cada iteración
  para  conseguir   satisface  los  requisitos   a  los  que   se  han
  comprometido. Las iteraciones son organizadas según la prioridad que
  tienen asignada en «Product Backlog list», teniendo en cuenta además
  la dependencia que puede haber entre las distintas tareas.
\item \textbf{Development  phase.} En esta  fase es llevado a  cabo el
  desarrollo del  proyecto. Una vez definido el  «Sprint Planning», se
  lleva a  cabo el \textit{«Sprint»},  es decir, la ejecución  de cada
  iteración. Cada día el  equipo realiza una reunión de sincronización
  (denominada \textit{«Scrum  daily meeting»}) para ver  y analizar el
  estado  de la iteración,  de tal  forma que  se puedan  realizar las
  adaptaciones  necesarias  si  la  iteración lo  requiere.   En  cada
  iteración  son llevadas  a  cabo las  siguientes acciones:  \subitem
  Planificación: se identifican las distintas actividades que se deben
  llevar  a  cabo  para   conseguir  un  resultado  completo  en  cada
  iteración. Se  definen los objetivos a conseguir  una vez finalizada
  la  iteración.   \subitem  Análisis  de requisitos:  se  realiza  un
  estudio de  los requisitos  que se deben  cumplir en  las diferentes
  actividades  de la  iteración.  \subitem  Diseño: se  llevan  a cabo
  diagramas  y/o  esquemas que  ayuden  a  comprender  las acciones  a
  realizar en cada  actividad. En esta fase pueden  ser vistos algunos
  requisitos conflictivos.  \subitem  Codificación: consiste en llevar
  a  cabo la  implementación de  lo anteriormente  definido.  \subitem
  Revisión:  verificar   que  no  existen  errores   en  las  acciones
  realizadas y que se ha alcanzado el objetivo deseado.
\item \textbf{Post-game  phase.} Una  vez completada la  iteración, el
  equipo expone al cliente los requisitos completados por medio de una
  reunión  (denominada  \textit{«Sprint  Review»}). El  cliente  puede
  realizar  cambios  o replanificar  el  proyecto  en  función de  los
  resultados  mostrados en una  iteración. De  esta forma,  el cliente
  puede  ver como  se  han  desarrollado los  objetivos  que expuso  y
  comprobar si se cumplen sus espectativas.
\end{itemize}

\section{Roles y responsabilidades en Scrum}

El proceso  de Scrum implica trabajar colaborativamente,  es decir, en
equipo. Para ello, existe  una comunicación y colaboración tanto entre
los distintos miembros del equipo, como el equipo con el cliente. Cada
miembro implicado  en el desarrollo del proyecto  tiene asociadas unas
responsabilidades que debe  asumir y llevar a cabo.  Dentro del método
de trabajo Scrum se pueden distinguir tres roles principales:

\begin{itemize}
\item  \textbf{Product Owner.}   Corresponde a  la figura  del cliente
  (este puede  ser interno  o externo a  la organización), el  cual se
  encarga  de  representar a  todas  las  personas  interesadas en  el
  proyecto.   Se  encarga de  definir  los  objetivos  del proyecto  y
  expresárselos  al equipo  de desarrolladores  a través  de  la lista
  priorizada  «Product  Backlog». Puede  replanificar  el proyecto  al
  final de  cada iteración, nunca  durante esta, según  los resultados
  obtenidos  en las  iteraciones  anteriores dentro  del contexto  del
  proyecto.  Participa  en las  reuniones «Sprint Planning»  y «Sprint
    Review». En la primera expone los requisitos de forma priorizada y
  responde a las posibles dudas del  equipo, y en la última revisa los
  requisitos completados.

\item \textbf{Scrum Master.} Es el  líder del equipo de desarrollo del
  proyecto. Se encarga de que todos los miembros del equipo cumplan el
  proceso  de Scrum,  de acuerdo  a las  prácticas  establecidas. Este
  administrador se  compromete a entregar los  resultados generados en
  base  a unos  requisitos. Garantiza  la existencia  de una  lista de
  requisitos  priorizada  y  organiza  al equipo  para  conseguir  los
  objetivos. También facilita las  reuniones realizadas, pero no tiene
  autoridad para tomar decisiones en ellas. Además este rol se encarga
  de que el proyecto vaya avanzando según lo acordado.

\item  \textbf{Scrum Team.}  Está formado  por una  o  varias personas
  encargadas de  desarrollar de manera conjunta el  proyecto. Se trata
  de un equipo auto-organizado,  que selecciona los requisitos y estima
  su  dificultad  en cada  iteración.  En  el  «Sprint Planning»  cada
  miembro se  autoasigna unas tareas  que debe completar antes  de que
  finalice dicha iteración. Una buena comunicación entre los distintos
  miembros, así como  entre el equipo y el  líder, resulta fundamental
  para poder llevar a cabo el proyecto de manera satisfactoria, por lo
  que  los distintos  miembros  deben trabajar  en  un mismo  entorno,
  maximizando así la comunicación entre ellos. Un buen equipo debe ser
  multidisciplinar, intentando depender lo mínimo de personas externas
  al equipo. Además,  deben dedicarse a tiempo  completo a un único
  proyecto  para  conseguir  una  buena productividad  y  mantener  el
  compromiso realizado con el cliente.
\end{itemize}

Además  de los  roles  detallados anteriormente,  existen otros  roles
dentro de Scrum considerados auxiliares. Estos roles no tienen por qué
darse siempre en el proceso de Scrum. Son los siguientes:

\begin{itemize}
\item  \textbf{Stakeholders.}  Este   rol  hace  referencia  a  aquellas
  personas que aportan  alguna función dentro del proyecto  y para las
  cuales el  proyecto les produce  un beneficio. Pueden  ser clientes,
  proveedores, vendedores, etc.
\item  \textbf{Managers  o  administradores.}  Son  aquellas  personas
  encargadas de tomar  las decisiones finales y de  indicar las normas
  que se deben seguir a la  hora de realizar el proyecto, es decir, se
  encargan  de  establecer de  manera  general  el  ambiente donde  se
  desarrolla el proyecto.
\end{itemize}

Una  vez  explicado  el   funcionamiento  y  los  roles  que  componen
Scrum\footnote{Si  no se  ha comprendido  algún concepto el lector  puede  ir al
  Anexo  ~\ref{anexo3},  el cual  contiene  los  conceptos básicos  de
  Scrum.} se detalla cómo se ha aplicado este método de trabajo a este
proyecto a la hora de ser desarrollado.

\section{Roles y responsabilidades de Scrum aplicados a este proyecto}

En el  siguiente cuadro aparecen  los distintos roles asignados  a las
personas de  este proyecto (ver  Cuadro~\ref{tab:rolesScrum}). En este
caso  no  se  han  dado  los  roles  auxiliares  de  «stakeholder»  ni
«manager».

\begin{table}[hp]
  \centering
  {\small
  \input{tables/rolesScrum.tex}
  }
  \caption[Roles asignados a las personas de este proyecto]
  {Roles asignados a las personas de este proyecto}
  \label{tab:rolesScrum}
\end{table}

\section{Fases de Scrum aplicadas a este proyecto}

En esta  sección se describen  las distintas fases que  componen Scrum
sobre   las    cuales   se   ha   desarrollado    el   proyecto   (ver
Figura~\ref{fig:procesoScrum}). El  proyecto ha sido  dividido en siete
iteraciones, correspondiendo cada una a un incremento que aporta nueva
funcionalidad  al  proyecto general.  Cada  iteración  ofrece toda  la
información referente a las  tareas que contiene, siguiendo el esquema
que    se     puede    ver    en    la     siguiente    imagen    (ver
Figura~\ref{fig:detalleIteracion}).

\begin{figure}[!h]
\begin{center}
\includegraphics[width=1\textwidth]{procesoScrum.png}
\caption{Fases de Scrum desarrolladas en este proyecto}
\label{fig:procesoScrum}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[width=0.9\textwidth]{detalleIteracion.png}
\caption{Detalle de cada iteración llevada a cabo en este proyecto}
\label{fig:detalleIteracion}
\end{center}
\end{figure}

\begin{itemize}
\item  \textbf{Pre-game  phase.}  \subitem  En  esta  primera fase  es
  llevada  a cabo  la planificación  del  proyecto.  A  partir de  los
  objetivos inicialmente expuestos (véase~\ref{chap:objetivos}) se trata de construir un
  sistema  distribuido  de  razonamiento  espacio-temporal  basado  en
  sentido común para Smart Cities.

En principio, las iteraciones tienen una duración entre 1 y 4 semanas,
dependiendo de la  dificultad de las tareas a  realizar dentro de cada
iteración. Es  necesario comunicar que el desarrollo  de este proyecto
ha  sido compatibilizado  con la  asistencia y  evaluación  de ciertas
asignaturas  de la  Escuela Superior  de  Informática, por  lo que  en
ciertas ocasiones no es posible dedicar más de 1 ó 2 horas al día.

El  cliente ha  presentado al  equipo su  lista «Product  Backlog». El
orden  de los  objetivos  presentados corresponde  a  la prioridad  de
estos, por lo que dicho orden será el seguido a la hora de desarrollar
el  proyecto. Se  muestra  a  continuación la  lista  expuesta por  el
cliente.

\subsubitem \textbf{Product Backlog List}

- \textit{Objetivo 1}: Búsqueda, análisis y selección de una ontología
para ciudades inteligentes.

- \textit{Objetivo 2}:  Traducción a la base de  conocimiento Scone de
la ontología seleccionada y generación de un parser para la traducción
automática de modelos de OpenStreetMap.

- \textit{Objetivo 3}:  Implementación en  Scone de los  mecanismos de
razonamiento espacio-temporal.

- \textit{Objetivo  4}:  Desarrollo de  un  sistema  para validar  los
mecanismos de razonamiento espacio-temporal.

- \textit{Objetivo  5}: Integración  de  los mecanismos  desarrollados
para el razonamiento espacio-temporal en la Smart City a través de una
infraestructura middleware.

- \textit{Objetivo  6}: Realización de un sistema que valide los mecanismos de razonamiento espacio-temporal llevados a cabo.

Una vez  definida la lista de  objetivos, en la segunda  parte de esta
fase el equipo elabora  la planificación de las distintas iteraciones,
es decir, el «Sprint Planning».

\subsubitem \textbf{Sprint Planning}

- \textit{Iteración 0}: Realización de un estado del arte.

- \textit{Iteración  1}:  Estructuración de  la  base de  conocimiento
Scone.

- \textit{Iteración  2}: Introducción  del  conocimiento necesario  en
Scone para algoritmos de razonamiento.

- \textit{Iteración    3}:   Implementación de  reglas    y   mecanismos
espacio-temporales.

- \textit{Iteración   4}:  Diseño   del   algoritmo  para   el
reconocimiento de matrículas.

- \textit{Iteración 5}: Integración en un sistema distribuido.

- \textit{Iteración 6}: Realización de un sistema de validación.

\item \textbf{Game phase.} 

Con el  «Sprint Planning»  definido, se lleva  a cabo la  ejecución de
cada uno de los «Sprint». Es necesario decir que el equipo de trabajo de Scrum ha hecho uso del gestor de proyectos «Redmine\footnote{\url{http://www.redmine.org/}}». Esta herramienta de código libre ha servido para gestionar toda la información asociada al proyecto de manera organizada, pudiendo controlar cada parte en todo momento. La información ha sido organizada en bloques denominados «tareas», por lo que el control de las diferentes partes del proyecto ha resultado más sencillo. Además el uso de este gestor ha dado lugar a una mejor sincronización entre los distintos miembros del equipo de Scrum. 

\subitem \textbf{Iteración 0: Realización de un estado del arte}

\subsubitem \textit{Planificación}:  Realizar un estado  del arte para
elegir la ontología adecuada para la Smart City.

\subsubitem \textit{Análisis de requisitos}: La ontología seleccionada
debe  describir todos  los conceptos  y relaciones  que se  dan  en el
dominio de la Smart City.

\subsubitem  \textit{Diseño}:  Estudio  de  diversas  ontologías,  las
cuales podrían ser las posibles para describir la Smart City.

\subsubitem \textit{Codificación}: Tras  el estudio realizado se lleva
a cabo la elección de la  ontología SOFIA por ser la ontología que más
se adapta al  entorno requerido. El resto de  ontologías examinadas se
centraban únicamente en  un ámbito de la Smart  City, como por ejemplo
el ámbito del transporte o el de la energía. Además en esta iteración es llevada a cabo la redacción del estado del arte realizado para la elección de SOFIA. 

\subsubitem  \textit{Revisión}:  Comprobación   de  que  SOFIA  es  la
ontología más adecuada y completa  para tratar la variedad de términos
y sus posibles relaciones dentro de la Smart City. También es necesario realizar una verificación de la documentación escrita acerca del estado del arte.

\subitem   \textbf{Iteración   1:  Estructuración   de   la  base   de
  conocimiento   Scone.}   \subsubitem   \textit{Planificación}:  Esta
iteración cuenta con  dos sub-partes. Por un lado  la traducción de la
ontología  SOFIA a términos  de Scone,  por otro  la traducción  de la
distribución de la ciudad a términos de Scone.
\subsubitem  \textit{Análisis  de   requisitos}:  Para  realizar  esta
iteración es  necesario familiarizarse con una  serie de herramientas,
las cuales  contienen parte del conocimiento  que se debe  migrar a la
base de conocimiento Scone.
\subsubitem  \textit{Diseño}:  Para   realizar  la  selección  de  las
herramientas que dan acceso a  la información que se debe almacenar en
Scone es necesario realizar un estudio previo de las mismas. Tras este
estudio se decide que la  herramienta encargada de tratar la ontología
SOFIA sea \textit{Protégé}  y que la distribución de  la ciudad (tanto
su callejero como otra información relevante) sea obtenida a través de
\textit{OpenStreetMap}.
\subsubitem \textit{Codificación}: Por  medio de Protégé se visualizan
las  distintas  entidades  y   relaciones  que  comprende  SOFIA.   La
implementación de su contenido da  lugar a dos archivos escritos en el
lenguaje   de  programación   LISP   (denominados  «entities.lisp»   y
«properties.lisp») para poder ser  almacenados en Scone. La traducción
de la información proporcionada por  OpenStreetMap se lleva a cabo por
medio  del   desarrollo  de  un   parser  para  traducir  del   XML  de
OpenStreetMap a Scone. Esta implementación desarrollada en el lenguaje
de   programación   Python   consigue  migrar   automáticamente   esta
información a archivos LISP.
\subsubitem \textit{Revisión}:  Comprobación de  que se dispone  de la
estructura conceptual necesaria para albergar la información que se va
generando en un contexto como el de la Smart City.

\subitem \textbf{Iteración 2:  Introducción del conocimiento necesario
  en   Scone   para    algoritmos   de   razonamiento.}    \subsubitem
\textit{Planificación}:  Una vez  seleccionadas  las herramientas,  se
procede a poblar  la base de conocimiento con  instancias concretas de
los tipos identificados.  
\subsubitem  \textit{Análisis de  requisitos}: La  información  que se
almacena  en   la  base  de   conocimiento  debe  dar  soporte   a  la
implementación de algoritmos que permitan el razonamiento.  Además esta
información debe estar  escrita en el lenguaje LISP  para que Scone la
pueda interpretar.
 \subsubitem \textit{Diseño}:  La información que se  debe almacenar en
Scone   tiene   que  permitir   extraer   información  más   elaborada
semánticamente que  una simple distribución  de calles y  elementos en
las mismas.   Se realiza  una imagen orientativa de como debe  quedar la  base de
conocimiento una vez se introduzca el correspondiente conocimiento.
\subsubitem \textit{Codificación}: Se introducen los archivos escritos
en  el  lenguaje  de  programación  LISP  generados  en  la  iteración
anterior.
\subsubitem  \textit{Revisión}: Comprobar  que  se posee  una base  de
conocimiento con  información necesaria  para llevar a  cabo cualquier
tipo de algoritmo de búsqueda y razonamiento.

\subitem  \textbf{Iteración 3: Implementación  de reglas  y mecanismos
  espacio-temporales.}        \subsubitem      \textit{Planificación}:
Implementación de  mecanismos de sentido común que  describan cómo los
seres    humanos   somos   capaces    de   razonar    sobre   aspectos
espacio-temporales.
\subsubitem \textit{Análisis de  requisitos}: Los mecanismos deben dar
soporte al razonamiento espacio-temporal dentro de la Smart City.
\subsubitem \textit{Diseño}: A pesar de que Scone contiene por defecto
implementados ciertos  algoritmos de inferencia,  ninguno se encuentra
enfocado al razonamiento espacio-temporal. Se procede a la creación de
uno de estos para almacenarlo en dicha base de conocimiento. Se diseña
una  lista con las  entidades, y  sus relaciones,  que debe  tener una
Smart City.
\subsubitem \textit{Codificación}:  Se lleva a  cabo la implementación
de  un  archivo  en  el  lenguaje  de  programación  LISP  (denominado
«SmartCityDomainKnowledge.lisp»),  el cual  recoge  la lista  diseñada
anteriormente. El conocimiento de este archivo sirve para dar lugar al
razonamiento  espacio-temporal dentro  de la  ciudad  inteligente. Por
medio de dicha  información es posible crear algoritmos  que dan lugar
al razonamiento espacio-temporal dentro de la Smart City.
\subsubitem \textit{Revisión}:  Verificar que las  reglas y mecanismos
implementados dan soporte al razonamiento espacio-temporal.

\subitem  \textbf{Iteración 4:  Diseño del  algoritmo  para el
  reconocimiento de matrículas.}
\subsubitem  \textit{Planificación}:  Con  los  mecanismos  anteriores
implementados, realizar  una adaptación  para demostrar la  validez de
estos.
\subsubitem   \textit{Análisis   de   requisitos}:  El   algoritmo   a
desarrollar  debe  probar  el  razonamiento  espacio-temporal  con  el
conocimiento contenido en la base de conocimiento Scone.
\subsubitem  \textit{Diseño}:  Realización de  un  pseudocódigo de  un
algoritmo que verifique el razonamiento espacio-temporal dentro de una
Smart City.  El algoritmo consistirá  en realizar un seguimiento  a un
determinado  coche  (a  través   de  la  detección  de  su  matrícula)
utilizando las cámaras desplegadas a lo largo de la ciudad.
\subsubitem \textit{Codificación}: En  base al pseudocódigo realizado,
se  lleva a cabo  la implementación  del algoritmo  en el  lenguaje de
programación Python. A partir de una determinada posición de un coche,
el  algoritmo es  capaz de  descubrir las  posibilidades que  tiene de
realizar  algún  giro o  seguir  por  la misma  vía  y  de ofrecer  la
siguiente  cámara  que debe  seguir  grabando  para  continuar con  el
seguimiento del coche.
\subsubitem \textit{Revisión}: Comprobar que el algoritmo implementado
funciona  correctamente,  es decir,  que  localiza  al  coche en  todo
momento.

\subitem \textbf{Iteración 5: Integración del algoritmo anterior en un sistema distribuido}

\subsubitem  \textit{Planificación}: Selección  de un  middleware para
que el algoritmo creado anteriormente sea instanciado como un servicio
de  dicho  middleware.  Una   vez  seleccionado,  se  procederá  a  la
adaptación del sistema a dicho middleware.

\subsubitem    \textit{Análisis   de   requisitos}:    El   middleware
seleccionado debe  adaptarse para ser  desplegado en el entorno  de la
Smart City.

\subsubitem \textit{Diseño}: Elección del middleware ZeroC ICE para el
sistema  distribuido, de tal  manera que  los distintos  servicios que
contiene funcionen como simuladores de Smart City, en el que de manera
transparente  sea posible acceder a  streaming de  vídeos como  si fueran
cámaras reales desplegadas en distintos puntos de la ciudad. Diseño de
un  diagrama  para  comprender   mejor  la  estructura  que  tiene  el
middleware en este proyecto.

\subsubitem   \textit{Codificación}:  Instalación   de  ZeroC   ICE  y
adaptación del algoritmo como servicio de dicho middleware.

\subsubitem \textit{Revisión}:  Comprobar la integración  del servicio
de razonamiento espacio-temporal en el sistema distribuido.


\subitem \textbf{Iteración 6: Realización de un sistema de validación}

\subsubitem  \textit{Planificación}: Llevar a cabo el desarrollo de un sistema que verifique el anterior algoritmo diseñado. 

\subsubitem    \textit{Análisis   de   requisitos}: El sistema a diseñar debe de verificar que el algoritmo diseñado en la anterior iteración da lugar a razonamientos del espacio y del tiempo dentro de las Smart Cities. El sistema a implementar debe de considerar todos los casos posibles que se pueden dar en una ciudad. 

\subsubitem \textit{Diseño}: Se realiza el diseño de un sistema que, mediante el empleo del razonamiento, dado el último punto donde el vehículo fue visualizado, se lleve a cabo la activación del conjunto de cámaras posibles donde el vehículo al cual se le está aplicando el vídeoseguimiento, se puede encontrar. 

\subsubitem   \textit{Codificación}: La implementación de este sistema se lleva a cabo con la realización de archivos escritos en el lenguaje de programación Python y su integración en el middleware ICE a través del servicio «IceGrid» que ofrece. Además una vez diseñado el sistema, se procede a la realización de un vídeo que demuestra el funcionamiento de dicho sistema. 

\subsubitem \textit{Revisión}:  Comprobar el funcionamiento del sistema realizado mediante pruebas. Dada una calle concreta y la ruta que sigue el coche, el diseño tiene que mostrar las calles por las que va realizando el seguimiento al vehículo.


\item \textbf{Post-game phase.} 

A continuación se muestran las fases post-game de cada iteración:

\subitem \textbf{Iteración 0: Realización de un estado del arte.}

Esta fase del  proyecto fue completada en un tiempo  de 4 semanas. Una
vez  completada la  iteración,  esta  fue expuesta  al  cliente en  la
reunión  «Sprint Review».  El  cliente examinó  la  ontología SOFIA  y
comprobó que se adaptaba a los requisitos expuestos. El equipo procede
a realizar la siguiente iteración.

\subitem   \textbf{Iteración   1:  Estructuración   de   la  base   de
  conocimiento Scone.}

Esta fase del proyecto fue completada en un tiempo de 3 semanas ya que
el  hecho de  realizar el  parser incrementaba  la dificultad  de esta
iteración.

Inicialmente,  este parser  consistía en  un único  archivo denominado
«osm.py»,  cuya estructura  puede ser  vista en  el diagrama  de flujo
contenido  en  el  Anexo~\ref{anexo5}.   Posteriormente,  tras  varias
reuniones con el Manager del proyecto  se llegó a la conclusión de que
la  información  que generaba  el  parser  debía  distribuirse en  dos
archivos distintos, por lo que debía existir una separación del parser
para  que la  base  de conocimiento  cargara  primero una  determinada
información  sobre la que  la segunda  pudiera construirse.   Esto dio
lugar  a  la  implementación  de  dos  archivos  escritos  en  Python:
«osm1.py» y «osm2.py»

La separación del archivo original en dos partes hizo que la iteración
tuviera una duración  de 3 días más. Una  vez completada la iteración,
esta fue expuesta al cliente en la reunión «Sprint Review». El cliente
examinó los  archivos que generaban  «osm1.py» y «osm2.py»  (estos son
«streetsInformation.lisp» y «streetsAdjacents.lisp» respectivamente) y
verificó  que  se adaptaban  a  los  requisitos  expuestos. El  equipo
procede a realizar la siguiente iteración.

\subitem \textbf{Iteración 2:  Introducción del conocimiento necesario
  en Scone para algoritmos de razonamiento.}

Esta fase del  proyecto fue completada en 1 semanas. Una vez obtenidos
los archivos  en la iteración  anterior, el almacenamiento y  carga de
estos en Scone  no era de gran dificultad. El  cliente comprobó que la
introducción del  conocimiento se había realizado  correctamente y que
Scone  almacenada la información  sin problemas.  El equipo  procede a
realizar la siguiente iteración.

\subitem  \textbf{Iteración  3:  Implementación  de reglas  y  mecanismos
  espacio-temporales.}

Esta fase  del proyecto fue completada  en un tiempo de  2 semana. Una
vez  completada la  iteración,  esta  fue expuesta  al  cliente en  la
reunión   «Sprint   Review».     El   cliente   examinó   el   archivo
«SmartCityDomainKnowledge.lisp»  y  comprobó  que  se adaptaba  a  los
requisitos  expuestos.   El equipo  procede  a  realizar la  siguiente
iteración.

\subitem  \textbf{Iteración 4:  Diseño del  algoritmo  para el
  reconocimiento de matrículas.}

Esta fase  del proyecto fue completada  en un tiempo de  4 semanas. La
implementación del algoritmo  era de gran dificultad. Cada  vez que se
realizaba una  verificación de éste, surgía algún  caso específico que
no se había contemplado y que se debía de resolver. Una vez completada
la  iteración, esta  fue expuesta  al  cliente en  la reunión  «Sprint
  Review». El cliente examinó el  algoritmo y comprobó que se adaptaba
a los requisitos expuestos. El  equipo procede a realizar la siguiente
iteración.

\subitem \textbf{Iteración 5: Integración del algoritmo anterior en un
  sistema distribuido}

Esta fase  del proyecto fue completada  en un tiempo de  4 semanas. La
tarea de adaptar el algoritmo anterior a un sistema distribuido era de
gran dificultad. Una vez completada la iteración, esta fue expuesta al
cliente  en la  reunión «Sprint  Review». El  cliente comprobó  que la
distribución del sistema era correcta.

\subitem \textbf{Iteración 6: Realización de un sistema de validación}

Esta fase  del proyecto fue completada  en un tiempo de  4 semanas. La implementación del sistema era de gran dificultad, ya que requería la intervención de otros factores externos a este proyecto. Ha sido posible realizar este sistema gracias a la colaboración de algunos miembros del grupo de investigación Arco, por lo que en esta iteración el equipo de trabajo de Scrum pasó a tener seis miembros. Una vez completada la iteración, esta fue expuesta al cliente  en la  reunión «Sprint  Review». El  cliente comprobó mediante la visualización de un ejemplo en vídeo que el sistema era correcto.\\\\

Una  vez  terminadas  todas  las  iteraciones, el  proyecto  final  se
encuentra acabado. El cliente  no ha realizado alteraciones respecto a
la especificación inicial, por lo que el orden propuesto en el «Sprint
  Planning»  inicial  ha  sido  el  orden  llevado  a  cabo.  Una  vez
completadas todas  las iteraciones, el equipo de desarrollo expone al
cliente  el resultado  final. Mediante el vídeo realizado en la última iteración, la verifación del sistema por parte del cliente le resulta más sencilla. Este comprueba  que  el sistema diseñado funciona correctamente y por lo tanto, el algoritmo y los mecanismos implementados en el proyecto cumplen sus funciones. Por lo tanto, el producto  final obtenido  cumple  satisfactoriamente con  sus  expectativas  y  el cliente determina que el resultado obtenido es el esperado.

\end{itemize}

% Local Variables:
%  coding: utf-8
%  mode: latex
%  mode: flyspell
%  ispell-local-dictionary: "castellano8"
% End:
