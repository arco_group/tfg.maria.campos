#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import Ice
Ice.loadSlice('../slice/civitas.ice -I /usr/share/Ice/slice')

import SemanticModel



class Client(Ice.Application):
    def run(self, argv):
        proxy = self.communicator().stringToProxy(argv[1])
	service = SemanticModel.ServicePrx.checkedCast(proxy)

        if not service:
            raise RuntimeError('Invalid proxy')

        theThingDict = {}

        # t = SemanticModel.Thing()
        # t.type = SemanticModel.TypeCode.stringT
        # t.value = "Ronda de Calatrava"
        # theThingDict["StreetName"] = t

        # lat = SemanticModel.Thing()
        # lat.type = SemanticModel.TypeCode.stringT
        # lat.value = "38.992067"
        # theThingDict["latitude"] = lat

        # lon = SemanticModel.Thing()
        # lon.type = SemanticModel.TypeCode.stringT
        # lon.value = "-3.922270"
        # theThingDict["longitude"] = lon

        # t = SemanticModel.Thing()
        # t.type = SemanticModel.TypeCode.stringT
        # t.value = "Calle Calatrava"
        # theThingDict["StreetName"] = t

        # lat = SemanticModel.Thing()
        # lat.type = SemanticModel.TypeCode.stringT
        # lat.value = "38.991541"
        # theThingDict["latitude"] = lat

        # lon = SemanticModel.Thing()
        # lon.type = SemanticModel.TypeCode.stringT
        # lon.value = "-3.923107"
        # theThingDict["longitude"] = lon



        # t = SemanticModel.Thing()
        # t.type = SemanticModel.TypeCode.stringT
        # t.value = "Calle Estrella"
        # theThingDict["StreetName"] = t
        # lat = SemanticModel.Thing()
        # lat.type = SemanticModel.TypeCode.stringT

        # lat.value = "38.991293"
        # theThingDict["latitude"] = lat

        # lon = SemanticModel.Thing()
        # lon.type = SemanticModel.TypeCode.stringT
        # lon.value = "-3.923439"
        # theThingDict["longitude"] = lon


        t = SemanticModel.Thing()
        t.type = SemanticModel.TypeCode.stringT
        t.value = "Calle San Antonio"
        theThingDict["StreetName"] = t
        lat = SemanticModel.Thing()
        lat.type = SemanticModel.TypeCode.stringT

        lat.value = "38.990849"
        theThingDict["latitude"] = lat

        lon = SemanticModel.Thing()
        lon.type = SemanticModel.TypeCode.stringT
        lon.value = "-3.924759"
        theThingDict["longitude"] = lon

        service.performAction('getNextCamera', theThingDict, None)

        return 0


sys.exit(Client().main(sys.argv))
