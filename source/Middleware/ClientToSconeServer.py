#!/usr/bin/python
#-*- coding: utf-8 -*-

import sys
import Ice
Ice.loadSlice('../slice/SconeWrapper.ice')
import SconeWrapper


class client(Ice.Application):
    def run(self, argv):
        proxy = self.communicator().stringToProxy(argv[1])
        scone = SconeWrapper.SconeServicePrx.checkedCast(proxy)

        if not scone:
            raise RuntimeError('Invalid proxy')
	cad = "(new-type {actor} {thing})\n"
	#cad = '(get-adjacent {' + nombreCalle +'}')
        answer = scone.sconeRequest(cad)
	print answer

        return 0

    #myStreet = "Calle de la Mata"

    # def getNextCamera(myStreet):

    #     posibleStreets.append(myStreet)
    #     longStreet, speed, timeOut, estimatedPosition
    #     expectedStreets = []

    #     for i in getAdjacents(myStreet):
    #         streetsAdjacents.append(getAdjacents[i])

    #     for i in streetsAdjacents:
    #         if senseWay(streetAdjacents[i]):
    #             posibleStreets.append(streetAdjacents[i])

    #         elif ckeckTrafficDirection(myStreet, streetAdjacents[i]):
    #             if utilWay(streetAdjacents[i]):
    #                 posibleStreets.append(streetAdjacents[i])

    #     for j in possibleStreets:
    #         if not carInScene (possibleStreets[j], timeOut):
    #             estimatedPosition = speed * timeOut
    #             expectedStreets.append(getNearStreet(estimatedPosition))

    #     return expectedStreet

    # def getAdjacents(myStreet):
    #    adjacents = ["Calle de Alonso Quijano","Calle del Infante de la Cerda", "Calle Pozo Concejo","Calle General Rey"]
    #    return adjacents

    # def senseWay(streetAdjacents):
    #     return false

    # def checkTrafficDirection(myStreet, streetAdjacents):
    #     return true

    # def utilWay(streetAdjacents)
    #     return true

    # def carInScene(possibleStreets, timeOut):
    #     return true

    # def getNearStreet(estimatedPosition):
    #     streets = ["Calle de Alonso Quijano","Calle del Infante de la Cerda", "Calle Pozo Concejo"]
     #   return streets

sys.exit(client().main(sys.argv))
