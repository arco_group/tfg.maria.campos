
EJECUCIÓN 
------------------------------------------------------------------

-Para ejecutar el Client.py introducir en la consola lo siguiente:

python Client.py 'service1 -t -e 1.1:tcp -h 161.67.106.58 -p 9090'


-Para ejecutar el Server.py introducir en la consola lo siguiente:

python Server.py --Ice.Config=Server.config "sconeService -t -e 1.1:tcp -h 161.67.106.88 -p 9090"



CAMBIOS EN ARCHIVOS
------------------------------------------------------------------

-- Añadir en Server.config esto:

Ice.ThreadPool.Server.Size = 10
Ice.ThreadPool.Client.Size = 10


-- Server.py

- Añadir las siguientes cabeceras:

Ice.loadSlice('../slice/SconeWrapper.ice')
import SconeWrapper

- Función "sendToScone", añadir esto al principio:

 def sendToScone(self, cad, theCommunicator):
	proxy = theCommunicator.stringToProxy("sconeService -t -e 1.1:tcp -h 161.67.106.88 -p 9090") #el proxy lo vas a tener que meter como cadena scone = SconeWrapper.SconeServicePrx.checkedCast(proxy)
	sconePrx = SconeWrapper.SconeServicePrx.checkedCast(proxy)
	if not sconePrx:
		...........

- Función "performAction", dentro del if, después de los dos print añadir esto:

	..........
	theCommunicator=current.adapter.getCommunicator()
        self.getNextCamera(street["StreetName"].value, theCommunicator)

- Para hacer una prueba sencilla, comentamos el resto de funciones de la clase "GetNextCameraServiceI" y en la función "getNextCamera" se añaden estas líneas para probar el método "utilWay":

	cad = "(is-util-way {"+myStreet+"})\n"
	print self.sendToScone(cad, theCommunicator)

- La clase Server se queda igual. 

-- Client.py no cambia. 


