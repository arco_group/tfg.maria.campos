#!/usr/bin/python -u
# -*- coding: utf-8 -*-

import sys
import Ice
from geopy import distance
from geopy import Point
Ice.loadSlice('../slice/civitas.ice -I /usr/share/Ice/slice')
Ice.loadSlice('../slice/SconeWrapper.ice')
import SemanticModel
import SconeWrapper


class GetNextCameraServiceI(SemanticModel.Service):
        def sendToScone(self, cad, theCommunicator):

                proxy = theCommunicator.stringToProxy("sconeService -t -e 1.1:tcp -h 161.67.27.95 -p 9091") #el proxy lo vas a tener que meter como cadena scone = SconeWrapper.SconeServicePrx.checkedCast(proxy)
                sconePrx = SconeWrapper.SconeServicePrx.checkedCast(proxy)
                if not sconePrx:
                        raise RuntimeError('Invalid proxy')
                if '\n' in cad:
                        answer = sconePrx.sconeRequest(cad)
                else:
                        cad = cad+'\n'
                        answer = sconePrx.sconeRequest(cad)
                return answer
                
        def parseSconeResult(self, str):
            finalStr =[]
            if len(str) > 2:
                tmpStr = str.replace('NIL','').replace('(','').replace(')','').replace('{','').replace('\n','').split('}')
                for i in tmpStr:
                    if not i=='':
                        finalStr.append(i)
                return finalStr
            else:
                return str

        def performAction(self, action, street, theSink, current=None):
                if action == "getNextCamera":
                        theCommunicator=current.adapter.getCommunicator()
                        self.getNextCamera(street["StreetName"].value, street["latitude"].value, street["longitude"].value,  theCommunicator)
    

        def getNextCamera(self, myStreet, street_lat, street_lon, theCommunicator):
                lastTimeSeenPoint = Point(' '.join([street_lat, street_lon]))
                possibleStreets = []
                possibleStreets.append(myStreet)
                streetAdjacents = []
                streetsOfNextCamera=[]
                myStreetWithSections = self.getStreetSections(myStreet, theCommunicator)
                tmpAdjacents = self.getAdjacents(myStreetWithSections, theCommunicator)
                
                print tmpAdjacents
                for i in tmpAdjacents:
                        if self.utilWay(i, theCommunicator):
                                 streetAdjacents.append(i)

                for i in streetAdjacents:
                        if self.numberOfWays(i, theCommunicator):
                                possibleStreets.append(i)

                        elif self.checkTrafficDirection(myStreet, i, theCommunicator):
                                possibleStreets.append(i)
                for street in possibleStreets:
                        # Pido el punto en el que la calle street
                        # intersecciona con myStreet y ese punto lo
                        # mido al punto donde el coche fue visto por
                        # ultima vez 
                        point = self.getNearestPointTo(street,lastTimeSeenPoint , theCommunicator)
                        dist = distance.distance(point,lastTimeSeenPoint).meters

                        if dist <130:
                                streetsOfNextCamera.append(street)

                print streetsOfNextCamera
                self.enableCamerasNearTo(streetsOfNextCamera, myStreet, theCommunicator)

        def enableCamerasNearTo(self, streetsOfNextCamera, myStreet, theCommunicator):
                cad = "(incoming-parent-elements  (lookup-element {camera}))\n"
                cameras = []
                proxies = []
                cameras = self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                for camera in cameras:
                        if self.isCameraNearSelectedStreets(camera, streetsOfNextCamera, myStreet, theCommunicator):
                                cad = "(the-x-of-y {proxy} {"+camera+"})\n"
                                proxies.append(self.parseSconeResult(self.sendToScone(cad, theCommunicator)))
                print proxies
                                
        def isCameraNearSelectedStreets(self, camera, streetsOfNextCamera, myStreet, theCommunicator):
                latitude = []
                longitude = []

                cad = "(the-x-of-y {latitude} {"+camera+"})\n"
                latitude = self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                cad = "(the-x-of-y {longitude} {"+camera+"})\n"
                longitude=self.parseSconeResult(self.sendToScone(cad, theCommunicator))

                coordinates = ' '.join([latitude[0], longitude[0]])
                
                pointOfCamera = Point(coordinates)

                cad = "(list-all-x-of-y {latitude} {"+myStreet+"})\n"
                latitude = self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                cad = "(list-all-x-of-y {longitude} {"+myStreet+"})\n"
                longitude=self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                
                coordinates = ' '.join([latitude[0], longitude[0]])
                
                myStreetPoint = Point(coordinates)
                                
                for street in streetsOfNextCamera:
                        p1 = self.getNearestPointTo(street.replace('\n',''), myStreetPoint, theCommunicator)
                        d1 = distance.distance(p1,pointOfCamera).meters
                        if  d1 < 100:
                                return True
                               
                
                

        def getNearestPointTo(self, street, point, theCommunicator):

                sectionOfStreet = self.getStreetSections(street, theCommunicator)
                latitude =[]
                longitude = []
                points=[]

                #Busco todos los puntos (nodos OSM) de la calle en la que estoy
                if len(sectionOfStreet)>=1:
                        for SoS in sectionOfStreet:
                                cad = "(list-all-x-of-y {latitude} {"+SoS+"})\n"
                                latitude = self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                                cad = "(list-all-x-of-y {longitude} {"+SoS+"})\n"
                                longitude=self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                                for lat, lon in zip(latitude, longitude):
                                        coordinates = ' '.join([lat, lon])
                                        points.append(Point(coordinates))
                min = 10000
                for p1 in points:
                        d1 =distance.distance(p1,point).meters
                        if min > d1:
                                min = d1
                                minPoint = p1
                return minPoint
                
        def getAdjacents(self, myStreetWithSections, theCommunicator):
                tmpAdj = []
                adjacents = []
                adjacentSections = []
                for myStreet in myStreetWithSections:
                    cad = "(list-all-x-of-y {adjacent} {"+myStreet+"})\n"
                    tmpAdj = self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                    for i in tmpAdj:
                        if not i=='adjacent':
                            adjacentSections = self.getStreetSections(i, theCommunicator)
                            for adjSec in adjacentSections:
                                    adjacents.append(adjSec)
                return adjacents
                
        def checkIfStreetWithSections(self, myStreet, theCommunicator):
                cad = "(list-all-x-of-y {section of} {"+ myStreet+"})\n"
                result = self.sendToScone(cad, theCommunicator)
                if 'section of' in result:
                        return False
                else:
                        return True
                        
        def getStreetSections(self, myStreet, theCommunicator):
                cad = "(list-all-x-of-y {section of} {"+ myStreet+"})\n"
                result = self.sendToScone(cad, theCommunicator)
                streets = []
                if not 'section of' in result:
                    if not myStreet =='':
                            streets.append(myStreet)
                    for i in self.parseSconeResult(result):
                            if not i=='':
                                    streets.append(i)
                    return streets
                else:
                    if not myStreet=='':
                        streets.append(myStreet)
                        return streets

        def numberOfWays(self, streetAdjacent, theCommunicator):

                cad = "(the-x-of-y {number of ways of} {"+streetAdjacent+"})";
                if 'twoways' in self.sendToScone(cad, theCommunicator): 
                        return True
                else:
                        return False
                        

        def checkTrafficDirection(self, myStreet, streetAdjacent, theCommunicator):
                #Mira el sentido de la calle
                streets=[]
                streetsPoints={}
                flag = False
                cad = "(the-x-of-y {traffic direction of} {"+streetAdjacent+"})"
                trafficDirection = self.sendToScone(cad, theCommunicator)
                
                if 'NIL' in trafficDirection:
                        return False
                ## tienes dos variables diccionario de la forma: calleFrom y calleTo
                calleTo = trafficDirection.replace('to ','').replace('}','')[trafficDirection.find("to "):]
                streets.append(calleTo.replace('\n',''))
                streets.append(streetAdjacent.replace('\n',''))
                
                # Si la calle tiene secciones, busco el punto mas
                # cercano al adyacente que este comprobando ahora,
                # sino pues anado la calle a la lista streets y que
                # calcule sus puntos normalmente  
                if self.checkIfStreetWithSections(myStreet, theCommunicator):
                        streetsPoints[myStreet.replace('\n','')] = [self.nearestIntersectionPoint(streetAdjacent.replace('\n',''), myStreet.replace('\n',''), theCommunicator)]
                else:
                        streets.append(myStreet.replace('\n',''))
                for i in streets:
                        latitude = []
                        longitude= []
                        points=[]
                        cad = "(list-all-x-of-y {latitude} {"+i+"})\n"
                        latitude=self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                        cad = "(list-all-x-of-y {longitude} {"+i+"})\n"
                        longitude= self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                        for lat, lon in zip(latitude, longitude):
                                coordinates = ' '.join([lat, lon])
                                points.append(Point(coordinates))
                        streetsPoints[i] = points 

                #Aqui ya sabemos que solo hay un punto, porque es el punto mas cercano
                p2 = streetsPoints[myStreet.replace('\n','')][0]

                for p0, p1 in zip(streetsPoints[calleTo.replace('\n','')], streetsPoints[streetAdjacent.replace('\n','')]):
                        d1=distance.distance(p1,p0).meters
                        d2=distance.distance(p2,p0).meters
                        tmp0=[]
                        tmp1=[]
                        tmp2=[]
                        tmp0.append(p0)
                        tmp1.append(p1)
                        tmp2.append(p2)

                        if d1 == d2:
                                p3 = self.tryWithTheSecondNearestIntersectionPoint(streetAdjacent.replace('\n',''), myStreet.replace('\n',''), theCommunicator)[1]
                                d3=distance.distance(p3,p0).meters
                        if d1>d2:
                                flag = True
                if not flag:
                        return True
                return False
        
        def tryWithTheSecondNearestIntersectionPoint(self, streetAdjacent, streetName, theCommunicator):

                sectionOfStreet = self.getStreetSections(streetName, theCommunicator)
                latitude =[]
                longitude = []
                points=[]
                pointsOfStreetAdjacent=[]
                #Busco todos los puntos (nodos OSM) de la calle en la que estoy
                if len(sectionOfStreet)>=1:
                        for SoS in sectionOfStreet:
                                cad = "(list-all-x-of-y {latitude} {"+SoS+"})\n"
                                latitude = self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                                cad = "(list-all-x-of-y {longitude} {"+SoS+"})\n"
                                longitude=self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                                for lat, lon in zip(latitude, longitude):
                                        coordinates = ' '.join([lat, lon])
                                        points.append(Point(coordinates))
                #Busco ahora todos los puntos de la calle a la que quiero saber si puedo girar
                cad = "(list-all-x-of-y {latitude} {"+streetAdjacent+"})\n"
                latitude=self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                cad = "(list-all-x-of-y {longitude} {"+streetAdjacent+"})\n"
                longitude= self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                for lat, lon in zip(latitude, longitude):
                        coordinates = ' '.join([lat, lon])
                        pointsOfStreetAdjacent.append(Point(coordinates))
                #Ahor mido las distancias de cada uno de los puntos de
                #la calle  streetName a  cada uno de  los de  la calle
                #streetAdjacent devolviendo  aquel cuya  distancia sea
                #menor
                min = 10000
                minPoint=[]
                minPoint.append(Point("38.990451, -3.924156"))
                minPoint.append(Point("38.990451, -3.924156"))
                for p1 in points:
                        for p2 in pointsOfStreetAdjacent:
                                d1 =distance.distance(p2,p1).meters
                                if min > d1:
                                        min = d1
                                        minPoint[1] = minPoint[0]
                                        minPoint[0] = p1

                return minPoint
        def nearestIntersectionPoint(self, streetAdjacent, streetName, theCommunicator):

                sectionOfStreet = self.getStreetSections(streetName, theCommunicator)
                latitude =[]
                longitude = []
                points=[]
                pointsOfStreetAdjacent=[]

                if len(sectionOfStreet)>=1:
                        for SoS in sectionOfStreet:
                                cad = "(list-all-x-of-y {latitude} {"+SoS+"})\n"
                                latitude = self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                                cad = "(list-all-x-of-y {longitude} {"+SoS+"})\n"
                                longitude=self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                                for lat, lon in zip(latitude, longitude):
                                        coordinates = ' '.join([lat, lon])
                                        points.append(Point(coordinates))

                cad = "(list-all-x-of-y {latitude} {"+streetAdjacent+"})\n"
                latitude=self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                cad = "(list-all-x-of-y {longitude} {"+streetAdjacent+"})\n"
                longitude= self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                for lat, lon in zip(latitude, longitude):
                        coordinates = ' '.join([lat, lon])
                        pointsOfStreetAdjacent.append(Point(coordinates))
                min = 10000
                for p1 in points:
                        for p2 in pointsOfStreetAdjacent:
                                d1 =distance.distance(p2,p1).meters
                                if min > d1:
                                        min = d1
                                        minPoint = p1
                tmp = []
                tmp.append(minPoint)
                return minPoint

        def utilWay(self, streetAdjacents, theCommunicator):
                cad = "(is-util-way {"+streetAdjacents+"})\n"
                resultFromScone = self.sendToScone(cad, theCommunicator)
                # if 'T' in resultFromScone:
                #     return True
                # else:
                #     return False
                return True

        def carInScene(self, possibleStreets, timeOut):
                ## llama al OCR
                return True

        def getNearStreet(self, estimatedPositionOfCar, possibleStreets):
                # nearStreets = []
                # for i in possibleStreets:
                #     cad = "(the-x-of-y {latitude} {"+i+"})"
                #     latitude = sendToScone(cad)
                #     cad = "(the-x-of-y {longitude} {"+i+"})"
                #     longitude = sendToScone(cad)
                #     coordinates = latitude + " " + longitude
                #     streetPosition = Point(coordinates)
                #     if distance.distance(streetPosition,estimatedPositionOfcar).meters <60:
                #         nearStreets.append(i)
                return True


class Server(Ice.Application):

    def run(self, argv):
        broker = self.communicator()
        servant = GetNextCameraServiceI()

        adapter = broker.createObjectAdapter("GetNextCameraServiceAdapter")
        proxy = adapter.add(servant, broker.stringToIdentity("GetNextCameraService"))

        print("'{0}'".format(proxy))
        sys.stdout.flush()

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0


server = Server()
sys.exit(server.main(sys.argv))
