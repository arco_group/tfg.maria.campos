#!/usr/bin/python -u
# -*- coding: utf-8 -*-

import sys, time
import Ice, IceStorm
import threading

from civitas_slice import Civitas, SemanticModel, SconeServicePrx


class VehicleTrackerServiceI(SemanticModel.Service,Civitas.PlateSink, Civitas.ControlPointSink):
        threads = list()
        platesLastTimeSeen = {}
        #TO-FIX: what if more than one plate.
        seenPlate=""
        seenPlateByCameraId=""
        def sendToGetNextCamera(self, cad, theCommunicator):

                proxy = theCommunicator.stringToProxy("sconeService -t -e 1.1:tcp -h 161.67.27.95 -p 9091") #el proxy lo vas a tener que meter como cadena scone = SconeWrapper.SconeServicePrx.checkedCast(proxy)

                getNextCameraPrx = SemanticModel.GetNextCameraServicePrx.uncheckedCast(proxy)

                if not getNextCameraPrx:
                        raise RuntimeError('Invalid proxy')
                answer = getNextCameraPrx.performAction(action, ThingDict, theSink)
                return answer


        def sendToScone(self, cad, theCommunicator):

                proxy = theCommunicator.stringToProxy("sconeService -t -e 1.1:tcp -h 161.67.27.95 -p 9091")
                sconePrx = SconeServicePrx.uncheckedCast(proxy)
                if not sconePrx:
                        raise RuntimeError('Invalid proxy')
                if '\n' in cad:
                        answer = sconePrx.sconeRequest(cad)
                else:
                        cad = cad+'\n'
                        answer = sconePrx.sconeRequest(cad)
                return answer
                

        def parseSconeResult(self, str):
            finalStr =[]
            if len(str) > 2:
                tmpStr = str.replace('NIL','').replace('(','').replace(')','').replace('{','').replace('\n','').split('}')
                for i in tmpStr:
                    if not i=='':
                        finalStr.append(i)
                return finalStr
            else:
                return str


        def getTheStreetOf(self, trafficLightId, theCommunicator):
                cad = "(the-x-of-y {streetlocation} {"+ trafficLightId+ "})\n"
                myStreet = self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                return myStreet[0]
        def getTheLatitudeOf(self, trafficLightId, theCommunicator):
                cad = "(the-x-of-y {latitude} {"+ trafficLightId+ "})\n"
                latitude = self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                return latitude[0]
        def getTheLongitudeOf(self, trafficLightId, theCommunicator):
                cad = "(the-x-of-y {longitude} {"+ trafficLightId+ "})\n"
                longitude = self.parseSconeResult(self.sendToScone(cad, theCommunicator))
                return longitude[0]


                
       
        def vehicleTracker(self,trafficLightId, timeStamp, theCommunicator):
            ## necesitamos saber  si el coche esta o no en la escena
            expectedStreets = []

            street = self.getTheStreetOf(trafficLightId, theCommunicator)
            print "Iniciando tracker a partir de ", trafficLightId, " ", timeStamp
            print "El control point de ", street

            proxy = theCommunicator.stringToProxy("GetNextCameraService -t -e 1.1:tcp -h 161.67.27.95 -p 9090")
            service = SemanticModel.ServicePrx.uncheckedCast(proxy)
            if not service:
                    raise RuntimeError('Invalid proxy')

            theThingDict = {}

            t = SemanticModel.Thing()
            t.type = SemanticModel.TypeCode.stringT
            t.value = street
            theThingDict["StreetName"] = t
            
            lat = SemanticModel.Thing()
            lat.type = SemanticModel.TypeCode.stringT
            lat.value = self.getTheLatitudeOf(trafficLightId, theCommunicator)
            theThingDict["latitude"] = lat
            
            lon = SemanticModel.Thing()
            lon.type = SemanticModel.TypeCode.stringT
            lon.value = self.getTheLongitudeOf(trafficLightId, theCommunicator)
            theThingDict["longitude"] = lon
            
            service.performAction('getNextCamera', theThingDict, None)

            while 1:
                    print self.platesLastTimeSeen
                    sys.stdout.flush()
                    time.sleep(5)
                    if int(time.time()) - self.platesLastTimeSeen[self.seenPlate] > 10:
                            
                            street = self.getTheStreetOf(self.seenPlateByCameraId, theCommunicator)
                            t.value = street
                            theThingDict["StreetName"] = t
            
                            lat.value = self.getTheLatitudeOf(self.seenPlateByCameraId, theCommunicator)
                            theThingDict["latitude"] = lat
            
                            lon.value = self.getTheLongitudeOf(self.seenPlateByCameraId, theCommunicator)
                            theThingDict["longitude"] = lon
                            print "Buscando el coche el calle ", street
                            service.performAction('getNextCamera', theThingDict, None)


        def carInScene(self, possibleStreets, timeOut):
                ## llama al OCR
                return True

        def getNearStreet(self, estimatedPositionOfCar, possibleStreets):
                # nearStreets = []
                # for i in possibleStreets:
                #     cad = "(the-x-of-y {latitude} {"+i+"})"
                #     latitude = sendToScone(cad)
                #     cad = "(the-x-of-y {longitude} {"+i+"})"
                #     longitude = sendToScone(cad)
                #     coordinates = latitude + " " + longitude
                #     streetPosition = Point(coordinates)
                #     if distance.distance(streetPosition,estimatedPositionOfcar).meters <60:
                #         nearStreets.append(i)
                return True        

        def plate(self, plateSink, cameraId, timeStamp, current=None):
                print "Plate ", plateSink
                print "CameraID ", cameraId.name
                print "TimeStamp ", timeStamp
 
                print "\n"
                self.seenPlate = plateSink
                self.seenPlateByCameraId = cameraId.name

                if plateSink in self.platesLastTimeSeen.keys():
                        self.platesLastTimeSeen.pop(plateSink)
                self.platesLastTimeSeen[plateSink] = timeStamp
                sys.stdout.flush()


        def traficLight(self, trafficLightId, status, timeStamp, current=None):
                print "TrafficLightID ", trafficLightId.name
                print "Status ", status
                print "TimeStamp ", timeStamp
                print "\n"
                print " Iniciando tracking the vehículo "
                sys.stdout.flush()
                theCommunicator=current.adapter.getCommunicator()

                t = threading.Thread(target=self.vehicleTracker, args=(trafficLightId.name, timeStamp,theCommunicator, ))
                self.threads.append(t)
                t.start()
                
                

class Server(Ice.Application):
    def get_topic_manager(self):
        key = 'IceStorm.TopicManager.Proxy'
        proxy = self.communicator().propertyToProxy(key)
        if proxy is None:
            print "property", key, "not set"
            return None

        print("Using IceStorm in: '%s'" % key)
        return IceStorm.TopicManagerPrx.checkedCast(proxy)

    def run(self, argv):
        topic_mgr = self.get_topic_manager()
        if not topic_mgr:
            print ': invalid proxy'
            return 2


        ic = self.communicator()
        vehicleTrackerServant = VehicleTrackerServiceI()

        adapter = ic.createObjectAdapter("VehicleTrackerServiceAdapter")
        subscriber = adapter.addWithUUID(vehicleTrackerServant)
        
        proxy = adapter.add(vehicleTrackerServant, ic.stringToIdentity("VehicleTrackerService"))
        print("'{0}'".format(proxy))
        sys.stdout.flush()

        topic_name = "Plates"
        qos = {}
        try:
            topic_plates = topic_mgr.retrieve(topic_name)
        except IceStorm.NoSuchTopic:
            topic_plates = topic_mgr.create(topic_name)

        topic_name = "ControlPoints"
        qos = {}
        try:
            topic_controlPoints = topic_mgr.retrieve(topic_name)
        except IceStorm.NoSuchTopic:
            topic_controlPoints = topic_mgr.create(topic_name)

        topic_plates.subscribe(qos,subscriber)
        topic_controlPoints.subscribe(qos, subscriber)
        print 'Waiting events...'

        adapter.activate()
        self.shutdownOnInterrupt()
        ic.waitForShutdown()

        topic_plates.unsubscribe(subscriber)
        topic_controlPoints.unsubscribe(subscriber)

        return 0

server = Server()
sys.exit(server.main(sys.argv))
