// -*- mode: c++; coding: utf-8 -*-

// #include <PropertyService.ice>
#include <Ice/BuiltinSequences.ice>

module SemanticModel {

    enum TypeCode {
      boolT,
      byteT,
      shortT,
      intT,
      longT,
      floatT,
      doubleT,
      stringT,
    };

    struct Thing {
	TypeCode type;
	Ice::ByteSeq value;
    };

    dictionary<string, Thing> ThingDict;

    interface Sink {
	void report(ThingDict e);
    };

    // interface Device extends PropertySetDef {
    interface Device  {
    };

    interface DeviceFactory {
	Device* make(string role);
    };

    interface Service{
	void performAction(string action, ThingDict params, Sink* theSink);
    };
};
