# -*- mode: python; coding: utf-8 -*-

import Ice

ice_slice_dir = Ice.getSliceDir() or "/usr/share/Ice/slice"
slice_path = "/usr/share/slice/civitas/civitas.ice"
Ice.loadSlice("{} -I{} --all".format(slice_path, ice_slice_dir))

from SemanticModel import ServicePrx, Thing, TypeCode

