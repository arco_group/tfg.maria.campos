#!/usr/bin/env python
#-*- coding: iso-8859-15 -*-
#Este archivo usa encoding: iso-8859-15
__author__ = 'Alejandro Julian Ferro Bejerano'

import OsmApi
from Estructura import NodoBusqueda
import math
import time
import heapq
import sys
import pickle

def main():
    #captura de datos de Ciudad Real
    #lista de elemntos ElementList[i]['data'] y ElementList[i]['type']
    ElementList =OsmApi.OsmApi().Map(-3.9524,38.9531,-3.8877,39.0086)
    NodeDicc={}
    WayList = {}
    NodeList = {}
    file = open ("/home/maria/repo/tfg.maria.campos/source/OSM/CiudadReal-osm.lisp", "w")
    for i in ElementList:
        if i['type'] == 'way' and isautilway(i):
            WayList[i['data']['id']]=dict(id= i['data']['id'],nombre= i['data']['tag']['name'],nodos= i['data']['nd'], oneway= i['data']['tag']['oneway'])
        else:
            if i['type'] == 'node':
                NodeList[i['data']['id']]=dict(id= i['data']['id'], lat= i['data']['lat'], lon = i['data']['lon'])
             
    n = WayList.values()
    colindantes = []

    for i in n:
        #print i['nodos'][0]
	ref = i['nodos'][0]
        print "latitud ", NodeList[ref]['lat']  , "del nodo " ,  i['nodos'][0]
	print "longitud ", NodeList[ref]['lon']  , "del nodo " ,  i['nodos'][0]
        buf = "(new-indv {%s} {street})\n" %i['nombre']
        file.write(buf.encode ('iso-8859-15'))
        buf1 = "(x-is-the-y-of-z {%s} {name} {%s})\n" %(i['nombre'],i['nombre'])
        file.write(buf1.encode ('iso-8859-15'))
        for currentStreetNodes in i['nodos']:
           for iterateStreet in WayList.values():
               if currentStreetNodes in iterateStreet['nodos'] and iterateStreet['nombre']!=i['nombre'] and iterateStreet['nombre'] not in colindantes:
                   #print iterateStreet['nombre']
                   colindantes.append(iterateStreet['nombre'])
                   buf2 = "(x-is-the-y-of-z {%s} {adjacent} {%s})\n" %(iterateStreet['nombre'],i['nombre'])
                   file.write(buf2.encode ('iso-8859-15'))
                   #print NodeList.get(i['id'])
           if len(colindantes)>1:
               buf3 = "(Traffic-direction of street %s: from %s to %s)\n" %(i['nombre'],colindantes[0],colindantes[len(colindantes)-1])
               file.write(buf3.encode ('iso-8859-15'))
           del colindantes[:]

           if i['oneway'] == 'yes':
               buf4 = "(x-is-the-y-of-z {oneway} {traffic-direction} {%s})\n" %i['nombre']
               file.write(buf4.encode ('iso-8859-15'))
           else:
               buf5 = "(x-is-the-y-of-z {twoways} {traffic-direction} {%s})\n" %i['nombre']
               file.write(buf5.encode ('iso-8859-15'))
         
    file.close()
   # print NodeList
    return 1

def maketree(searchnode,hashtable):
    id_padre = searchnode.getestado()['id']
    adyacentes = sucesores(hashtable[id_padre])
    for nodos in adyacentes:
        nodo_aux = getnode(nodos,hashtable)
        searchnode= NodoBusqueda(nodo_aux, id_padre,nodo_aux['calle'])
        heapq.heappush(monticulo,searchnode)


def sucesores(node):
    adyacentes = []
    for i in node['adyacentes']:
        adyacentes.insert(0,i.keys()[0])
    return adyacentes

def esfinal(node_start,node_end):
    if node_start['id'] == node_end['id'] :
        return True
    else:
        return False
    
def isautilway(way):
    value = False
    try:
        if way['data']['tag']['highway'] == 'pedestrian' or way['data']['tag']['highway'] == 'residential' or way['data']['tag']['highway'] == 'living_street' or way['data']['tag']['highway'] == 'motorway' or way['data']['tag']['highway'] == 'primary' or way['data']['tag']['highway'] == 'sencondary' or way['data']['tag']['highway'] == 'tertiary' or way['data']['tag']['highway'] == 'footway' or way['data']['tag']['highway'] == 'trunk':
            value = True
            if not ('name' in way['data']['tag']):
                way['data']['tag']['name']= ' '
            if not ('oneway' in way['data']['tag']):
                way['data']['tag']['oneway']= 'no'
            if way['data']['tag']['highway'] == 'pedestrian':
                way['data']['tag']['oneway']= 'no'
    except:
        pass
    return value

def getnode(idNode, FinalTable):
    if FinalTable.has_key(int(idNode)):
        return FinalTable[int(idNode)]
    else:
        print ("Identificador de nodo no válido")

def gettable(FinalTable):
    for nodes in FinalTable:
        print (str(nodes)+' - '+str(FinalTable[nodes])) 


def lefttorigth(way,HasTable,NodeDicc):
    ndList = way['nodos']
    
    for i in range(len(ndList)-1):
        node1 = NodeDicc[ndList[i]]
        node2= NodeDicc [ndList[i+1]]
        if HasTable.has_key(node1['id']) and HasTable[node1['id']].has_key('adyacentes'):
            HasTable[node1['id']]['adyacentes'].append(getdistance(node1,node2))
        else:
            HasTable[node1['id']]=dict(id= node1['id'],lat= node1['lat'],lon= node1['lon'], calle = way['nombre'],adyacentes=[getdistance(node1,node2)])
    return HasTable

def rigthtoleft(way,HasTable,NodeDicc):
    ndList = way['nodos'][::-1]
    for i in range(len(ndList)-1):
        node1 = NodeDicc[ndList[i]]
        node2= NodeDicc [ndList[i+1]]
        if HasTable.has_key(node1['id']) and HasTable[node1['id']].has_key('adyacentes'):
            HasTable[node1['id']]['adyacentes'].append(getdistance(node1,node2))
        else:
            HasTable[node1['id']]=dict(id= node1['id'],lat= node1['lat'],lon= node1['lon'], calle = way['nombre'],adyacentes=[getdistance(node1,node2)])
    return HasTable

def getdistance(Node1,Node2):
    ady = {}
    ady[Node2['id']]= dict(distancia= dist(Node1['lon'],Node1['lat'],Node2['lon'],Node2['lat']))
    return ady                                                                          

def merc_x(lon):
    r_major=6378137.000
    return r_major*math.radians(lon)
 
def merc_y(lat):
    if lat>89.5:lat=89.5
    if lat<-89.5:lat=-89.5
    r_major=6378137.000
    r_minor=6356752.3142
    temp=r_minor/r_major
    eccent=math.sqrt(1-temp**2)
    phi=math.radians(lat)
    sinphi=math.sin(phi)
    con=eccent*sinphi
    com=eccent/2
    con=((1.0-con)/(1.0+con))**com
    ts=math.tan((math.pi/2-phi)/2)/con
    y=0-r_major*math.log(ts)
    return y


#Distancia entre dos punto geograficos.
#Se obtiene sus proyecciones Mercator
#y la distancia euclidea entre ellas
def dist(p1lon,p1lat,p2lon,p2lat):
    x1=merc_x(p1lon)
    x2=merc_x(p2lon)
    y1=merc_y(p1lat)
    y2=merc_y(p2lat)
    return math.sqrt((x1-x2)**2+(y1-y2)**2)

def makehastable(WayList, NodeDicc):
    HasTable = {}
    for ways in WayList:
        if WayList[ways]['oneway'] == 'yes':
            HasTable.update(lefttorigth(WayList[ways], HasTable,NodeDicc))
        elif WayList[ways]['oneway'] == '-1':
            HasTable.update(lefttorigth(WayList[ways],HasTable,NodeDicc))
        else:
            HasTable.update(lefttorigth(WayList[ways],HasTable,NodeDicc))
            HasTable.update(rigthtoleft(WayList[ways],HasTable,NodeDicc))
    for i in NodeDicc:
        if not HasTable.has_key(i):
            HasTable[NodeDicc[i]['id']] = NodeDicc[i]
    return HasTable

if len(sys.argv) == 2:
    if sys.argv[1] == '-all':
        gettable(main())
    elif sys.argv[1].isdigit():
        print getnode(sys.argv[1],main())
    else:
        print('Introduzca los argumentos: "-all" o "identificador de nodo"')
if len(sys.argv) == 3:
    if sys.argv[1].isdigit() and sys.argv[2].isdigit():
        monticulo = []
        idnode_end = int(sys.argv[2])
        idnode_start =int(sys.argv[1])
        hashtable = main()
        node_start = getnode(idnode_start,hashtable)
        node_end = getnode(idnode_end,hashtable)
        searchnode= NodoBusqueda(node_start, None ,node_start['calle'])
        heapq.heappush(monticulo,searchnode)
        while not esfinal(node_start, node_end):
            inicio =time.time()
            maketree(heapq.heappop(monticulo),hashtable)
            final = time.time()
            print "%d %.16f" % (monticulo.__len__(),final-inicio)
            
else:
    print('La ejecución solo consta de un argumento: "-all" , "id de nodo" o de la opción "test [id de nodo] "')

