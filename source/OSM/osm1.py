#!/usr/bin/env python
#-*- coding:utf-8; -*-

import OsmApi
import sys
import unicodedata

def main():
    ElementList =OsmApi.OsmApi().Map(-3.9485,38.9673,-3.9106,39.0040)
    WayList = {}
    NodeList = {}
    file = open ("/home/maria/repo/tfg.maria.campos/source/Scone/Scone/0.8.22/kb/core-components/streetsInformation.lisp", "w")

    for i in ElementList:
        if i['type'] == 'way' and isautilway(i):
            WayList[i['data']['id']]=dict(id= i['data']['id'],nombre= i['data']['tag']['name'],nodos= i['data']['nd'], oneway= i['data']['tag']['oneway'], highway=i['data']['tag']['highway'])
        else:
            if i['type'] == 'node':
                NodeList[i['data']['id']]=dict(id= i['data']['id'], lat= i['data']['lat'], lon = i['data']['lon'])

    n = WayList.values()
    waysName =['Calle', 'Ronda', 'Avenida', 'Vereda', 'Carretera', 'Pasaje', 'Plaza', 'Grupo', 'Camino', 'Paseo', 'Autovía'.decode('utf-8'), 'Travesía'.decode('utf-8'), 'Puerta', 'Paseo', 'Residencial', 'Urbanización'.decode('utf-8'), 'Estación'.decode('utf-8'), 'Plazuela']
    insertedStreets =[]
    streetsName = []
    for i in n:
	if not i['nombre']== ' ':
		i['nombre'] = remove_accents(i['nombre'])
		notWayName=True
		streetName = i['nombre'].upper()
		newName = i['nombre'].upper()
		wayNameAlreadyExists = False
		for wayName in waysName:
		    if wayName in i['nombre']:
			newName = i['nombre'].replace(wayName,'')[1:]
			if newName not in streetsName:
 			    streetsName.append(newName)
			else:
			    wayNameAlreadyExists = True
			notWayName = False
                if notWayName:
		    streetName = "Calle "+ i['nombre']
		if streetName not in insertedStreets:
		    insertedStreets.append(streetName)
		    buf = "(new-indv {%s} {street})\n" %(streetName)
		    file.write(buf.encode ('utf-8'))
                    buf = "(new-is-a {%s} {%s})\n" %(streetName, i['highway'])
                    file.write(buf.encode ('utf-8'))
 		    buf = "(x-is-the-y-of-z {%s} {street name} {%s})\n" %(newName,i['nombre'])
		    file.write(buf.encode ('utf-8'))

		    if i['oneway'] == 'yes':
		        buf = "(x-is-the-y-of-z {oneway} {number of ways of} {%s})\n" %(streetName)
		        file.write(buf.encode ('utf-8'))
		    else:
		        buf = "(x-is-the-y-of-z {twoways} {number of ways of} {%s})\n" %(streetName)
		        file.write(buf.encode ('utf-8'))
	        else:
  		    buf = "(new-is-a {%s} {%s})\n" %(streetName, i['highway'])
	            file.write(buf.encode ('utf-8'))    
                    buf = "(x-is-a-y-of-z {%s-%s} {section of} {%s})\n" %(i['nombre'], i['nodos'][0],i['nombre'])
                    file.write(buf.encode ('utf-8'))    
                    streetName = streetName+'-'+str(i['nodos'][0])
                    if i['oneway'] == 'yes':
		        buf = "(x-is-the-y-of-z {oneway} {number of ways of} {%s})\n" %(streetName)
		        file.write(buf.encode ('utf-8'))
		    else:
		        buf = "(x-is-the-y-of-z {twoways} {number of ways of} {%s})\n" %(streetName)
		        file.write(buf.encode ('utf-8'))
	            
                for ref in i['nodos']:
                    buf = "(x-is-a-y-of-z {%s} {latitude} {%s})\n" %(NodeList[ref]['lat'],streetName)
                    file.write(buf.encode ('utf-8'))  
                    buf = "(x-is-a-y-of-z {%s} {longitude} {%s})\n" %(NodeList[ref]['lon'],streetName)
                    file.write(buf.encode ('utf-8'))  
    file.close()
    return 1
   
def remove_accents(data):
    s = ''.join((c for c in unicodedata.normalize('NFD',unicode(data)) if unicodedata.category(c) != 'Mn'))
    return s.decode()

def isautilway(way):
    value = False
    try:
        if way['data']['tag']['highway'] == 'pedestrian' or way['data']['tag']['highway'] == 'residential' or way['data']['tag']['highway'] == 'living_street' or way['data']['tag']['highway'] == 'motorway' or way['data']['tag']['highway'] == 'primary' or way['data']['tag']['highway'] == 'sencondary' or way['data']['tag']['highway'] == 'tertiary' or way['data']['tag']['highway'] == 'footway' or way['data']['tag']['highway'] == 'trunk':
            value = True
            if not ('name' in way['data']['tag']):
                way['data']['tag']['name']= ' '
            if not ('oneway' in way['data']['tag']):
                way['data']['tag']['oneway']= 'no'
            if way['data']['tag']['highway'] == 'pedestrian':
                way['data']['tag']['oneway']= 'no'
    except:
        pass
    return value

def gettable(FinalTable):
    for nodes in FinalTable:
        print (str(nodes)+' - '+str(FinalTable[nodes])) 

if len(sys.argv) == 2:
    if sys.argv[1] == '-all':
        gettable(main())
    elif sys.argv[1].isdigit():
        print getnode(sys.argv[1],main())
    else:
        print('Introduzca los argumentos: "-all" o "identificador de nodo"')
if len(sys.argv) == 3:
    if sys.argv[1].isdigit() and sys.argv[2].isdigit():
        monticulo = []
        idnode_end = int(sys.argv[2])
        idnode_start =int(sys.argv[1])
        hashtable = main()
        node_start = getnode(idnode_start,hashtable)
        node_end = getnode(idnode_end,hashtable)
        searchnode= NodoBusqueda(node_start, None ,node_start['calle'])
        heapq.heappush(monticulo,searchnode)
        while not esfinal(node_start, node_end):
            inicio =time.time()
            maketree(heapq.heappop(monticulo),hashtable)
            final = time.time()
            print "%d %.16f" % (monticulo.__len__(),final-inicio)
            
else:
    print('La ejecución solo consta de un argumento: "-all" , "id de nodo" o de la opci�n "test [id de nodo] "')
