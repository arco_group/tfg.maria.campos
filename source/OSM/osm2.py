#!/usr/bin/env python
#-*- coding:utf-8; -*-

import OsmApi
import sys
import unicodedata

def main():
    #lista de elemntos ElementList[i]['data'] y ElementList[i]['type']
    ElementList =OsmApi.OsmApi().Map(-3.9485,38.9673,-3.9106,39.0040)
    WayList = {}
    file = open ("/home/maria/repo/tfg.maria.campos/source/Scone/Scone/0.8.22/kb/core-components/streetsAdjacents.lisp", "w")

    for i in ElementList:
        if i['type'] == 'way' and isautilway(i):
            WayList[i['data']['id']]=dict(id= i['data']['id'],nombre= i['data']['tag']['name'],nodos= i['data']['nd'], oneway= i['data']['tag']['oneway'], highway=i['data']['tag']['highway'])

    n = WayList.values()
    adjacents = []
    insertedStreets = {}
    for i in n:
	  if not i['nombre'] == ' ':
		i['nombre'] = remove_accents(i['nombre'])
		for currentStreetNodes in i['nodos']:
		    for iterateStreetNodes in WayList.values():
		        if currentStreetNodes in iterateStreetNodes['nodos'] and iterateStreetNodes['nombre']!=i['nombre'] and iterateStreetNodes['nombre'] not in adjacents:
		            iterateStreetNodes['nombre'] = remove_accents(iterateStreetNodes['nombre'])
		            adjacents.append(iterateStreetNodes['nombre'])
			    if iterateStreetNodes['nombre'] == ' ':
			    	i['nombre'] = remove_accents(i['nombre'])
				buf = "(x-is-a-y-of-z {%s} {adjacent} {%s})\n" %("roundabout",i['nombre'])			
			    else:
			    	i['nombre'] = remove_accents(i['nombre'])
		                buf = "(x-is-a-y-of-z {%s} {adjacent} {%s})\n" %(iterateStreetNodes['nombre'],i['nombre'])
		            file.write(buf.encode ('utf-8'))
		if len(adjacents)>1:
                    if adjacents[0] ==' ':
                        streetFrom = remove_accents(i['nombre'])
                        streetTo = adjacents[len(adjacents)-1]
                        streetName = remove_accents(i['nombre'])
                    else:
                        if adjacents[len(adjacents)-1] == ' ':
                            streetFrom =	adjacents[0]
                            streetTo = remove_accents(i['nombre'])
                            streetName= remove_accents(i['nombre'])
                        else:
                            streetFrom = adjacents[0]
                            streetTo = adjacents[len(adjacents)-1]
                            streetName = remove_accents(i['nombre'])

                    if i['nombre'] not in insertedStreets.keys():
                        buf="(x-is-the-y-of-z {from %s to %s} {traffic direction of} {%s})\n" %(streetFrom, streetTo, streetName)
                        insertedStreets[i['nombre']] =  [streetFrom, streetTo]
                    else:
                        previousPairs=[]
                        previousPairs=insertedStreets[i['nombre']]
                        if previousPairs[1]== streetFrom:
                            buf="(x-is-the-y-of-z {from %s to %s} {traffic direction of} {%s})\n" %(previousPairs[0], streetTo, streetName)
                            insertedStreets[i['nombre']] = [previousPairs[0], streetTo]
                        else:
                            if previousPairs[0] == streetTo:
                                buf="(x-is-the-y-of-z {from %s to %s} {traffic direction of} {%s})\n" %(streetFrom, previousPairs[1], streetName)
                                insertedStreets[i['nombre']] = [streetFrom, previousPairs[1]]
                            else:
                                buf = '\n'
                
                    file.write(buf.encode ('utf-8'))                      
		del adjacents[:]
    file.close()
    return 1
   
   
def remove_accents(data):
    s = ''.join((c for c in unicodedata.normalize('NFD',unicode(data)) if unicodedata.category(c) != 'Mn'))
    return s.decode(('utf-8'))
   
def isautilway(way):
    value = False
    try:
        if way['data']['tag']['highway'] == 'pedestrian' or way['data']['tag']['highway'] == 'residential' or way['data']['tag']['highway'] == 'living_street' or way['data']['tag']['highway'] == 'motorway' or way['data']['tag']['highway'] == 'primary' or way['data']['tag']['highway'] == 'sencondary' or way['data']['tag']['highway'] == 'tertiary' or way['data']['tag']['highway'] == 'footway' or way['data']['tag']['highway'] == 'trunk':
            value = True
            if not ('name' in way['data']['tag']):
                way['data']['tag']['name']= ' '
            if not ('oneway' in way['data']['tag']):
                way['data']['tag']['oneway']= 'no'
            if way['data']['tag']['highway'] == 'pedestrian':
                way['data']['tag']['oneway']= 'no'
    except:
        pass
    return value

def gettable(FinalTable):
    for nodes in FinalTable:
        print (str(nodes)+' - '+str(FinalTable[nodes])) 

if len(sys.argv) == 2:
    if sys.argv[1] == '-all':
        gettable(main())
    elif sys.argv[1].isdigit():
        print getnode(sys.argv[1],main())
    else:
        print('Introduzca los argumentos: "-all" o "identificador de nodo"')
if len(sys.argv) == 3:
    if sys.argv[1].isdigit() and sys.argv[2].isdigit():
        monticulo = []
        idnode_end = int(sys.argv[2])
        idnode_start =int(sys.argv[1])
        hashtable = main()
        node_start = getnode(idnode_start,hashtable)
        node_end = getnode(idnode_end,hashtable)
        searchnode= NodoBusqueda(node_start, None ,node_start['calle'])
        heapq.heappush(monticulo,searchnode)
        while not esfinal(node_start, node_end):
            inicio =time.time()
            maketree(heapq.heappop(monticulo),hashtable)
            final = time.time()
            print "%d %.16f" % (monticulo.__len__(),final-inicio)
            
else:
    print('La ejecución solo consta de un argumento: "-all" , "id de nodo" o de la opcion "test [id de nodo] "')
