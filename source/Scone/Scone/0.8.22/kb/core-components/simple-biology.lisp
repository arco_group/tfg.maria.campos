;;; -*- Mode:Lisp -*-
;;; ***************************************************************************
;;; Minimal intuitive knowledge of biology and medicine for the core
;;; Scone knowledge base.
;;;
;;; Author & Maintainer: Scott E. Fahlman
;;; ***************************************************************************
;;; Copyright (C) 2010, Scott E. Fahlman.

;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;   http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing,
;;; software distributed under the License is distributed on an "AS
;;; IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
;;; express or implied.  See the License for the specific language
;;; governing permissions and limitations under the License.

;;; The Scone engine may incoporate some fragments of the NETL2
;;; system, developed by Scott E. Fahlman for IBM Coporation between
;;; June 2001 and May 2003.  IBM holds the copyright on NETL2 and has
;;; made that software available to the author under the Apache 2 Open
;;; Source license.

;;; Development of Scone from 2003-2008 was supported in part by the
;;; Defense Advanced Research Projects Agency (DARPA) under contract
;;; numbers NBCHD030010 and FA8750-07-D-0185. Additional support for
;;; Scone development has been provided by generous research grants
;;; from Cisco Systems Inc. and from Google Inc.

;;; Any opinions, findings and conclusions or recommendations
;;; expressed in this material are those of the author and do not
;;; necessarily reflect the views of DARPA or our other sponsors.

;;; ***************************************************************************

;;; NOTE: This is meant to be the stuff that you will usually want around for
;;; any "common sense" application.  It knows about animals, plants, gender,
;;; sickness, etc.  For more specialized scientific/medical applictions you
;;; would load a much more detailed file on top of this one.

(new-complete-split-subtypes
 {animate}
 '({animal} {plant})
 :english "living thing")

(new-complete-split-subtypes
 {animate}
 '(({mature} :adj "full-grown")
   ({immature} :adj "juvenile")))

;;; Sex and Gender

(new-complete-split-subtypes
 {animate}
 '(({sexually reproducing} :adj)
   ({asexually reproducing} :adj)))

(new-complete-split-subtypes
 {sexually reproducing}
 '(({male} :adj-noun)
   ({female} :adj-noun)))

(new-type-role {parent} {sexually reproducing}
	       {sexually reproducing}
	       :n 2)

(new-indv-role {mother} {sexually reproducing}
	       {parent})

(new-is-a {mother} {female})

(new-indv-role {father} {sexually reproducing}
	       {parent})

(new-is-a {father} {male})

(new-type-role {offspring} {sexually reproducing}
	       {sexually reproducing}
	       :may-have t)

(new-type-role {son} {sexually reproducing}
	       {offspring}
	       :may-have t)

(new-is-a {son} {male})

(new-type-role {daughter} {sexually reproducing}
	       {offspring}
	       :may-have t)

(new-is-a {daughter} {female})


;;; Habitat

(new-complete-split-subtypes
 {animal}
 '(({flying} :adj)
   ({non-flying} :adj )))

(new-complete-split-subtypes
 {animal}
 '(({water-dwelling} :adj "aquatic")
   ({land-dwelling} :adj "terrestrial")
   ({amphibious} :adj)))

(new-complete-split-subtypes
 {animal}
 '(({air-breathing} :adj)
   ({water-breathing} :adj)))

(new-is-a {land-dwelling} {air-breathing})
(new-is-a {water-dwelling} {water-breathing})


;;; Taxonomy.

(new-complete-split-subtypes {animal}
			     '({vertebrate} {invertebrate}))

(new-is-a {vertebrate} {sexually reproducing})

(new-complete-split-subtypes {vertebrate}
			     '({fish} {amphibian} {reptile} {bird} {mammal}))

(new-is-a {fish} {water-dwelling})
(new-is-a {amphibian} {amphibious})
(new-is-a {reptile} {land-dwelling})
(new-is-a {bird} {land-dwelling})
(new-is-a {mammal} {land-dwelling})

(new-type {water-dwelling mammal} {mammal})
(new-is-not-a {water-dwelling mammal} {land-dwelling})
(new-is-a {water-dwelling mammal} {water-dwelling})
(new-is-not-a {water-dwelling mammal} {water-breathing})
(new-is-a {water-dwelling mammal} {air-breathing})

(new-is-a {fish} {non-flying})
(new-is-a {amphibian} {non-flying})
(new-is-a {reptile} {non-flying})
(new-is-a {bird} {flying})
(new-is-a {mammal} {non-flying})

;;; Define a few animal types, just as an example.
(new-split-subtypes {mammal}
		    '({elephant} {lion} {tiger} {bat} {monkey} {person}
		      {pig} {horse} {cow} {donkey} {dog} {cat} {whale}))

(new-is-not-a {bat} {non-flying})
(new-is-a {bat} {flying})

;; Got to say explicitly that whale is not land-dwelling.  That's implied by
;; {water-dwelling mammal}, but we lose the race because of the direct parent
;; link to {mammal}.
(new-is-not-a {whale} {land-dwelling})
(new-is-a {whale} {water-dwelling mammal})

(new-split-subtypes {bird}
		    '({canary} {eagle} {crow} {penguin}))

(new-is-not-a {penguin} {flying})
(new-is-a {penguin} {non-flying})

(new-split-subtypes {fish}
		    '({shark} {tuna} {perch} {goldfish} {flying-fish}))

(new-is-not-a {flying-fish} {non-flying})
(new-is-a {flying-fish} {flying})

;;; Diseases

(new-type {disease} {intangible})

(new-complete-split-subtypes
 {disease}
 '(({infectious} :adj)
   ({non-infectious} :adj)))

(new-indv-role {pathogen} {infectious}
	       {animate})

;;; Define a few diseases.
(new-members {infectious}
	     '({influenza} {smallpox} {anthrax}))

(new-members {non-infectious}
	     '({scurvy} {cystic fibrosis} {melanoma}))

(new-complete-split-subtypes
 {animate}
 '(({sick} :adj "ill" "diseased")
   ({healthy} :adj)))

(new-is-a {sick} {bad health condition})
(new-is-a {healthy} {good health condition})

(new-indv-role {affliction} {sick} {disease}
	       :english '(:role "malady"))
