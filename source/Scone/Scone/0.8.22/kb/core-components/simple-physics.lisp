;;; -*- Mode:Lisp -*-
;;; ***************************************************************************

;;; Minimal intuitive physics and chemistry knowledgfe for the core
;;; Scone knowledge base.
;;;
;;; Author & Maintainer: Scott E. Fahlman
;;; ***************************************************************************
;;; Copyright (C) 2010, Scott E. Fahlman.

;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;   http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing,
;;; software distributed under the License is distributed on an "AS
;;; IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
;;; express or implied.  See the License for the specific language
;;; governing permissions and limitations under the License.

;;; The Scone engine may incoporate some fragments of the NETL2
;;; system, developed by Scott E. Fahlman for IBM Coporation between
;;; June 2001 and May 2003.  IBM holds the copyright on NETL2 and has
;;; made that software available to the author under the Apache 2 Open
;;; Source license.

;;; Development of Scone from 2003-2008 was supported in part by the
;;; Defense Advanced Research Projects Agency (DARPA) under contract
;;; numbers NBCHD030010 and FA8750-07-D-0185. Additional support for
;;; Scone development has been provided by generous research grants
;;; from Cisco Systems Inc. and from Google Inc.

;;; Any opinions, findings and conclusions or recommendations
;;; expressed in this material are those of the author and do not
;;; necessarily reflect the views of DARPA or our other sponsors.

;;; ***************************************************************************

;;; NOTE: This is very minimal for now.

;;; Every physical object has a mass.
(new-indv-role {object mass} {physical object}
	       {mass measure}
	       :english '(:role "mass" "weight"))

;;; Every physical object also has some dimensions.
(new-indv-role {object height} {physical object}
	       {length measure}
	       :english '(:role "height"))

(new-indv-role {object length} {physical object}
	       {length measure}
	       :english '(:role "length"))

(new-indv-role {object width} {physical object}
	       {length measure}
	       :english '(:role "width" "breadth"))

(new-indv-role {object volume} {physical object}
	       {volume measure}
	       :english '(:role "volume"))

(new-indv-role {object area} {physical object}
	       {area measure}
	       :english '(:role "area" "footprint"))

;;; More about materials.

(new-split-subtypes
 {material}
 '(({solid} :adj-noun)
   ({liquid} :adj-noun)
   ({gas} "vapor" :adj "gaseous" "vaporized")
   ({plasma})))

;;; A favorite material.
(new-type {water} {material})
(new-intersection-type {ice} '({water} {solid}) :english "water ice")
(new-intersection-type {liquid water} '({water} {liquid}) :english "water")
(new-intersection-type {steam} '({water} {gas}) :english "water vapor")









