;;; -*- Mode:Lisp -*-
;;; ***************************************************************************
;;; Model of space and spatial relations for the core Scone knowledge base.
;;;
;;; Author & Maintainer: Scott E. Fahlman
;;; ***************************************************************************
;;; Copyright (C) 2010, Scott E. Fahlman.

;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;   http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing,
;;; software distributed under the License is distributed on an "AS
;;; IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
;;; express or implied.  See the License for the specific language
;;; governing permissions and limitations under the License.

;;; The Scone engine may incoporate some fragments of the NETL2
;;; system, developed by Scott E. Fahlman for IBM Coporation between
;;; June 2001 and May 2003.  IBM holds the copyright on NETL2 and has
;;; made that software available to the author under the Apache 2 Open
;;; Source license.

;;; Development of Scone from 2003-2008 was supported in part by the
;;; Defense Advanced Research Projects Agency (DARPA) under contract
;;; numbers NBCHD030010 and FA8750-07-D-0185. Additional support for
;;; Scone development has been provided by generous research grants
;;; from Cisco Systems Inc. and from Google Inc.

;;; Any opinions, findings and conclusions or recommendations
;;; expressed in this material are those of the author and do not
;;; necessarily reflect the views of DARPA or our other sponsors.

;;; ***************************************************************************

;;; NOTE: This is very minimal for now.  Ideally, we need to integrate
;;; this with a module that reasons about 2D and 3D maps and images.
;;; I'm pretty sure that human spatial reasoning is not all symbolic.
;;; But that's a big task -- probably a Ph.D. thesis for some really
;;; smart student.

;;; "Place" is used in a few ways: geographical area, point in 3-space, etc.

(new-type {place} {thing}
	  :english '("place" "location" "position"))

;;; The concept "place" includes both abstract spaces, like "Canada" and
;;; tangible objects such as "my house".

(new-intersection-type {tangible place}
		       '({tangible} {place}))
(new-intersection-type {intangible place}
		       '({intangible} {place}))

;;; A geographical-area is a region on earth.
;;; We'll worry about other planets later.

(new-type {geographical area} {intangible place}
	  :english '("area" "location" "geographical area"))

(new-split-subtypes {geographical area}
		    '(({land region} "land")
		      ({water region} "water")
		      ({air region} "air")
		      ({space region} "space")
		      {mixed-type region}))

;;; Define some relations over geographical areas.

(new-relation {area near}
	      :a-inst-of {geographical area}
	      :b-inst-of {geographical area}
	      :symmetric t
	      :english '(:relation "near"))

(new-relation {area adjacent to}
	      :a-inst-of {geographical area}
	      :b-inst-of {geographical area}
	      :symmetric t
	      :english '(:relation "adjacent to"))

(new-relation {area contains}
	      :a-inst-of {geographical area}
	      :b-inst-of {geographical area}
	      :english '(:relation "contains"))

(new-relation {same area as}
	      :a-inst-of {geographical area}
	      :b-inst-of {geographical area}
	      :symmetric t)

(new-relation {greater area than}
	      :a-inst-of {geographical area}
	      :b-inst-of {geographical area}
	      :english '(:relation "larger than"
			 "greater area than"))

(new-relation {area more populous than}
	      :a-inst-of {geographical area}
	      :b-inst-of {geographical area}
	      :english '(:relation "larger than"
			 "more populous than"))

(new-relation {located at}
	      :a-inst-of {physical object}
	      :b-inst-of {place})
