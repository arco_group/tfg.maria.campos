(defun is-util-way (str)
  (let ((str-elem (lookup-element str)))
    (if (or 
	 (eq (is-x-a-y? str-elem {residential}) ':YES)
	 (eq (is-x-a-y? str-elem {roundabout}) ':YES)
	 (eq (is-x-a-y? str-elem {living_street}) ':YES))
	t
	nil)))
	 
