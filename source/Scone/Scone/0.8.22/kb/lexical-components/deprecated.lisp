;;; List of WordNet elements (from synsets) that we want to mark as
;;; :deprecated, rendering them inoperative in testing for checkables
;;; violations.

{iodine.n.01}
{helium.n.01}
{he.n.02}       ; Hebrew letter
{cad.n.01}                
{stallion.n.01}

