;;; This file contains definitions for a selection of conceptual metaphors and
;;; metonyms.

(new-cm-pattern
 {CM poverty is disease}
 '(:x-is-a-y {poverty} {disease})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)

(new-cm-pattern
 {CM poverty is a place}
 '(:x-is-a-y {poverty} {place})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)

;; This one is so general we don't know if it's about ineqaulity.

(new-cm-pattern
 {CM intangible grips}
 '(:s-v {intangible} {grip.v.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.4)

(new-cm-pattern
 {CM poverty grips}
 '(:s-v {poverty} {grip.v.01})
 :specializes {CM intangible grips}
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)

(new-cm-pattern
 {CM intangible buries}
 '(:s-v {intangible} {bury.v.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :other
 :target-domain-confidence 0.4)

(new-cm-pattern
 {CM taxes bury}
 '(:s-v {tax.n.01} {bury.v.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.9)

(new-cm-pattern
 {CM intangible restrains}
 '(:s-v {intangible} {restrain.v.03})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :other
 :target-domain-confidence 0.4)

(new-cm-pattern
 {CM finance restrains}
 '(:s-v {finance.n.01} {restrain.v.03})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.8)

(new-cm-pattern
 {CM fall into financial condition}
 '(:v-o {fall_into.v.01} {financial_condition.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.9)
 
(new-cm-pattern
 {CM organization has foundation}
 '(:x-has-y {organization} {foundation})
 :strength 0.6
 :target-position 2
 :source-position 1
 :source-target-confidence 0.9
 :target-domain :other
 :target-domain-confidence 0.5)


;;; ---------------------------------------------------------------------------
;;; Amos

; e.g. (:ENTITY_IS_ENTITY "education" "route")
(new-cm-pattern
  {CM education is a path}
  '(:x-is-a-y {education} {way.n.06})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. (:ENTITY_IS_ENTITY "degree" "path")
(new-cm-pattern
  {CM academic degree is a path}
  '(:x-is-a-y {academic_degree.n.01} {way.n.06})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. (:ENTITY_HAS_MODIFIER "poverty" "mind-numbing")
(new-cm-pattern
  {CM bodily condition describes poverty}
  '(:x-modifies-y {bodily condition} {poverty})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; making a CM pattern out of this because the concept of "burden"
; includes words like "dead weight" and "pill," which would also form
; a metaphor when equated to poverty
;
; also, there are similar such metaphors in Spanish, so this CM would
; catch that as well
;


(new-cm-pattern
  {CM condition of poverty}
  '(:x-has-y {poverty} {condition.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence -1.0)


; I tried implementing CM patterns in the mets-eng-econ files
; directly... they're a bit too general, as you said
(new-cm-pattern
  {CM poverty is confinement}
  '(:x-is-a-y {poverty} {confinement.n.02})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "cure poverty"
(new-cm-pattern
  {CM help poverty}
  '(:v-o {help.v.02} {poverty})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)

#|

; e.g. (:ACTION_HAS_OBJECT "spread" "opportunity")
(new-cm-pattern
  {CM spread possibility}
  '(:v-o {spread.v.01} {possibility.n.02})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. (:ACTION_HAS_OBJECT "spread" "misery")
(new-cm-pattern
  {CM spread happiness condition}
  '(:v-o {spread.v.01} {happiness condition})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


(new-cm-pattern
  {CM spread of happiness condition}
  '(:x-has-y {happiness condition} {spread.v.01})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)

|#

; e.g. "alleviate hunger"
(new-cm-pattern
  {CM alleviate physiological state}
  '(:v-o {relieve.v.01} {drive.n.09})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.8)


; e.g. "alleviate misery"
(new-cm-pattern
  {CM alleviate form of existence}
  '(:v-o {relieve.v.01} {condition})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.8)


; e.g. "tax burden"
(new-cm-pattern
  {CM economic condition describes burden}
  '(:x-modifies-y {economic condition} {burden.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "alleviate income inequality"
(new-cm-pattern
  {CM alleviate economic condition}
  '(:v-o {relieve.v.01} {economic condition})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "alleviate inequality"
(new-cm-pattern
  {CM alleviate difference}
  '(:v-o {relieve.v.01} {difference.n.01})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. "alleviate vulnerability"
(new-cm-pattern
  {CM alleviate weakness}
  '(:v-o {relieve.v.01} {weakness.n.03})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. "alleviate misery", "alleviate suffering"
(new-cm-pattern
  {CM alleviate misery}
  '(:v-o {relieve.v.01} {misery.n.01})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)

(new-cm-pattern
  {CM soften difference}
  '(:v-o {soften} {difference.n.01})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.8)


(new-cm-pattern
  {CM physical act on weakness}
  '(:v-o {physical action} {weakness.n.03})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)

; burden 
(new-cm-pattern
  {CM condition is burden}
  '(:x-is-y {condition} {load.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. (:ENTITY_HAS_ENTITY "misery" "burden")
(new-cm-pattern
  {CM burden of condition}
  '(:x-has-y {condition} {load.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)

; e.g. "taxation is a burden"
(new-cm-pattern
  {CM economic condition is a burden}
  '(:x-is-a-y {economic condition} {load.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; in Spanish, ENTITY_HAS_ENTITY lists poverty second
;
; e.g. (:ENTITY_HAS_ENTITY "lastre" "pobreza")
(new-cm-pattern
  {CM Spanish burden of economic condition}
  '(:x-has-y {load.n.01} {economic condition})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; in English, ENTITY_HAS_ENTITY lists poverty first
;
; e.g. (:ENTITY_HAS_ENTITY "poverty" "burden")
(new-cm-pattern
  {CM English burden of economic condition}
  '(:x-has-y {economic condition} {load.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


#|

;;; Might keep this in a tighter form, but "distribute wealth socred as
;;; a false positive.
;;;
;;; tried it on the nomets file, didn't get any false positives, but that may
;;; be due to the limited number of examples there
; e.g. "spread education" or "spread poverty"
(new-cm-pattern
  {CM spread economic condition}
  '(:v-o {spread.v.01} {economic condition})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)



; e.g. "spread of poverty"
(new-cm-pattern
  {CM spread of economic condition}
  '(:x-has-y {economic condition} {spread.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)

|#


; e.g. "abyss of poverty"
(new-cm-pattern
  {CM place of economic condition}
  '(:x-has-y {economic condition} {place})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "tax strangles"
(new-cm-pattern
  {CM economic condition physically acts}
  '(:s-v {economic condition} {physical action})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)

;;; Caused a false positive on "distribute wealth".
;;; See if we can tighen this.  Or maybe "distribute" is not
;;; a physical act.
;;;
;;; It is actually triggering the pattern {CM economic condition is physical entity},
;;; while detecting {stagger.v.03} as the source... I have no idea why, since {stagger.v.03}
;;; is an intangible
; e.g. "defend public education"
(new-cm-pattern
  {CM physical acts on economic condition}
  '(:v-o {physical action} {economic condition})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)

;;; Copying some of the {economic condition} CM patterns for {education}

(new-cm-pattern
  {CM spread education}
  '(:v-o {spread.v.01} {education})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.7)


(new-cm-pattern
  {CM spread of education}
  '(:x-has-y {education} {distribution.n.02})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.7)


(new-cm-pattern
  {CM education physically acts}
  '(:s-v {education} {physical action})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.7)


(new-cm-pattern
  {CM physical acts on education}
  '(:v-o {physical action} {education})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.7)






; e.g. (:ENTITY_HAS_ENTITY "misery" "weight")
(new-cm-pattern
  {CM weight of condition}
  '(:x-has-y {condition} {weight.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. (:ENTITY_HAS_ENTITY "taxation" "weight")
(new-cm-pattern
  {CM English weight of economic condition}
  '(:x-has-y {economic condition} {weight.n.01})
  :specializes {CM weight of condition}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "robbery of class"
(new-cm-pattern
  {CM larceny of economic condition}
  '(:x-has-y {economic condition} {larceny.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "demise of poverty"
(new-cm-pattern
  {CM death of economic condition}
  '(:x-has-y {economic condition} {death.n.04})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


(new-cm-pattern
  {CM periodic event of condition}
  '(:x-has-y {condition} {periodic_event.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. "tide of prosperity", "cycle of poverty"
; {economic_condition.n.01} ???
(new-cm-pattern
  {CM periodic event of economic condition}
  '(:x-has-y {economic condition} {periodic_event.n.01})
  :specializes {CM periodic event of condition}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


(new-cm-pattern
  {CM condition is periodic event}
  '(:x-is-y {condition} {periodic_event.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. "poverty is a cycle"
(new-cm-pattern
  {CM economic condition is periodic event}
  '(:x-is-y {economic condition} {periodic_event.n.01})
  :specializes {CM condition is periodic event}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "ladder of opportunity"
(new-cm-pattern
  {CM possibility has physical entity}
  '(:x-has-y {possibility.n.02} {physical_entity.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)



(new-cm-pattern
  {CM economic abstraction has structure}
  '(:x-has-y {economic abstraction} {structure.n.01})
;  :specializes {CM abstraction has physical entity}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


(new-cm-pattern
  {CM economic abstraction has path}
  '(:x-has-y {economic abstraction} {way.n.06})
;  :specializes {CM abstraction has physical entity}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "cornerstone of taxation"
(new-cm-pattern
  {CM economic abstraction has building material}
  '(:x-has-y {economic abstraction} {building_material.n.01})
;  :specializes {CM abstraction has physical entity}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "blight of poverty"
(new-cm-pattern
  {CM economic condition has decay}
  '(:x-has-y {economic condition} {decay.n.04})
;  :specializes {CM abstraction has decay}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "stranglehold of misery"
(new-cm-pattern
  {CM power of abstraction}
  '(:x-has-y {abstraction.n.06} {power.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. "stranglehold of poverty"
(new-cm-pattern
  {CM power of economic condition}
  '(:x-has-y {economic condition} {power.n.01})
  :specializes {CM power of abstraction}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)

;; Causes many false positives.
(new-cm-pattern
  {CM physical action of condition}
  '(:x-has-y {condition} {physical action})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. "struggle of poverty"
(new-cm-pattern
  {CM physical action of economic condition}
  '(:x-has-y {economic condition} {physical action})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "collapse of education"
(new-cm-pattern
  {CM physical event of abstraction}
  '(:x-has-y {abstraction.n.06} {physical event})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. "collapse of the middle class"
(new-cm-pattern
  {CM physical event of economic condition}
  '(:x-has-y {economic condition} {physical event})
  :specializes {CM physical event of abstraction}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "education betrays"
(new-cm-pattern
  {CM abstraction deceives}
  '(:s-v {abstraction.n.06} {deceive.v.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


(new-cm-pattern
  {CM economic condition deceives}
  '(:s-v {economic condition} {deceive.v.01})
  :specializes {CM abstraction deceives}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "poverty path", "poverty trap"
(new-cm-pattern
  {CM economic condition describes physical entity}
  '(:x-modifies-y {economic condition} {physical_entity.n.01})
;  :specializes {CM abstraction describes physical entity}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "education pipeline"
(new-cm-pattern
  {CM education describes physical entity}
  '(:x-modifies-y {education} {physical_entity.n.01})
;  :specializes {CM abstraction describes physical entity}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "alleviate gap [of poverty]"
(new-cm-pattern
  {CM alleviate physical distance}
  '(:v-o {relieve.v.01} {physical distance})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


(new-cm-pattern
  {CM alleviate situation}
  '(:v-o {relieve.v.01} {situation.n.02})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. "alleviate struggle"
(new-cm-pattern
  {CM alleviate conflict}
  '(:v-o {relieve.v.01} {conflict.n.01})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. "alleviate oppression"
(new-cm-pattern
  {CM alleviate relationship}
  '(:v-o {relieve.v.01} {relationship.n.03})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


(new-cm-pattern
  {CM pamper abstraction}
  '(:v-o {pamper.v.01} {abstraction.n.06})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. "coddle the rich"
(new-cm-pattern
  {CM pamper economic condition}
  '(:v-o {pamper.v.01} {economic condition})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "education betrays"
(new-cm-pattern
  {CM education deceives}
  '(:s-v {education} {deceive.v.01})
  :specializes {CM abstraction deceives}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.7)


;;; ---------------------------------------------------------------------------
;;; Thomson


;; V3 CHANGES:
;; no change to the CMs I had written in v2.
;;
;; all new stuff was appended (see below).

(new-cm-pattern
 {CM opportunity has scales}
 '(:x-has-y {opportunity.n.01} {scale.n.02})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.6)

(new-cm-pattern
 {CM equality is balance}
 '(:x-is-y {social equality} {balance.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)

(new-cm-pattern
 {CM inequality is distance}
 '(:x-is-y {social inequality} {physical distance})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)

(new-cm-pattern
 {CM launch people}
 '(:v-o {launch} {person})
 :strength 0.8
 :target-position 2
 :source-position 1
 :source-target-confidence 1.0
 :target-domain :other
 :target-domain-confidence 0.5)

(new-cm-pattern
 {CM launch workers}
 '(:v-o {launch} {worker})
 :strength 0.9
 :target-position 2
 :source-position 1
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.9)

(new-cm-pattern
 {CM suppress growth}
 '(:v-o {physically suppress} {economic growth})
 :strength 1.0
 :target-position 2
 :source-position 1
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.7)

#|
;;; Might keep this in some form, but "Question inequality" was scored as a
;;; false positive.  Tighten?
(new-cm-pattern
 {CM address intangible}
 '(:v-o {address.v.01} {intangible})
 :strength 0.9 ; I don't know about this one... arguably the figurative use is so common that it's not even a metaphor.
 :target-position 2
 :source-position 1
 :source-target-confidence 1.0
 :target-domain :other
 :target-domain-confidence 0.8)
|#

(new-cm-pattern
 {CM food is fiscal}
 '(:x-modifies-y {fiscal} {food})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.6)

(new-cm-pattern
 {CM social status is ladder}
 '(:x-is-a-y {socioeconomic condition} {ladder.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.7)

(new-cm-pattern 
 {CM social class is animal}
 '(:x-is-a-y {social class} {animal})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.8)

(new-cm-pattern
 {CM money moves}
 '(:s-v {money} {move.v.02})
 :strength 0.6 ; Only a metaphor if we consider "move" distinct from "be moved"
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :other
 :target-domain-confidence 0.5)

(new-cm-pattern
 {CM social class moves}
 '(:s-v {social class} {move.v.02})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :other
 :target-domain-confidence 0.5)

(new-cm-pattern
 {CM social class acts toward social class}
 '(:s-v-o {social class} {action} {social class})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.8)

(new-cm-pattern
 {CM social class moves to or from social class}
 '(:s-v-o {social class} {move in relation to} {social class})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)

(new-cm-pattern
 {CM move to or from social class} ; actually, I guess this captures the previous one too
 '(:v-o {move in relation to} {social class})
 :strength 1.0
 :target-position 2
 :source-position 1
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)

(new-cm-pattern
 {CM mobility is socioeconomic} ; pretty conventionalized, but might as well
 '(:x-modifies-y {economic} {mobility.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.9)

(new-cm-pattern
 {CM rigidity is socioeconomic}
 '(:x-modifies-y {economic} {rigidity})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)

(new-cm-pattern
 {CM motion is socioeconomic}
 '(:x-modifies-y {economic} {motion.n.06})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.9)

(new-cm-pattern
 {CM division is unhealthy}
 '(:x-modifies-y {unhealthy.a.01} {division.n.03})
 :strength 1.0
 :target-position 2
 :source-position 1
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.9)

(new-cm-pattern
 {CM income mobility}
 '(:x-modifies-y {income.n.01} {mobility.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.9)

(new-cm-pattern
 {CM social class has health}
 '(:x-has-y {social class} {health condition})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)

(new-cm-pattern
 {CM economy has health}
 '(:x-has-y {economy.n.01} {health condition})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.5)

(new-cm-pattern
 {CM social class ails}
 '(:s-v {social class} {suffer.v.06})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)

(new-cm-pattern
 {CM economy ails}
 '(:s-v {economy.n.01} {suffer.v.06})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.5)

(new-cm-pattern
 {CM social class is ailed}
 '(:v-o {ail} {social class})
 :strength 1.0
 :target-position 2
 :source-position 1
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.9)

(new-cm-pattern
 {CM economy is ailed}
 '(:v-o {ail} {economy.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.5)

(new-cm-pattern
 {CM social class is place}
 '(:x-is-a-y {social class} {place})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.8)

(new-cm-pattern
 {CM leave social class}
 '(:v-o {leave.v.01} {social class})
 :strength 1.0
 :target-position 2
 :source-position 1
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)

(new-cm-pattern
 {CM drive inequality}
 '(:v-o {drive.v.01} {social inequality})
 :strength 1.0
 :target-position 2
 :source-position 1
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)

(new-cm-pattern
 {CM government chooses}
 '(:s-v {gov-as-state} {choose.v.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :other
 :target-domain-confidence 0.6)

(new-cm-pattern
 {CM government acts on social class}
 '(:s-v-o {gov-as-state} {action} {social class})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.8)

(new-cm-pattern
 {CM opinion is device}
 '(:x-is-a-y {opinion.n.01} {instrumentality.n.03})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :other
 :target-domain-confidence 0.6)

(new-cm-pattern
 {CM stagnation is socioeconomic}
 '(:x-modifies-y {economic} {stagnation.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.6)

(new-cm-pattern
 {CM nation moves to or from social condition}
 '(:s-v-o {state.n.04} {move in relation to} {socioeconomic condition})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.5)

(new-cm-pattern
 {CM tap social class}
 '(:v-o {draw from} {social class})
 :strength 1.0
 :target-position 2
 :source-position 1
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.9)

(new-cm-pattern
 {CM economy has competitors}
 '(:x-has-y {economy.n.01} {competitor})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.8) ; written with "winners and losers" in mind

(new-cm-pattern
 {CM ladder is socioeconomic}
 '(:x-modifies-y {economic} {ladder.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.6)

(new-cm-pattern
 {CM economy has ladder}
 '(:x-has-y {economy.n.01} {ladder.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.6)

;;; ---------------------------------------------------------------------------
;;; John

(new-cm-pattern
 {CM poverty is physical object}
  '(:x-is-a-y {poverty} {physical object})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
#|
 ; not sure about metaphor
 (new-cm-pattern
 {CM poverty is portrait}
  '(:x-is-a-y {poverty} {portrait.n.02})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0
 :specializes {CM poverty is physical object})
|#

(new-cm-pattern
  {CM poverty is living thing}
  '(:x-is-a-y {poverty} {animate})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 (new-cm-pattern
  {CM poverty is fight}
  '(:x-is-a-y {poverty} {fight})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 ; not sure about this metaphor...
 (new-cm-pattern
  {CM poverty is victim}
  '(:x-is-a-y {poverty} {victim})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 (new-cm-pattern
  {CM poverty is physical condition}
  '(:x-is-a-y {poverty} {physical_condition.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 (new-cm-pattern
  {CM poverty is a deception}
  '(:x-is-a-y {poverty} {deception.n.02})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 (new-cm-pattern
  {CM taxation is a deception}
  '(:x-is-a-y {taxation.n.03} {deception.n.02})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 (new-cm-pattern
  {CM taxation is fight}
  '(:x-is-a-y {taxation.n.03} {fight})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 (new-cm-pattern
  {CM taxation is crime}
  '(:x-is-a-y {taxation.n.03} {crime.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
  (new-cm-pattern
  {CM taxation is physical condition}
  '(:x-is-a-y {taxation.n.03} {physical_condition.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 (new-cm-pattern
 {CM taxation is disease}
 '(:x-is-a-y {taxation.n.03} {disease})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0
 :specializes {CM taxation is physical condition})
 
 ; iffy metaphor
 (new-cm-pattern
 {CM taxation is low point}
 '(:x-is-a-y {taxation.n.03} {low point})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 (new-cm-pattern
 {CM poverty is low point}
 '(:x-is-a-y {poverty} {low point})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 ;not entirely sure about this metaphor
  (new-cm-pattern
 {CM taxation is body of water}
 '(:x-is-a-y {taxation.n.03} {water area})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
  (new-cm-pattern
 {CM taxation is a tool}
 '(:x-is-a-y {taxation.n.03} {implement.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 (new-cm-pattern
 {CM taxation is confinement}
 '(:x-is-a-y {taxation.n.03} {confinement})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0
 :specializes {CM taxation is physical condition})
 
 (new-cm-pattern
 {CM access to education is a fight}
 '(:x-is-a-y {access to education} {fight})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
; {game.n.01}
 (new-cm-pattern
 {CM access to education is a game}
 '(:x-is-a-y {access to education} {game})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)

#|
 (new-cm-pattern
 {CM wealth is physical object}
 '(:x-is-a-y {wealth.n.01} {physical object})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
|#
 
  (new-cm-pattern
 {CM wealth is living organism}
 '(:x-is-a-y {wealth.n.01} {animate})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
  (new-cm-pattern
 {CM wealth is fight}
 '(:x-is-a-y {wealth.n.01} {fight})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 (new-cm-pattern
 {CM wealth is physical location}
 '(:x-is-a-y {wealth.n.01} {place})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
#|
 (new-cm-pattern
 {CM wealth is person}
 '(:x-is-a-y {wealth.n.01} {person})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0
 :specializes {CM wealth is living organism})
|#
 
 (new-cm-pattern
 {CM social class is physical location}
 '(:x-is-a-y {class.n.03} {place})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
(new-cm-pattern
 {CM social class is living organism}
 '(:x-is-a-y {class.n.03} {animate})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
(new-cm-pattern
 {CM social class is fight}
 '(:x-is-a-y {class.n.03} {fight})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 ;not sure what this metaphor is about
(new-cm-pattern
 {CM social class is low point}
 '(:x-is-a-y {class.n.03} {low point})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
(new-cm-pattern
 {CM social class is physical object}
 '(:x-is-a-y {class.n.03} {physical object})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
(new-cm-pattern
 {CM social class is journey}
 '(:x-is-a-y {class.n.03} {journey.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
(new-cm-pattern
 {CM social class is physical injury}
 '(:x-is-a-y {class.n.03} {injury.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
(new-cm-pattern
 {CM social class is measure}
 '(:x-is-a-y {class.n.03} {measure})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
(new-cm-pattern
 {CM socio-economic class is a low point}
 '(:x-is-a-y {socio-economic class} {low point})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
(new-cm-pattern
 {CM socio-economic class is an abyss}
 '(:x-is-a-y {socio-economic class} {chasm.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
(new-cm-pattern
 {CM socio-economic class is a machine}
 '(:x-is-a-y {socio-economic class} {machine})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)

;;-----------------
;; John (Farsi-inspired)

 (new-cm-pattern
 {CM social class dies}
 '(:s-v {social class} {die.v.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 (new-cm-pattern
 {CM height of inequality}
 '(:x-has-y {inequality.n.01} {height.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence .5)

(new-cm-pattern
 {CM root of inequality}
 '(:x-has-y {inequality.n.01} {ancestor.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence .5)
 
(new-cm-pattern
 {CM wealth has gap}
 '(:x-has-y {wealth.n.01} {opening.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 (new-cm-pattern
 {CM income has gap}
 '(:x-has-y {income.n.01} {opening.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 (new-cm-pattern
 {CM education has gap}
 '(:x-has-y {education} {opening.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 (new-cm-pattern
 {CM poverty has gateway}
 '(:x-has-y {poverty} {movable_barrier.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
(new-cm-pattern
 {CM poverty is swamp}
 '(:x-is-y {poverty} {wetland.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
(new-cm-pattern
 {CM poverty has size}
 '(:x-has-y {poverty} {size.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
(new-cm-pattern
 {CM poverty is trap}
 '(:x-is-y {poverty} {trap.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
(new-cm-pattern
 {CM poverty is puzzle}
 '(:x-is-y {poverty} {puzzle.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
(new-cm-pattern
 {CM poverty is poison}
 '(:x-is-y {poverty} {poison.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
(new-cm-pattern
 {CM education is road}
 '(:x-is-y {education} {road.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)

;;; --------------------------
;;; John (more)

; Economic slumps tend to hit those struggling
 (new-cm-pattern
 {CM economy slumps}
 '(:s-v {economy.n.01} {sag.v.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)

 ; For instance, the state's excise tax eats up 11 percent of the income
 (new-cm-pattern
 {CM tax eats}
 '(:s-v {taxation.n.03} {consume.v.02})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 ; have generally not tackled inequality in the world head on
(new-cm-pattern
 {CM tackle inequality}
 '(:v-o {tackle.v.03} {inequality.n.01})
 :strength 1.0
 :target-position 2
 :source-position 1
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.5)
 
 ; But the state can choose to mute the impact of the invisible hand
(new-cm-pattern
 {CM economy is a hand}
 '(:x-is-a-y {economy.n.01} {hand.n.01})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 1.0)
 
 ; the human suffering caused by selfish economic policies
(new-cm-pattern
 {CM selfish policy}
 '(:x-modifies-y {selfish.a.01} {policy.n.01})
 :strength 1.0
 :target-position 2
 :source-position 1
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.5)


; e.g. "life is a lottery"
(new-cm-pattern
  {CM life is gambling}
  '(:x-is-a-y {life} {gambling.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.7)


; e.g. "life's lottery"
(new-cm-pattern
  {CM life's gambling}
  '(:x-has-y {life} {gambling.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.7)


; e.g. "inequality soars"
(new-cm-pattern
  {CM inequality flies}
  '(:s-v {social inequality} {fly.v.03})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


(new-cm-pattern
  {CM physical phenomenon of economic abstraction}
  '(:x-has-y {economic abstraction} {physical_phenomenon.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "currency inertia"
(new-cm-pattern
  {CM economic abstraction describes}
  '(:x-modifies-y {economic abstraction} {physical_phenomenon.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "anemic [economic] recovery"
(new-cm-pattern
  {CM physically weak transformation}
  '(:x-modifies-y {anemic.s.01} {transformation.n.01})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)

(new-cm-pattern
  {CM physical acts on economic abstraction}
  '(:v-o {physical action} {economic abstraction})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)

; I don't think any of the other definitions for "weak" cut it -- they
; are all either physical, or they describe a downward trend in prices,
; and the economy is much more than just the price of things
;
; e.g. "weak economy"
(new-cm-pattern
  {CM physically weak economic abstraction}
  '(:x-modifies-y {weak.a.01} {economic abstraction})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


(new-cm-pattern
  {CM physically strong economic abstraction}
  '(:x-modifies-y {strong.a.01} {economic abstraction})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)

; e.g. "economic destruction"
(new-cm-pattern
  {CM economic describes termination}
  '(:x-modifies-y {economic.a.01} {termination.n.05})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "tax strangles"
(new-cm-pattern
  {CM economic abstraction physically acts}
  '(:s-v {economic abstraction} {physical action})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)

#|
; +2 false negatives, +11 true positives.
(new-cm-pattern
  {CM physical acts on condition}
  '(:v-o {physical action} {condition})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)
|#

; e.g. "gap [between the rich and poor] yawns [wider than ...]"
(new-cm-pattern
  {CM physical distance physically acts}
  '(:s-v {physical distance} {physical action})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


(new-cm-pattern
  {CM economic abstraction describes mobility}
  '(:x-modifies-y {economic abstraction} {mobility.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "stagnant [wage] levels"
(new-cm-pattern
  {CM stagnant describes degree}
  '(:x-modifies-y {stagnant.s.02} {degree.n.01})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "wages have been flat"
(new-cm-pattern
  {CM flat describes economic abstraction}
  '(:x-modifies-y {flat.s.01} {economic abstraction})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "economic stagnation"
(new-cm-pattern
  {CM economic abstraction describes inaction}
  '(:x-modifies-y {economic abstraction} {inaction.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "political life"
(new-cm-pattern
  {CM abstraction describes life}
  '(:x-modifies-y {abstraction.n.06} {life.n.02})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. "healthy community", "healthy democracy"
(new-cm-pattern
  {CM healthy describes place}
  '(:x-modifies-y {healthy} {place})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. "healthy democracy"
(new-cm-pattern
  {CM healthy describes organization}
  '(:x-modifies-y {healthy} {organization})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. "healthy economy"
(new-cm-pattern
  {CM healthy describes economic abstraction}
  '(:x-modifies-y {healthy} {economic abstraction})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "[how much the] top guys [have]"
(new-cm-pattern
  {CM top describes person}
  '(:x-modifies-y {top.a.01} {person.n.01})
;  :specializes {CM abstraction describes physical entity}
  ; not always a metaphor, I suppose; can sometimes literally mean
  ; the guy with a high elevation
  :strength 0.8
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.8)


; doesn't work; not sure why, so replaced with pattern below
;
; e.g. "the economy created [19 million new] jobs"
(new-cm-pattern
  {CM economy creates activity}
  '(:s-v-o {economy.n.01} {make.v.03} {activity.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  ; it's about the economy for sure, but about economic inequality in particular?
  :target-domain-confidence 0.8)


#|
;;; false positive for "the economy is getting better" ("get" as in {draw.v.15})

; e.g. "the economy created [19 million new jobs]"
(new-cm-pattern
  {CM economy creates}
  '(:s-v {economy.n.01} {make.v.03})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  ; it's about the economy for sure, but about economic inequality in particular?
  :target-domain-confidence 0.8)
|#



; e.g. "[the effects on the] civic fabric"
(new-cm-pattern
  {CM civic describes physical entity}
  '(:x-modifies-y {civic.a.01} {physical_entity.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


(new-cm-pattern
  {CM income describes distribution}
  '(:x-modifies-y {income.n.01} {distribution.n.02})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


(new-cm-pattern
  {CM socially describes fluid}
  '(:x-modifies-y {socially.r.01} {fluid.s.02})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


(new-cm-pattern
  {CM economically describes fluid}
  '(:x-modifies-y {economically.r.03} {fluid.s.02})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


(new-cm-pattern
  {CM chorus of greed}
  '(:x-has-y {greed.n.01} {chorus.n.01})
;  :specializes {CM chorus of abstraction}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.9)



; e.g. "the tall masts of the national chains [reach skywards]"
(new-cm-pattern
  {CM mast of restaurant chains}
  '(:x-has-y {chain.n.04} {mast.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.8)

#|
;;; might not need to tighten, since it detects (:v-o "complete" "school") as a metaphor...
;;; :v-o isn't even :x-is-a-y!

; e.g. "education is road"
(new-cm-pattern
  {CM education is physical entity}
  '(:x-is-a-y {education} {physical_entity.n.01})
  :specializes {CM abstraction is physical entity}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)
|#


; e.g. "unhealthy division"
(new-cm-pattern
  {CM unhealthy describes physical distance}
  '(:x-modifies-y {unhealthy.a.01} {physical distance})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; e.g. "unhealthy economy"
(new-cm-pattern
  {CM unhealthy describes economic abstraction}
  '(:x-modifies-y {unhealthy.a.01} {economic abstraction})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "taxes are the price [we pay for civilized society]"
(new-cm-pattern
  {CM taxation is price}
  '(:x-is-a-y {tax} {price.n.03})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "falling wages"
(new-cm-pattern
  {CM dropping economic abstraction}
  '(:x-modifies-y {dropping.s.01} {economic abstraction})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)



; e.g. "the Millennium summit set [eight key development targets]"
(new-cm-pattern
  {CM summit determines}
  '(:s-v {summit.n.03} {determine.v.03})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :other
  :target-domain-confidence 0.5)


; e.g. "rising wages"
(new-cm-pattern
  {CM rising economic abstraction}
  '(:x-modifies-y {rising.a.01} {economic abstraction})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; detects the CM pattern {physical acts on abstraction} instead...
; e.g. "hit a new record"
(new-cm-pattern
  {CM hit a new record}
  '(:x-modifies-y {strike.v.04} {record.n.06})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :other
  :target-domain-confidence 0.5)


; e.g. "concentration of poverty"
(new-cm-pattern
  {CM concentration of economic abstraction}
  '(:x-has-y {economic abstraction} {concentration.n.02})
;  :specializes {CM concentration of abstraction}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "the concentration of too many dollars [in the hands of the few]"
(new-cm-pattern
  {CM concentration of currency}
  '(:x-has-y {currency.n.01} {concentration.n.02})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "[The deficit] threatens our future"
(new-cm-pattern
  {CM endanger time}
  '(:v-o {endanger.v.01} {time.n.05})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.8)


; e.g. "tax hike"
(new-cm-pattern
  {CM economic abstraction describes motion}
  '(:x-modifies-y {economic abstraction} {motion.n.06})
;  :specializes {CM abstraction describes motion}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)

; e.g. "pockets of consumers"
(new-cm-pattern
  {CM consumers have containers}
  '(:x-has-y {consumer.n.01} {container.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.9)


; e.g. "high taxes", "big income"
(new-cm-pattern
  {CM physical description describes economic abstraction}
  '(:x-modifies-y {physical description} {economic abstraction})
;  :specializes {CM physical description describes abstraction}
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


(new-cm-pattern
  {CM abstraction is rubric}
  '(:x-is-a-y {abstraction.n.06} {rubric.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


(new-cm-pattern
  {CM economic abstraction is structure}
  '(:x-is-a-y {economic abstraction} {structure.n.01})
  :specializes {CM abstraction is rubric}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "rising inequality is a rubric"
(new-cm-pattern
  {CM economic abstraction is rubric}
  '(:x-is-a-y {economic abstraction} {rubric.n.01})
  :specializes {CM abstraction is rubric}
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "reverse the tides"
(new-cm-pattern
  {CM figuratively reverse periodic event}
  '(:v-o {change_by_reversal.v.01} {periodic_event.n.01})
  :strength 1.0
  :target-position 1
  :source-position 2
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.6)


; once again, can't get this s-v-o pattern to work, and if not all three are together
; like this, I think literal interpretations are perfectly plausible.
;
; e.g. "smoking and obesity turn on class"
(new-cm-pattern
  {CM organic process depends on economic abstraction}
  '(:s-v-o {organic_process.n.01} {depend_on.v.01} {economic abstraction})
  :strength 1.0
  :target-position 3
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 1.0)


; e.g. "vicious cycle"
(new-cm-pattern
  {CM barbarous describes periodic event}
  '(:x-modifies-y {barbarous.s.01} {periodic_event.n.01})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.8)

; e.g. "breeding antagonism"
(new-cm-pattern
  {CM breed emotion}
  '(:v-o {breed.v.03} {emotion})
  :strength 1.0
  :target-position 2
  :source-position 1
  :source-target-confidence 1.0
  :target-domain :economic-inequality
  :target-domain-confidence 0.7)

(new-cm-pattern
 {CM education is a granter of access}
 '(:x-is-a-y {education.n.01} {item for access})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.8)


(new-cm-pattern
 {CM degree is a granter of access}
 '(:x-is-a-y {academic degree} {item for access})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.8)



(new-cm-pattern
 {CM poverty is an institution}
 '(:x-is-a-y {poverty} {institution.n.02})
 :strength 1.0
 :target-position 1
 :source-position 2
 :source-target-confidence 1.0
 :target-domain :economic-inequality
 :target-domain-confidence 0.9)
