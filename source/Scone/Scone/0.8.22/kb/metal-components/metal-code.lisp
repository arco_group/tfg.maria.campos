;;; -*- Mode:Lisp -*-
;;; ***************************************************************************
;;; Code specific to the METAL (IARPA METAPHOR) Project.
;;;
;;; Author & Maintainer: Scott E. Fahlman
;;; ***************************************************************************
;;; Copyright (C) 2003-2013, Scott E. Fahlman
;;;
;;; The Scone software is made available to the public under the
;;; Apache 2.0 open source license.  A copy of this license is
;;; distributed with the software.  The license can also be found at
;;;
;;; http://www.apache.org/licenses/LICENSE-2.0.
;;;
;;; Unless required by applicable law or agreed to in writing,
;;; software distributed under the License is distributed on an "AS
;;; IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
;;; express or implied.  See the License for the specific language
;;; governing permissions and limitations under the License.
;;;
;;; Development of Scone since January, 2012, has been supported in
;;; part by the Intelligence Advanced Research Projects Activity
;;; (IARPA) via Department of Defense US Army Research Laboratory
;;; contract number W911NF-12-C-0020.
;;;
;;; Development since February 2013 has been supported in part by the
;;; U.S. Office of Naval Research under award number N000141310224.
;;;
;;; Development of Scone from 2003 through 2008 was supported in part
;;; by the Defense Advanced Research Projects Agency (DARPA) under
;;; contract numbers NBCHD030010 and FA8750-07-D-0185.
;;;
;;; Additional support for Scone development has been provided by
;;; generous research grants from Cisco Systems Inc. and from Google
;;; Inc.
;;;
;;; The U.S. Government is authorized to reproduce and distribute
;;; reprints for Governmental purposes notwithstanding any copyright
;;; annotation thereon.
;;;
;;; Disclaimer: The views and conclusions contained herein are those
;;; of the authors and should not be interpreted as necessarily
;;; representing the official policies or endorsements, either
;;; expressed or implied, of IARPA, DoD/ARL, ONR, DARPA, the
;;; U.S. Government, or any of our other sponsors.
;;; ***************************************************************************

;;; Forms parallel to ENGLISH for other METAL languages.

;;; %%% NOTE: This is a quick-and-dirty kludge, soon to be replaced.
;;; For now, there is a SPANISH function, but no :SPANISH keyword in
;;; the various element-creating functions.  All the SPANISH function
;;; does, for now, it to stuff the Spanish lexical items into the same
;;; hash-table as the English ones.  Same for Farsi and Russian,
;;; though they will not be confused with English lexical items, since
;;; the alphabets are different.

(defun spanish (element &rest r)
  (apply #'english (cons element r)))

(defun russian (element &rest r)
  (apply #'english (cons element r)))

(defun farsi (element &rest r)
  (apply #'english (cons element r)))

;;; ---------------------------------------------------------------------------
;;; Set up linking variables for language and culture.

(defparameter *current-language* nil
  "The language of the text currently being processed.")

(defparameter *current-culture* nil
  "The culture within which to process the current text.")

(defparameter *language-token-map* nil
  "A list, each of whose items is a sublist with three elements: an
   ISO language token (as a keyword), a Scone node for some {human language}, and
   a Scone node for some {human culture}.")

(defparameter *named-entity-type-map* nil
  "A list, each of whose items is a sublist with two elements: a
   named-entity-type keywords, such as :person, followed by a Scone node
   for some that type, such as {person}.")

;;; Counters used to generate unique ID-tags for sentence and checkable
;;; structures.

(defparameter *system-start-time* (get-internal-real-time)
  "A large integer related to the time when this system was started,
   in milliseconds.  Used to create system-unique-IDs.")

(defparameter *sentence-counter* 0
  "This counter is incremented each time we generate a unique ID for a
   sentence.")

(defparameter *checkable-counter* 0
  "This counter is incremented each time we generate a unique ID for a
   checkable.")

;;; Global variables used by SCONE-M4-DETECT and friends.

(defparameter *sentence-structure-list*
  "Each sentence we analyze creates a sentence-structure that is stored
   in this list, most-recent first.")

(defparameter *pattern-evidence-threshold* 0.4
  "If the strength of the best detected CM-pattern is above this threshold
   call it a metaphor.")

;;; ---------------------------------------------------------------------------
;;; Miscellaneous Utilities

;;; TEST-FORMS lets you safely try any sequence of Scone or Lisp
;;; commands to SEE if this sequence causes an error.  Used to see if
;;; newly-created large structues have any imcompatibility that would
;;; signal a non-literal usage.  For smaller "checkables" we use
;;; simpler, more local tests.

(defun test-forms (forms)
  "Takes a list FORMS with any number of forms to evaluate.  These
   will normally be commands to create new structures in the Scone
   knowledge base. We attempt to create these, returning T if
   successful, NIL if any error is signalled, but we suppress actually
   going into the debugger).  In any case, we roll back the KB to the
   status quo ante before returning."
  (let ((success nil)
	(rollback *last-element*))
    (unwind-protect
	 ;; Don't let any errors pop us into the debugger.
	 (ignore-errors
	   ;; Eval all the forms, in order.
	   (dolist (f forms)
	     (eval f))
	   ;; If we made it to here without an error, success.
	   (setq success t))
      ;; Whatever happens, roll back the KB to where it was when we
      ;; entered.
      (remove-elements-after rollback)
      success)))

;;; Random small utility.  Dependency graphs are unreadable unless
;;; pprinted.

(defun pprint-file (filename)
  "Read contents of the specified file in the current KB area.  PPrint
   each form to standard output."
  (let* ((pathname
	  (merge-pathnames (pathname filename)
			   *default-kb-pathname*))
	 (stream (open pathname
				 :direction :input
				 :external-format :utf8)))
    (do ((form (read stream nil :eof) (read stream nil :eof)))
	((eq form :eof) 'done)
      (pprint form)
      (format t "~%"))))


;;; ---------------------------------------------------------------------------
;;; Stuff to modify the knowledge base by ingesting entries from WordNet or other
;;; lexical resources and filtering out problematic entries.

;;; New types created by NEW-WORDNET-TYPE, defined below, get this set
;;; of features by default.  Normally this is a list of keywords.

(defparameter *wordnet-default-features* nil
  "A list of features (keywords) that will be added to each new
   WordNet-derived type created by NEW-WORDNET-TYPE.")

;;; Form for creating new WordNet-derived concepts in Scone

(defun new-wordnet-type (iname
			 parent
			 &key (features *wordnet-default-features*))
   "Create a new type-node in Scone, based on a WordNet synset.  The
   INAME is something like {dog.n.01} and the parent is some other
   wordnet or native-scone existing type-node either from WordNet or
   native Scone.

   WordNet just calls NEW-TYPE (maybe someday we'll figure out which
   ones should be individuals), but it also can add some properties to
   the synset-derived type-node.  A property is just a Lisp keyword.
   If the user does not supply properties, we use the ones in
   *WORDNET-DEFAULT-FEATURES*.

   This function also suppresses adding the INAME as one of the
   English names for this concept."
   ;; Skip duplicates.  Create the new type-node.
   (unless (lookup-element iname)
     ;; Add the features for this new type-node.
     (let ((e (new-type iname parent :english :no-iname)))
       (dolist (f features)
	 (set-element-property e f))
       e)))

(defun new-wordnet-verb (iname
			 parent
			 &key
			 (features *wordnet-default-features*))
  "Create a new action-type structure in Scone, based
   on a WordNet synset.  The INAME is something like {run.v.01} and
   the PARENT is some other wordnet or native-scone type-node."
  (let ((e (new-type iname parent :english :no-iname)))
    ;; Make sure it's an action.
    (unless (eq :yes (is-x-a-y? e *action*))
      (new-is-a e *action*))
    ;; Add the features.
    (dolist (f features)
      (set-element-property e f))
    e))

;;; Read a file of element internal names (in curly braces) that we
;;; want to mark with the :deprecated tag.  These will be inoperative
;;; in when looking for checkable violations.

(defun mark-deprecated-elements ()
  "Read the file \"lexical-components/deprecated.lisp\".  Each line is
   the internal name of a WordNet element (synset) that will get the
   :deprecated tag, rendering it inoperative when looking for
   checkable violations."
  (let* ((pathname (merge-pathnames
		    (pathname "lexical-components/deprecated.lisp")
		    *default-kb-pathname*))
	 (stream (open pathname
		       :direction :input
		       :external-format :utf8))
	 (counter 0))
    (loop
	(let ((iname (read stream nil :eof)))
	  (cond ((eq iname :eof)
		 (commentary "~&Marked ~D elements as deprecated.~%"
			     counter)
		 (force-output *terminal-io*)
		 (return-from mark-deprecated-elements nil))
		((setq iname (lookup-element iname))
		 (incf counter)
		 (set-element-property iname :deprecated)))))))


;;; Function to deprecate any KB element that can't help with finding
;;; preference-breaking because we don't know enough about it.  That
;;; is, there is no split-set above it.

(defun deprecate-useless-elements ()
  "Check for useless wordnet-derived elements in the KB, meaning those
   that have no split above them.  Mark these with the :deprecated
   property."
  ;; For all elements...
  (with-markers (m1)
    (let ((count 0))
    (do-elements (e)
      (let ((element-ok nil))
	;; Check only WordNet-derived concept (synset) nodes.
	(when (and (get-element-property e :wordnet)
		   (not (get-element-property e :deprecated)))
	  ;; Mark E and all superior classes with M1.
	  (upscan e m1)
	  ;; For each of these, see if they are attached to a split.
	  (do-marked (superior m1)
	    (when (incoming-split-wires superior)
	      (setq element-ok t)))
	  ;; If we didn't find a split, mark E as useless.
	  (unless element-ok
	    (incf count)
	    (set-element-property e :deprecated)))))
    (format t "~&;; Found and deprecated ~D useless elements.~%" count))))


;;; This is used to find self-contradictory situations caused by the mapping in
;;; various versions of the wordnet-map.lisp file.

(defun test-map-file (filename)
  "Read in a map file containing (mostly) NEW-EQ and NEW-IS-A
   calls. For each proposed new link, run SPLIT-VIOLATIONS-BELOW? on
   the elements being linked.  If there's a problem, print a specific
   complaint.  If not, execute the function."
  (let* ((pathname (merge-pathnames (pathname filename)
				    *default-kb-pathname*))
	 (in-stream (open pathname
			  :direction :input
			  :external-format :utf8)))
    (commentary "~&;; Loading KB \"~A\"." filename)
    (force-output *terminal-io*)
    (loop
	(let ((form (read in-stream nil :eof)))
	  (cond ((eq form :eof)
		 (commentary "~&Load of \"~A\" completed.  ~D elements total.~%"
			     filename *n-elements*)
		 (force-output *terminal-io*)
		 (return-from test-map-file nil))
		((not (listp form))
		 (eval form))
		((or (eq (car form) 'new-is-a)
		     (eq (car form) 'new-eq))
		 (unless
		     (split-violations-below? (second form)
					      (third form))
		   (eval form)))
		(t (eval form)))))))


;;; ---------------------------------------------------------------------------
;;; Here is the machinery for creating and matching Conceptual
;;; Metaphor Patterns, or CM-patterns for short.

;;; These linking variables are defined in simple-episodic-model.lisp.
(declaim (special *action-object* *action-agent*))

;;; First some linking variables for the Scone nodes representing the
;;; various checkable types.  These variables get filled in
;;; by Scone elements created later when the "metal-kb.lisp" file is
;;; loaded.
(defparameter *x-is-y* nil)
(defparameter *x-is-a-y* nil)
(defparameter *x-modifies-y* nil)
(defparameter *x-has-y* nil)
(defparameter *x-is-the-y-of-z* nil)
(defparameter *s-v* nil)
(defparameter *v-o* nil)
(defparameter *s-v-o* nil)
(defparameter *x-at-location-y* nil)

;;; Create linking variables for the {cm pattern} type and its roles.
(defparameter *cm-pattern* nil)

(defparameter *cm-checkable-label* nil)
(defparameter *cm-x-type* nil)
(defparameter *cm-y-type* nil)
(defparameter *cm-z-type* nil)
(defparameter *cm-payload* nil)

(defparameter *cm-pattern-marker* nil
  "When loading the metal.lisp file, we allocate a marker for the duration
   and place it on all CM=-pattern elements.  The number of this marker
   goes here.")

(defparameter *desired-target-domain* :economic-inequality
  "This will be one of the target domains specified by the sponsor.  Currently
   the value must be one of :economic-inequality or :governance.  If specified
   we score potential LMs according to both inherent quality and fit within
   this domain.  If this parameter is NIL, we look only at inherent quality.")

(defparameter *metaphor-found-in-sentence* nil
  "This gets set if any of the checkables in a sentence finds a metaphor.")

(defparameter *sentences-with-metaphors* 0
  "This gets incremented when a sentence is found to have at least one
   metaphor.")

;;; Function to create a new CM-pattern and the associated meaning payload
;;; structure.

(defstruct payload
  strength
  target-position
  source-position
  source-target-confidence
  target-domain
  target-domain-confidence)

(defun new-cm-pattern (iname
		       checkable-pattern
		       &key
		       ;; If this CM-pattern specializes some other
		       ;; one, show the more general Cm_pattern here.
		       (specializes *cm-pattern*)
		       ;; :strength is 1.0 for definite metaphor, -1.0
		       ;; for definite literal, and 0.0 for no
		       ;; knowledge.
		       (strength 1.0)
		       ;; Indicate the positions of the source and
		       ;; target strings within the checkable, and our
		       ;; confidence that these are in the correct
		       ;; order.
		       (target-position 1)
		       (source-position 2)
		       (source-target-confidence 0.0)
		       ;; The taget-domain, one of
		       ;; :economic-inequality, :governance, and
		       ;; :other, and the confidence we have in
		       ;; this call.
		       (target-domain :economic-inequality)
		       (target-domain-confidence 0.5))
  "Create a new CM-pattern and the associated payload structure.  The
   CHECKABLE-PATTERN in this case is a list with one of the checkable-tag
   keywords as the first element, followed by two or three Scone
   concepts in curly-brace notation.  So it is something like (:S-V
   {building} {speak}).

   If the checkable being processed matches this pattern, we use the
   information in the payload structure to fill in the values required
   by the checkable-output structure.  If more than one CM-pattern
   matches we take the most specific one, and then the
   highest-strength one within that group.

   The keyword arguments are optional -- the user can supply some,
   all, or none of them.  If the caller provides only the checkable
   pattern, the default is to treat this as a definite metaphor (value
   1.0) in the :economic-inequality domain with moderate confidence
   0.5.  However, the choice of traget and source posiitons within the
   checkable is not made -- this is filled in later by code that uses
   defaults for the various checkable types."
  (when (setq checkable-pattern
	      (get-cp-elements checkable-pattern))
    (let* ((cm-element
	    (new-type iname specializes))
	   ;; Create the payload structure for this CM-pattern.
	   (payload
	    (make-payload
	     :strength strength
	     :target-position target-position
	     :source-position source-position
	     :source-target-confidence source-target-confidence
	     :target-domain target-domain
	     :target-domain-confidence target-domain-confidence)))
      ;; Fill in the Scone elements that are a part of this
      ;; CM-pattern.
      (x-is-the-y-of-z (first checkable-pattern)
		       *cm-checkable-label* cm-element)
      (x-is-the-y-of-z (second checkable-pattern)
		       *cm-x-type* cm-element)
      (x-is-the-y-of-z (third checkable-pattern)
		       *cm-y-type* cm-element)
      ;; Some checkable-labels take a Z argument, some don't.
      (when (fourth checkable-pattern)
	(x-is-the-y-of-z (fourth checkable-pattern)
			 *cm-z-type* cm-element))
      ;; And associate the payload structure with this CM-pattern.
      (x-is-the-y-of-z (new-struct payload)
		       *cm-payload*
		       cm-element)
      cm-element)))

(defun get-cp-elements (checkable-pattern)
  "Converts the first element -- the checkable label -- to a Scone concept-node.
   Then scans the remaining items in the pattern -- normally iname-elements --
   to Scone nodes.  If any of these do NOT correspond to currently-existing
   Scone concepts, print a comment to that effect and return NIL.  Else, return
   the list of Scone concept nodes representing this pattern."
  (let ((output nil)
	(temp nil))
    ;; Find the Scone element for the checkable keyword.
    (setq temp (get (first checkable-pattern) :scone-element))
    (unless temp
      (format t "~&~S unknown checkable type in CM-pattern definition.~%"
	      (first checkable-pattern))
      (return-from get-cp-elements nil))
    (push temp output)
    ;; For all other args, find the Scone element that is named by this
    ;; arg.  If there is no element defined, complain and go.
    (dolist (arg (cdr checkable-pattern))
      (setq temp (lookup-element arg))
      (unless temp
	(format t "~&~S unknown element in CM-pattern definition."
		arg)
	(return-from get-cp-elements nil))
      (push temp output))
    ;; Turn around OUTPUT list and return it.
    (nreverse output)))

;;; ---------------------------------------------------------------------------
;;; Definition of SCONE-M4-DETECT, the main entry point for the Scone
;;; detection/interpretation pipeline.

;;; SCONE-M4-DETECT will build a structure for each sentence, containing,
;;; among other things, a list of checkable structures, one for each
;;; checkable passed to us (possibly modified by splitting some, etc.)

;;; SCONE-M4-DETECT returns only the list of checkable-output structures.
;;; Processes external to Scone will combine the information from these
;;; checkable structures with information from other pipelines to get
;;; an overall decision about whether the sentence contains an on-topic
;;; metaphor, and which is best if there are more than one.

;; Define the structure we will create for the sentence as a whole.

(defstruct sentence-output
  "This structure is instantiated onceCreate one instance of this
   structure for each sentence.  It is used to record sentence-level
   information and to hold the list of checkables that contain the
   real results."
  ;; The sentence itself, as a text string.
  sentence
  ;; For non-English strings, this is the translation into English, if
  ;; one was passed in to us.
  translated-text-string
  ;; The sentence broken into words (tokenized) and then lemmatized.
  ;; This is saved as a list, with each entry a two-element sublist:
  ;; the original text string for this word, and a lemmatized version
  ;; of it.
  lemmatized-tokens
  ;; A unique string that identifies this sentence and that can be
  ;; used to access all info associted with this sentence in the Scone
  ;; KB.  This string may have been passed as an argument or may have
  ;; been generated by Scone.
  sentence-id
  ;; A list of checkable output structures, one for each checkable
  ;; actually analyzed by the system.  This may differ from the number
  ;; of checkables actually passed to SCONE-M4-DETECT because some
  ;; checkables may be split into smaller ones, etc., before analysis.
  checkable-outputs)

(defstruct checkable-output
  "We build one of these for each checkable that we process.  It
   is used to return the results of processing this one checkable."
  ;; The checkable itself, in the form actually processed by Scone.
  ;; It may differ from the checkable actually passed to
  ;; SCONE-M4-DETECT due to lemmatization, replacing old checkable
  ;; types with new ones, etc.
  checkable
  ;; A unique string created by Scone to identify this checkable and
  ;; structures in Scone that are associated with it.
  checkable-id
  ;; This is the unique ID string identifying the sentence of which
  ;; this checkable is a part.
  sentence-id
  ;; Value is one of :metaphor, :literal, :maybe, or :unknown,
  ;; indicating Scone's overall judgment about whether the checkable is a
  ;; lexical metaphor.
  checkable-decision
  ;; This is a value from -1.0 to 1.0 indicating the results of
  ;; analysis of the checkable by matching of CM-patterns.  1.0
  ;; indicates strong belief that the checkable is a metaphor; -1.0
  ;; indicates strong belief that it is literal.  If there is no
  ;; evidence, the value is 0.0.
  pattern-evidence
  ;; This is a value from -1.0 to 1.0 indicating the results of
  ;; analysis of the checkable by detection of selectional preference
  ;; violations.  1.0 indicates strong belief that the checkable is a
  ;; metaphor; -1.0 indicates strong belief that it is literal.  If
  ;; there is no evidence, the value is 0.0.
  preference-violation-evidence
  ;; A list of two integers indicating the text span within the
  ;; original sentence string, that contains the LM, if one was
  ;; detected.
  lm-span
  ;; The string representing the source of the detected LM, or NIL.
  ;; This string is the lemmatized form actually passed to Scone.
  lm-source-string
  ;; A list of two integers representing the span covered by the
  ;; source-string, in its original form, within the original
  ;; sentence.
  lm-source-span
  ;; The string representing the source of the detected LM, or NIL.
  ;; This string is the lemmatized form actually passed to Scone.
  lm-target-string
  ;; A list of two integers representing the span covered by the
  ;; target-string, in its original form, within the original
  ;; sentence.
  lm-target-span
  ;; A value from 0.0 to 1.0 indicating our confidence that the
  ;; LM-source and LM-target are right-way-round.
  source-target-confidence
  ;; This is a keyword, one of :ECONOMIC-INEQUALITY, :GOVERNANCE, or
  ;; :UNKNOWN.
  target-domain
  ;; This is a value from 0.0 to 1.0 indicating our level of
  ;; confidence that the detected LM is associated with the
  ;; target-domain specified above.
  target-domain-confidence
  ;; A Scone concept-node indicating the most-specific CM-source
  ;; concept for the detected LM.
  specific-cm-source
  ;; An index into the Source Concept Table identifying the
  ;; standardized concept that most closely matches the
  ;; SPECIFIC-CM-SOURCE above, if we can find such a concept.
  ;; Otherwise, NIL.
  cm-source-index
  ;; A Scone concept-node indicating the most-specific CM-target
  ;; concept for the detected LM.
  specific-cm-target
  ;; An index into the Target Concept Table identifying the
  ;; standardized concept that most closely matches the
  ;; SPECIFIC-CM-SOURCE above, if we can find such a concept.
  ;; Otherwise, NIL.
  cm-target-index
  ;; This is the best CM-pattern we found for this checkable.
  cm-pattern
  ;; This is the split-set that caused the strongest preference
  ;; violation, if any, for this checkable.
  split-violated)

;;; Document-level metaphor processing.  For now, this is just a
;;; place-holder.

(defun new-document ()
  "Call this when moving to a new document to tell Scone it should
   re-initialize its document-level context."
  ;; Doesn't do anything for now.
  nil)

;;; THIS IS THE MAIN ENTRY POINT TO SCONE'S METAPHOR-DETECTION
;;; PIPELINE.

(defun scone-m4-detect
    (;; LANGUAGE tells Scone what language and culture we are working
     ;; in.  For now, it is one of :EN, :ES, :RU, and :FA.
     language
     ;; A unique string that identifies this sentence and that can be
     ;; used to access all info associted with this sentence in the
     ;; Scone KB.  If this argument is NIL, Scone will generate the
     ;; string.
     sentence-id
     ;; The TEXT-STRING is simply the text of the sentence to be
     ;; processes, passed as a Common Lisp string in UTF8 format.
     text-string
     ;; If the TEXT-STRING is non-English, TRANSLATED-TEXT-STRING is
     ;; the English translation, if that is available.  Else, NIL.
     ;; Scone doesn't do anything but record this.  It's only for
     ;; debugging and tuning.
     translated-text-string
     ;; The LEMMATIZED-TOKENS argument is a list of two-element
     ;; sublists.  In each of these, the first element is a string
     ;; representing a word or lexical item after tokenization.  The
     ;; second is a string representing the canonical or lexicalized
     ;; form.  This can be NIL if this information is not available.
     lemmatized-tokens
     ;; The NAMED-ENTITIES argument is a list of two-element sublists,
     ;; each of which represents some NAMED-ENTITY identified by the
     ;; NLP pipeline upstream.  The first element is a string (which
     ;; may contain multiple words) identifying the entity.  The
     ;; second is a type-label such as :PERSON or :COMPANY.  If Scone
     ;; does not already know this entity, it will be added to the KB,
     ;; and can be used in processing the checkables that follow in
     ;; this same call.
     named-entities
     ;; The CHECKABLES argument is a list of all the checkables
     ;; identified for this sentence.  Each is a sublist, starting with
     ;; a keyword tag and followed by some number of lexical items extracted from
     ;; the sentence and then lemmatized.  Or a list-item may be a long-
     ;; form checkable, with indices into the string and translated lexical items.
     ;; See the API document for details on the long form.
     checkables)
  "This is the main entry point to the Scone pipeline.  It is called
   once for each sentence.  For return values and a description of
   what goes into the repository, see the API document."
  ;; Generate the SENTENCE-ID string if necessary. 
  (unless sentence-id
    (setq sentence-id
	  (format nil "Sentence-ID ~D ~D"
		  *system-start-time*
		  (incf *sentence-counter*))))
  ;; It's useful for debugging to print a comment telling what sentence
  ;; we are working on.
  (format t "~%; ~S~%" text-string)
  (let* ((*current-language* nil)
	 (*current-culture* nil)
	 ;; We will change the current context.  This makes it revert
	 ;; when we exit.
	 (*context* *context*)
	 ;; Create the sentence-output structure.  The
	 ;; :SENTENCE-DECISION and :SENTENCE-CONFIDENCE fileds will be
	 ;; filled in after we process all the checkables.
	 (sentence-output
	  (make-sentence-output
	   :sentence text-string
	   :translated-text-string translated-text-string
	   :lemmatized-tokens lemmatized-tokens
	   :sentence-id sentence-id))
	 ;; The accumulating list of checkable output structures returned
	 ;; by analysis of the individual checkables.
	 (checkable-outputs nil)
	 ;; This is initialized to NIL for the sentence.  It gets set by
	 ;; PROCESS-CHECKABLE if it finds any metaphor in the sentence.
	 (*metaphor-found-in-sentence* nil))
    ;; Process the langauge code and set up the cultural context.
    (process-language-code language)
    ;; Process the named entities that were passed, if any.
    ;; Basically, we put all of these into the Scone KB in the current
    ;; culture, under the specified type unless there is a conflict.
    (process-named-entities named-entities)
    ;; Clean up the checkables list and get everything into long-form.
    (setq checkables (fix-checkables checkables))
    ;; Process each checkable in turn to identify any related CM-patterns
    ;; and, if one is found, to process its payload.  This creates and
    ;; returns a checkable-output structure.
    (dolist (c checkables)
      (push
       (process-checkable c sentence-output)
       checkable-outputs))
    ;; The checkable-outputs list is backwards, so flip it.
    (setq checkable-outputs (nreverse checkable-outputs))
    ;; If we have a met in this sentence, tick the counter and
    ;; print a comment.
    (when *metaphor-found-in-sentence*
      (incf *sentences-with-metaphors*)
      (format t "~&; ~D sentences with metaphors found so far."
	      *sentences-with-metaphors*))
    ;;; Return the results.
    checkable-outputs))

(defun process-language-code (code)
  "Dummy definition for now."
  code)

;(defun process-language-code (code)
;  "Set up the current language and culture, given a language code."
;  (let ((item (find-if #'(lambda (x) (eq (car x) code))
;		       *language-token-map*)))
;    (when item
;      (setq *current-language* (second item))
;      (setq *current-culture* (third item))
;      (in-context (third item)))))

(defun fix-checkables (checkables-list)
  "Given a checkables list, scan it for old-style checkables and
   replace them with the corresponding new-style ones.  May require
   re-shuffling the order of arguments.  Also break down 3-arg
   checkables into two (for now), and turn everything into long-form
   checkables."
  (let ((output nil))
    (dolist (x checkables-list)
      ;; Extract (or create) the three components of a long-form
      ;; checkable.
      (let ((checkable
	     (if (listp (car x)) (car x) x))
	    (offsets
	     (if (listp (car x)) (second x) nil))
	    (translated
	     (if (listp (car x)) (third x) nil)))
	;; Handle ill-formed offsets passed as a list of NILs.
	(when (and offsets
		   (listp offsets)
		   (not (integerp (car offsets))))
	  (setq offsets nil))
	;; Convert old-style checkables to new.  These are deprecated
	;; but we need to support them for now.
	(case (car checkable)
	   ((:entity-is-entity :entity_is_entity)
	    (push
	     `((:x-is-y ,(second checkable) ,(third checkable))
	       ,offsets
	       ,translated)
	     output))
	   ((:entity-has-modifier :entity_has_modifier)
	    (push
	     `((:x-modifies-y ,(third checkable) ,(second checkable))
	       ,(swap-offsets offsets)
	       ,(swap-translations translated))
	     output))
	 ((:entity-has :entity-has-entity :entity_has :entity_has_entity)
	    (push
	     `((:x-has-y ,(second checkable) ,(third checkable))
	       ,offsets
	       ,translated)
	     output))
	 ((:action-has-subject :action_has_subject)
	    (push
	     `((:s-v ,(third checkable) ,(second checkable))
	       ,(swap-offsets offsets)
	       ,(swap-translations translated))
	     output))
	 ((:action-has-object :action_has_object)
	    (push
	     `((:v-o ,(second checkable) ,(third checkable))
	       ,offsets
	       ,translated)
	     output))
	 ((:action-occurs-at-place :action_occurs_at_place)
	    (push
	     `((:x-at-location-y ,(second checkable) ,(third checkable))
	       ,offsets
	       ,translated)
	     output))
	 ;; And also deal with the 3-arg-cases.
	 ((:s-v-o :s_v_o)
	  ;; For the detection test, we are not processing the
	  ;; full triplet :s-v-o.  Break out the two-arg cases.
	  ;; If there is a non-null subject, add an :s-v to the list.
	  (when (second checkable)
	    (push
	     `((:s-v ,(second checkable) ,(third checkable))
	       ,offsets
	       ,translated)
	     output))
	  ;; If there is an non-null object, add a :v-o to the list.
	  (when (fourth checkable)
	    (push
	     `((:v-o ,(third checkable) ,(fourth checkable))
	       ,(cddr offsets)
	       ,(cdr translated))
	     output)))
	 ((:x-is-the-y-of-z :x-is-the-y-of-z)
	  ;; For the detection test, break this into an :x-has-y
	  ;; test and an :x-is-a-y test.
	  (push
	   `((:x-has-y ,(fourth checkable) ,(third checkable))
	     ,(swap-offsets (cddr offsets))
	     ,(swap-translations (cdr translated)))
	   output)
	  (push
	   `((:x-is-a-y ,(second checkable) ,(third checkable))
	     ,offsets
	     ,translated)
	   output))
	 ;; Nothing to fix, add it to the list as a long-form
	 ;; checkable.
	 (t (push (list checkable offsets translated)
		  output)))))
    ;; Get the output list right-way-round and return it.
    (nreverse output)))

(defun swap-offsets (offsets)
  "Given an offset list, swap the first two pairs and discard
   any others.  If arg is NIL, return NIL."
  (and offsets
       `(,(third offsets)
	 ,(fourth offsets)
	 ,(first offsets)
	 ,(second offsets))))

(defun swap-translations (translations)
  "Given a list to lexical-item translations, swap the first two items
   and discard the rest.  If given NIL, return NIL."
  (and translations
       `(,(second translations)
	 ,(first translations))))

(defun process-named-entities (entity-list)
  "Given a list of entity-name, type-tag pairs, put these entities
   into the KB for use in this and subsequent calls.  If a
   type-designator is associated with NIL instead of a Scone type, we
   don't create this one.  Note: The new entity will go into the
   context for the curent culture."
  ;; Look at all the pairs on the named-entity list.
  (dolist (x entity-list)
    ;; First element in the pair is the entity name.  Remove underscores.
    (let* ((string (convert-underscores (first x)))
	   ;; If the tag is in the *NAMED-ENTITY-TYPE-MAP*, save that
	   ;; entry.  Else NIL.
	   (entry 
	    (find-if #'(lambda (y) (eq (first y) (second x)))
		      *named-entity-type-map*))
	   (scone-type (when entry (second entry))))
      ;; If an entity of this name is already known to Scone, just
      ;; skip this, since info from the named-entity extractor is
      ;; probably noisy.
      (unless (or (null scone-type)
		  ;; Colons in the string confuse the Scone iname
		  ;; system.
		  (find #\: string)
		  (lookup-definitions string))
	;; It is not known so create a new indv node of the specified type.
	(new-indv string scone-type)))))

(defun process-checkable (c sentence-output)
  "Given one long-form checkable C, run the metaphor detection and
   interpretation machinery, returning the CHECKABLE-OUTPUT structure.
   Also pass the current SENTENCE-OUTPUT so that we can refer to it."
  (let* ((c-out
	  ;; Create and initialize the CHECKABLE-OUTPUT strucutre.
	  (make-checkable-output
	   :checkable c
	   :checkable-id
	   (format nil "Checkable-ID ~D ~D"
		  *system-start-time*
		  (incf *checkable-counter*))
	   :sentence-id
	   (sentence-output-sentence-id sentence-output)
	   :checkable-decision :unknown
	   :pattern-evidence 0.0
	   :preference-violation-evidence 0.0))
	 ;; Get the short-form checkable.
	 (checkable (first c))
	 ;; And the lexical argument offsets.
	 (offsets (second c)))
    ;; Process two-argument checkables for CM-patterns, filling in the
    ;; fields of the checkable-output structure.  For now, we're only
    ;; looking at 2-arg checkables.
    (when (= (length checkable) 3)
      (process-cm-patterns-2 checkable offsets c-out))
    ;; Return the modified C-OUT structure.
    c-out))

(defun set-up-st-info (checkable-keyword source target confidence)
  "Internal utility to set up information needed by
   PROCESS-SOURCE-TARGET-INFO.  For each checkable-type keyword, we
   indicate which argument is typically the LM-source and which is
   typically the LM-target.  These are used as default choices, but
   can be over-ridden by specific CM-patterns.  We also provide a
   confindence value, indicating how strongly we believe that the
   source and target are right-way-round."
  (setf (get checkable-keyword :default-source-arg) source)
  (setf (get checkable-keyword :default-target-arg) target)
  (setf (get checkable-keyword :source-target-confidence) confidence))

;; These are the actual values we use for the current checkable-labels.
;; We have to return strings from the original sentence as LM-source and
;; LM-target, even for sentences in which (in my opinion) the actual
;; source or target is implicit and not explicitly mentioned in the
;; sentence.  So we just do what come closest, and lower the confidence
;; a bit.
(set-up-st-info :x-is-y 2 1 0.3)
(set-up-st-info :x-is-a-y 2 1 0.9)
(set-up-st-info :x-modifies-y 2 1 0.3)
(set-up-st-info :x-has-y 2 1 0.2)
(set-up-st-info :s-v 2 1 0.7)
(set-up-st-info :v-o 1 2 0.7)
(set-up-st-info :x-at-location-y 2 1 0.7)
(set-up-st-info :x-is-the-y-of-z 2 1 0.9)
(set-up-st-info :s-v-o 2 1 0.7)

(defun process-cm-patterns-2 (c offsets c-out &optional (retry nil))
  "Given a (short-form) checkable C with two string arguments, check
   all combinations of meanings for the lexical-item (string)
   arguments.  If any CM-patterns are found, apply them.  Results get
   merged into the checkable-output structure that is passed as C-OUT.
   OFFSETS is the list of offsets that were supplied with the
   checkable.

   If RETRY is :s-v or :v-o, we have a checkable of that type that
   had no matching pattens of its own.  So now we are looking for
   an :x-is-a-y pattern, linking the S or V to the role-type."
  (let* ((label-element
	  (if retry
	      (get :x-is-a-y :scone-element)
	      (get (first c) :scone-element)))
	 (x-meanings
	  (lookup-definitions (second c)))
	 (y-meanings
	  (lookup-definitions (third c)))
	 (matching-cm-patterns nil)
	 (cm-pattern-strength 0.0)
	 ;; The best CM-pattern found so far, and its strength.
	 (best-cm-pattern nil)
	 (best-strength 0.0)
	 ;; When we have found a best-CM-pattern, this is the set
	 ;; of meanings that led us there.
	 (best-x-meaning nil)
	 (best-y-meaning nil))
    ;; Unless we have at least one meaning for both lexical arguments,
    ;; leave the output structure unchanged.  The default decision is
    ;; :unknown.
    (when (and x-meanings y-meanings label-element)
      ;; Consider all combinations of X and Y meanings.
      (dolist (xm x-meanings)
	(setq xm (car xm))
	(dolist (ym y-meanings)
	  (setq ym (car ym))
	  (cond
	    ((null retry)
	     ;; Find the set of lowermost CM-patterns for each
	     ;; combination, if any.
	     (setq matching-cm-patterns
		(find-matching-cm-patterns-2
		 label-element
		 xm
		 ym)))
	     ((eq retry :s-v)
	      ;; Check for is-a patterns that link S to V's agent
	      ;; role.
	      (setq matching-cm-patterns
		    (find-matching-cm-patterns-2
		     label-element
		     xm
		     (the-x-role-of-y *action-agent* ym))))
	    ((eq retry :v-o)
	     ;; Check for is-a patterns that link O to V's object
	     ;; role.
	     (setq matching-cm-patterns
		   (find-matching-cm-patterns-2
		    label-element
		    ym
		    (the-x-role-of-y *action-object* xm)))))	    
	  ;; Compute the strength of each of these patterns.  If any
	  ;; is stronger than the best pattern found so far for this
	  ;; checkable, it becomes the BEST-CM-PATTERN.
	  (dolist (p matching-cm-patterns)
	    ;; Computer the strength of our belief that this pattern
	    ;; is a metaphor.  If *desired-target-domain* is non-nil,
	    ;; this is the strengh of our belief that the pattern is
	    ;; a metaphor in the specified domain; otherwise, it's just
	    ;; the inherent strength.
	    (setq cm-pattern-strength
		  (cm-pattern-strength p xm ym))
	    (when (> cm-pattern-strength  best-strength)
	      (setq best-cm-pattern p)
	      (setq best-strength cm-pattern-strength)
	      (setq best-x-meaning xm)
	      (setq best-y-meaning ym))))))
    (cond
      (best-cm-pattern
	;; We have a winner.  Process its meaning payload.  For now
	;; that just means we harvest its values into the checkable-output
	;; structure.
	(process-payload-2 c offsets c-out best-cm-pattern
			   best-x-meaning best-y-meaning retry))
      ;; No winner.  If this is an :s-v checkable, also check for
      ;; :x-is-a-y relation between S and the agent-type of V.
      ((and (not retry) (eq (first c) :s-v))
       (process-cm-patterns-2 c offsets c-out :s-v))
      ;; No winner.  If this is a :v-o checkable, also check for
      ;; :x-is-a-y relation between O and the object-type of V.
      ((and (not retry) (eq (first c) :v-o))
       (process-cm-patterns-2 c offsets c-out :v-o)))
    ;; Return the updated checkable-output pattern.
    c-out))

;;; %%% Note: The function below may be slow if we have a large number
;;; of CM-patterns defined.  There are lots of ways we could speed it up
;;; by clever indexing and data structures.

(defun find-matching-cm-patterns-2 (checkable-label xm ym)
  "Given Scone elements corresponding to a CHACKABLE-LABEL and one
   possible meaning for each of the two lexical arguments, find all
   the matching CM-patterns.  Return a list of the lowermost of these.
   There still maybe be more than one."
  (when (and checkable-label xm ym)
    (with-markers (m1 m2 m3 m4)
      (progn
	;; Mark all superiors of the checkable-label and the two
	;; meaning-nodes.
	(upscan checkable-label m1)
	(upscan xm m2)
	(upscan ym m3)
	;; Now, search for all the CM-patterns that are attached to
	;; XM as the X-type role.
	(do-marked (e m2)
	  (let ((pat (context-wire e)))
	    (when (and (marker-on? pat *cm-pattern-marker*)
		       (marker-on? (the-x-of-y *cm-checkable-label* pat) m1)
		       (marker-on? (the-x-of-y *cm-x-type* pat) m2)
		       (marker-on? (the-x-of-y *cm-y-type* pat) m3))
	    (mark pat m4))))
	(list-lowermost m4)))))

(defun process-payload-2 (c offsets c-out cm-pattern
			    x-concept y-concept retry)
  "Given a 2-argument checkable C, OFFSET values for the args, and a
   matching CM-PATTERN, plus the relevant meanings for X and Y, get
   the payload structure of this pattern and update the
   checkable-output structure C-OUT based on information in the
   payload.  The RETRY arg indicates whether this CM-patter is from
   the original checkable, or is from finding an :x-is-a-y relationship
   between the checkable and the role-type in an :s-v or :v-o pattern."
  (let* ((payload
	  (node-value (the-X-of-y *cm-payload* cm-pattern)))
	 (temp nil)
	 ;; Extract what this CM-pattern thinks about the location
	 ;; of the source and target.
	 (target-position (payload-target-position payload))
	 (source-position (payload-source-position payload))
	 (source-target-confidence
	  (payload-source-target-confidence payload)))
    ;; If we have no S-T ordering from the CM-pattern, use default values
    ;; based on the checkable type.
    (when (or (eql source-target-confidence 0.0)
	      (null source-target-confidence))
      (setq target-position
	    (get (car c) :default-target-arg))
      (setq source-position
	    (get (car c) :default-source-arg))
      (setq source-target-confidence
	    (get (car c) :source-target-confidence)))
    ;; Kludge: If we are retrying a :v-o checkable as an
    ;; :x-is-a-y, flip the source and target.
    (when (eq retry :v-o)
      (rotatef target-position source-position))
    ;; Compute and record the pattern-evidence for this checkable.
    ;; If it is above threshold, call this a metaphor.
    (when (>=
	   (setf (checkable-output-pattern-evidence c-out)
		 (payload-strength payload))
	   *pattern-evidence-threshold*)
      (setf (checkable-output-checkable-decision c-out)
	    :metaphor)
      (setq *metaphor-found-in-sentence* t))
    (setf (checkable-output-target-domain c-out)
	  (payload-target-domain payload))
    (setf (checkable-output-target-domain-confidence c-out)
	  (payload-target-domain-confidence payload))
    (setf (checkable-output-specific-cm-target c-out)
	  (if (eql target-position 1)
	      x-concept y-concept))
    (setf (checkable-output-specific-cm-source c-out)
	  (if (eql target-position 1)
	      y-concept x-concept))
    (setf (checkable-output-cm-pattern c-out) cm-pattern)
    ;; If the pattern payload supplied a source-target-confidence,
    ;; fill in the LM-source, LM-target, and spans.  Else, this will
    ;; be done later, based on just the type of the checkable.
    (setq temp (payload-source-target-confidence payload))
    (when (and temp target-position)
      ;; Record the source-target confidence, source and target
      ;; strings, and the spans.
      (setf (checkable-output-source-target-confidence c-out) temp)
      (cond ((eql 1 target-position)
	     ;; Target is the first string arg.
	     (setf (checkable-output-lm-target-string c-out)
		   (second c))
	     (when offsets
	       (setf (checkable-output-lm-target-span c-out)
		     (list (nth 0 offsets)
			   (nth 1 offsets))))
	     ;; Source is the second string arg.
	     (setf (checkable-output-lm-source-string c-out)
		   (third c))
	     (when offsets
	       (setf (checkable-output-lm-source-span c-out)
		     (list (nth 2 offsets)
			   (nth 3 offsets)))))
	    ;; Target is the second string arg.
	    (t (setf (checkable-output-lm-target-string c-out)
		     (third c))
	       (when offsets
		 (setf (checkable-output-lm-target-span c-out)
		       (list (nth 2 offsets)
			     (nth 3 offsets))))
	       ;; Source is the second string arg.
	       (setf (checkable-output-lm-source-string c-out)
		     (second c))
	       (when offsets
		 (setf (checkable-output-lm-source-span c-out)
		       (list (nth 0 offsets)
			     (nth 1 offsets))))))
      ;; Record the composite LM-span.
      (when offsets
	(setf (checkable-output-lm-span c-out)
	      (list (min (nth 0 offsets)
			 (nth 2 offsets))
		    (max (nth 1 offsets)
			 (nth 3 offsets))))))
    c-out))

;; We will only report the results of one matching CM-pattern for a
;; given checkable, so we want to find the strongest, based on the
;; pattern's inherent strength and whether it matches the desired
;; target domain.  We'll have to play with these heursitics.

(defun cm-pattern-strength (cm-pattern x-meaning y-meaning)
  "Determine how strong we believe a CM-pattern to be, given the
   CM-PATTERN itself, an X-MEANING (a Scone concept), a Y-MEANING, and
   the *DESIRED-TARGET-DOMAIN*.  Return this as a value from 0.0 to
   1.0."
  (declare (ignore x-meaning y-meaning))
  (let ((payload (node-value (the-x-of-y *cm-payload* cm-pattern))))
    ;; If there's a desired target domain, we want to match it.
    (cond (*desired-target-domain*
	   (if (eq (payload-target-domain payload)
		   *desired-target-domain*)
	       ;; This seems to be the domain we want, so scale
	       ;; the inherent confidence by the confidence that
	       ;; we are in this target domain.
	       (* (payload-strength payload)
		  (payload-target-domain-confidence payload))
	       ;; Not the right domain.  Scale inherent
	       ;; confidence down by how sure we are of the
	       ;; domain mismatch.
	       (* (payload-strength payload)
		  (- 1.0
		     (payload-target-domain-confidence payload)))))
	  ;; If there is no desired target domain, just
	  ;; go with the inherent strength.
	  (t (payload-strength payload)))))

(defun test-checkable (c &optional named-entities)
  "This function is just for testing things quickly.  It takes a
   short-form checkable C and optionally a list of named entities and
   their types.  It runs these arguments through SCONE-M4-DETECT, with
   dummy values for all the other arguments."
  (scone-m4-detect
   :en
   nil
   "Dummy string."
   "Dummy string translation."
   nil
   named-entities
   `((,c (0 5 10 15)))))

#|
;;; ---------------------------------------------------------------------------
;;; The VIOLATES function and its parts.

;;; NOTE: This is from the old December checkables system.  Will be updated
;;; soon.

;;; VIOLATES is (for now) the primary interface for processing
;;; checkables -- descriptive phrases and simple clauses -- for
;;; violations of type restrictions (a.k.a. selctional preferences).

(defun violates (checkable-type &rest rest)
  "Check whether a text checkable violates any type-restrictions
   (a.k.a. selectonal preferences) in the Scone KB.

   The first argument will be a keyword, such as :entity-has-modifier.
   This is followed by two or more words or phrases, expressed as
   UTF8-encoded Lisp strings, so non-English alphabets are OK.  The
   first argument indicates what we believe to be the grammatical
   relation between these other arguments.

   The primary return value is T or NIL, representing Scone's overall
   judgement about whether this combination makes literal sense (NIL
   for no-violation) or does not (T for violation, indicating a
   metaphor or metonym).  Note that the system may return T even if
   some literal interpretation has been found, but judges it to be
   improbable.

   The remaining return values, if present, give some additional
   information about the decision.  The second return value is some
   indication of why the call was made.  Interesting values are
   (:unknown-lexical-item <item>), (:no-useful-meaning <item>, and so
   on.  The third return value, if present, is the set of argument
   combinations that caused a violation; the fourth is the set of
   combinations that did NOT cause a violation."
  (format t "~&Processing ~S  ~S: ~%" checkable-type rest)
  (incf *checkables*)
  ;; Here we just dispatch to the individual functions that handle each type
  ;; of checkable, returning the values they return.
  (ecase checkable-type
    (:entity-has-modifier
     (apply #'v-incompatible-2 rest))
    (:action-has-modifier
     (apply #'v-incompatible-2 rest))
    (:entity-is-entity
     (apply #'v-incompatible-2 rest))
    (:action-has-subject
     (apply #'action-has-subject rest))
    (:action-has-object
     (apply #'action-has-object rest))
    ;; Scone can't do anything useful with this one, as of now.
    (:action-occurs-at-place
     nil)
;    These are not currently being passed.
;    (:action-svo
;     (apply #'v-rel rest))
;    (:relation-sro
;     (apply #'v-rel rest))
;    (:entity-has
;     (apply #'v-has rest))
    ))


;;; Check two-argument forms for compatibility violations.
 
(defun v-incompatible-2 (x y)
  "Takes two lexical items (Lisp strings) X and Y.  Each of these
   strings may have multiple definitions, so we need to look at each
   combination of possible meanings to see if any are compatible."
  (let ((x-meanings (lookup-definitions x))
	(y-meanings (lookup-definitions y))
	(conflicts nil)
	(no-conflicts nil))
    (cond
      ;; If either of the args is an unknown term, i.e.
      ;; one with no meanings, return NIL with this explanation.
      ((null x-meanings)
       (incf *unknown-lex*)
       (values nil `(:unknown-lexical-item ,x)))
      ((null y-meanings)
       (incf *unknown-lex*)
       (values nil `(:unknown-lexical-item ,y)))
      ;; Filter both lists for useless values.  If either list is
      ;; empty after filtering, return NIL with this explanation.
      ((null (setq x-meanings (filter-meanings x-meanings)))
       (incf *no-useful-meanings*)
       (values nil `(:no-useful-meanings ,x)))
      ((null (setq y-meanings (filter-meanings y-meanings)))
       (incf *no-useful-meanings*)
       (values nil `(:no-useful-meanings ,y)))
      ;; We have two known lexical items, each with at least one
      ;; useful meaning.  Consider each combination of an X meaning
      ;; and a Y meaning, noting those winners that are compatible
      ;; with one anothewhich combinations show conflicts.
      (t (dolist (xm x-meanings)
	   (dolist (ym y-meanings)
	     (if (incompatible? xm ym)
		 (push `(,xm ,ym) conflicts)
		 (push `(,xm ,ym) no-conflicts))))
	 ;; Return a decision based on some combintation of the
	 ;; winners and losers.
	 (combine-results conflicts no-conflicts)))))

;;; We will look at different versions of these two functions to see
;;; what works best for this application.

;;; This version just removes the elements that have the :useless
;;; property.

(defun filter-meanings (ml)
  "Given a list of word-meanings ML,return a list of the ones we
   actually want to use.  List only the Scone-element part of each
   word-meaning structure."
  (let ((winners nil))
    (dolist (m ml)
      ;; The element is the first item in the meaning structure.
      (setq m (car m))
      (unless (get-element-property m :deprecated)
	(push m winners)))
    winners))

;;; Consider the list of non-conflicting meaning combinations and the list
;;; of conflicting meaning combinations.  Return a decision, based on some
;;; combitation of these.

(defun combine-results (conflicts no-conflicts)
  "Consider the list of conflicting and non-conflicting meaning pairs.
   Return a decision.  If either set is NIL, it's easy.  If there are some
   of each, any combo with only native-Scone elements prevails.
   If there Scone combos on both lists, or none on both lists, return
   NIL, since there appears to be some valid literal interpretation"
  (cond
    ;; All combinations are OK.  No violation.
    ((null conflicts)
     (incf *only-ok*)
     (values nil
	     :only-ok
	     conflicts
	     no-conflicts))
    ;; We have no no-conflict combinations, so it's a violation.
    ((null no-conflicts)
     (incf *only-violations*)
     (incf *checkable-violations*)
     (setq *any-violations* t)     
     (values
      t
      :only-conflicts
      conflicts
      no-conflicts))
    ;; Mixture of conflicts and no-conflicts.  Scan the lists.
    ;; All-native combinations trump those with WordNet
    ;; word-senses in them.
    ((and conflicts no-conflicts)
     (incf *mixed*)
     (cond
       ;; All the vetted combinations say there's a conflict.
       ((and (not (unvetted-only conflicts))
	     (unvetted-only no-conflicts))
	(incf *checkable-violations*)
	(setq *any-violations* t)     
	(values
	 t
	 :vetted-conflict
	 conflicts
	 no-conflicts))
       ;; Otherwise, the two lists look equally strong, or the vetted
       ;; values say "no conflict", so we signal no conflict.
       (t
	(values nil
		:mixed
		conflicts
		no-conflicts))))))

(defun unvetted-only (combos)
  "Given a list of word-sense combinations, return T if none of them
   contains only vetted word-senses.  A word-sense is unvetted if it
   is from WordNet and does not have the :vetted feature."
  (dolist (c combos)
    (let ((vetted-combo t))
      ;; Any unvetted word-sense in a combo knocks it out.
      (dolist (sense c)
	(when (and (get-element-property sense :wordnet)
		   (not (get-element-property sense :vetted)))
	  (setq vetted-combo nil)))
      (when vetted-combo
	(return-from unvetted-only nil))))
  t)


    
(defun action-has-subject (action subject)
  "Takes two lexical items (Lisp strings) ACTION and SUBJECT.  ACTION
   must be an {action}.  See if there are any literal interpetations where
   the SUBJECT can literally be the SUBJECT of ACTION."
  (action-has-role action *action-agent* subject))

(defun action-has-object (action object)
  "Takes two lexical items (Lisp strings) ACTION and OBJECT.  ACTION
   must be an {action}.  See if there are any literal interpetations where
   the SUBJECT argument can literally be the object of this ACTION."
  (action-has-role action *action-object* object))

(defun action-has-role (action role filler)
  "This does the work for the various <action-has-x> functions.  Pass the role
   node and the string representing the would-be filler."
  (let ((action-meanings (lookup-definitions action))
	(filler-meanings (lookup-definitions filler))
	(conflicts nil)
	(no-conflicts nil)
	(any-actions? nil))
    (cond
      ;; If either of the args is an unknown term, i.e.
      ;; one with no meanings, return no violation.
      ((null action-meanings)
       (incf *unknown-lex*)
       (values nil `(:unknown-lexical-item ,action)))
      ((null filler-meanings)
       (incf *unknown-lex*)
       (values nil `(:unknown-lexical-item ,filler)))
      ;; Filter out useless meanings and note when either ACTION or
      ;; FILLER has no remaining values.  While we're at it, replace
      ;; each meaning with just the Scone element part.
      ((null (setq action-meanings (filter-meanings action-meanings)))
       (incf *no-useful-meanings*)
       (values nil `(:no-useful-meanings ,action)))
      ((null (setq filler-meanings (filter-meanings filler-meanings)))
       (incf *no-useful-meanings*)
       (values nil `(:no-useful-meanings ,filler)))
      ;; Consider each combination of an ACTION meaning and an FILLER meaning.
      (t (dolist (am action-meanings)
	   ;; If this meaning is not an {ACTION}, can't be a winner.
	   (when (eq :yes (is-x-a-y? am *action*))
	     ;; Note that we found at least one relation.
	     (setq any-actions? t)
	     (dolist (fm filler-meanings)
	       (if (can-x-be-the-y-of-z? fm role am)
		   (push `(,am ,fm) no-conflicts)
		   (push `(,am ,fm) conflicts)))))
	 ;; Return the appropriate values.
	 (if any-actions?
	     (combine-results conflicts no-conflicts)
	     ;; Didn't find any action meanings.
	     (progn
	       (incf *no-useful-meanings*)	     
	       (values nil `(:no-action-meanings ,action) nil nil)))))))


|#

