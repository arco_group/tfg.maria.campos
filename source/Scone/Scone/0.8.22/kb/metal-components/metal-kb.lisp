;;; -*- Mode:Lisp -*-
;;; ***************************************************************************
;;; Knowledge specific to the METAL (IARPA Metaphor) Project.
;;; Author and Maintainer: Scott E. Fahlman

;;; Contributions by Amos Ng, Sukhada Palkar, John Retterer-Moore, and
;;; Thomson Yeh.
;;; ***************************************************************************
;;; Copyright (C) 2003-2013, Scott E. Fahlman
;;;
;;; The Scone software is made available to the public under the
;;; Apache 2.0 open source license.  A copy of this license is
;;; distributed with the software.  The license can also be found at
;;;
;;; http://www.apache.org/licenses/LICENSE-2.0.
;;;
;;; Unless required by applicable law or agreed to in writing,
;;; software distributed under the License is distributed on an "AS
;;; IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
;;; express or implied.  See the License for the specific language
;;; governing permissions and limitations under the License.
;;;
;;; Development of Scone since January, 2012, has been supported in
;;; part by the Intelligence Advanced Research Projects Activity
;;; (IARPA) via Department of Defense US Army Research Laboratory
;;; contract number W911NF-12-C-0020.
;;;
;;; Development since February 2013 has been supported in part by the
;;; U.S. Office of Naval Research under award number N000141310224.
;;;
;;; Development of Scone from 2003 through 2008 was supported in part
;;; by the Defense Advanced Research Projects Agency (DARPA) under
;;; contract numbers NBCHD030010 and FA8750-07-D-0185.
;;;
;;; Additional support for Scone development has been provided by
;;; generous research grants from Cisco Systems Inc. and from Google
;;; Inc.
;;;
;;; The U.S. Government is authorized to reproduce and distribute
;;; reprints for Governmental purposes notwithstanding any copyright
;;; annotation thereon.
;;;
;;; Disclaimer: The views and conclusions contained herein are those
;;; of the authors and should not be interpreted as necessarily
;;; representing the official policies or endorsements, either
;;; expressed or implied, of IARPA, DoD/ARL, ONR, DARPA, the
;;; U.S. Government, or any of our other sponsors.
;;; ***************************************************************************

;;; Note: All this stuff should eventually find its way into the
;;; appropriate place in Scone's "core" knowledge base.  This file
;;; assumes that the "core" KB is already loaded.

;;; Knowledge for English examples.

;;; ---------------------------------------------------------------------------
;;; Language and Culture

;;; For now, we focus on languages and cultures needed for the METAL Project.

(new-type {human language} {intangible}
	  :english '("language" "natural language"))

(new-split-subtypes {human language}
   '({English language}
     {Spanish language}
     {Farsi language}
     {Russian language}))

(new-type {human culture} {intangible})

(new-split-subtypes {human culture}
   '({American culture}
     {Mexican culture}
     {Iranian culture}
     {Russian culture}))

;; Populate the list, defined in metal-code.lisp, that maps language tags
;; into the appropriate Scone concepts for Langauge and Culture.

;; At Eric Riebling's request, we are using two-letter ISO language
;; symbols as the tags, since that encoding is used elsewhere in UIMA.
;; For now we are linking the culture to each language, though that
;; linkage not necessarily immutable outside of this project.

(setq *language-token-map*
      (list
       (list :en
	     (lookup-element {English language})
	     (lookup-element {American culture}))
       (list :es
	     (lookup-element {Spanish language})
	     (lookup-element {Mexican culture}))
       (list :fa
	     (lookup-element {Farsi language})
	     (lookup-element {Iranian culture}))
       (list :ru
	     (lookup-element {Russian language})
	     (lookup-element {Russian culture}))))

;; Popualte the list, defined in metal-code.lisp, that maps the type labels
;; we get from named-entity-extraction into Scone types that we can check.
;; This is not final until we decide what extraction software, or perhaps a
;; combination, we want to use.  For now, I'm just using the tags that
;; appear in the examples Eric Riebling sent me.

(setq *named-entity-type-map*
      (list
       (list :gpe_desc (lookup-element {place}))
       (list :number nil)
       (list :money (lookup-element {money}))
       (list :org_desc (lookup-element {organization}))
       (list :per_desc (lookup-element {person}))
       (list :organization (lookup-element {organization}))
       (list :person (lookup-element {person}))
       (list :date (lookup-element {date}))
       (list :contact_info (lookup-element {information object}))
       (list :duration (lookup-element {time interval}))
       (list :cardinal nil)
       (list :location (lookup-element {place}))
       (list :gpe (lookup-element {place}))
       (list :law (lookup-element {information object}))
       (list :cardinal (lookup-element {integer}))
       (list :animal (lookup-element {animal}))))

;;; ---------------------------------------------------------------------------
;;; KB support for Conceptual Metaphor Patterns, or CM-patterns for short.

;;; Checkable labels are used to indicate how the other items in the checkable
;;; relate to one another.

(new-type {checkable label} {information object})

(new-members {checkable label}
	     '({checkable x-is-y}
	       {checkable x-modifies-y}
	       {checkable x-has-y}
	       {checkable x-is-the-y-of-z} 
	       {checkable s-v}
	       {checkable v-o}
	       {checkable s-v-o}
	       {checkable x-at-location-y}))

(new-indv {checkable x-is-a-y} {checkable-label})

;;; Linking variable to make it easy to refer to these labels in compiled code.

(setq *x-is-y*
      (lookup-element {checkable x-is-y}))
(setq *x-is-a-y*
      (lookup-element {checkable x-is-a-y}))
(setq *x-modifies-y*
      (lookup-element {checkable x-modifies-y}))
(setq *x-has-y*
      (lookup-element {checkable x-has-y}))
(setq *x-is-the-y-of-z*
      (lookup-element {checkable x-is-the-y-of-z}))
(setq *s-v*
      (lookup-element {checkable s-v}))
(setq *v-o*
      (lookup-element {checkable v-o}))
(setq *s-v-o*
      (lookup-element {checkable s-v-o}))
(setq *x-at-location-y*
      (lookup-element {checkable x-at-location-y}))

;;; Also, store these elements as the :scone-element property of each
;;; (new-style) checkable keyword.

(setf (get :x-is-y :scone-element)
      (lookup-element {checkable x-is-y}))
(setf (get :x-is-a-y :scone-element)
      (lookup-element {checkable x-is-a-y}))
(setf (get :x-modifies-y :scone-element)
      (lookup-element {checkable x-modifies-y}))
(setf (get :x-has-y :scone-element)
      (lookup-element {checkable x-has-y}))
(setf (get :x-is-the-y-of-z :scone-element)
      (lookup-element {checkable x-is-the-y-of-z}))
(setf (get :s-v :scone-element)
      (lookup-element {checkable s-v}))
(setf (get :v-o :scone-element)
      (lookup-element {checkable v-o}))
(setf (get :s-v-o :scone-element)
      (lookup-element {checkable s-v-o}))
(setf (get :x-at-location-y :scone-element)
      (lookup-element {checkable x-at-location-y}))

;; These should find the same patterns on lookup.  They differ
;; only in how we assign LM-source and LM-target.  OK, it's
;; a kludge.
(new-eq {checkable x-is-y} {checkable x-is-a-y})

;;; Create the {cm pattern} type and its roles.
(setq *cm-pattern*
      (new-type {cm pattern} {information object}))

(setq *cm-checkable-label*
      (new-indv-role {cm checkable label} {cm pattern} {checkable label}))

(setq *cm-x-type*
      (new-indv-role {cm x-type} {cm pattern} {thing}))
(setq *cm-y-type*
      (new-indv-role {cm y-type} {cm pattern} {thing}))
(setq *cm-z-type*
      (new-indv-role {cm z-type} {cm pattern} {thing}))
(setq *cm-payload*
      (new-indv-role {cm payload} {cm pattern} {function}))

;;; ---------------------------------------------------------------------------
;;; Pronouns

;;; Until we are properly resolving pronoun references (if ever), we will just
;;; represent the type-constraint.  So "I" is an anonymous male person, etc.

;;; Here are the English ones.

(english (new-indv nil {person})
	 "I" "we" "me" "us" "you" "my" "mine" "myself" "our" "ours"
	 "ourselves" "who" "whom" "your" "yours" "yourself" "yourselves")

(english (new-indv nil {male person})
	 "he" "him" "his" "himself")

(english (new-indv nil {female person})
	 "she" "her" "hers" "herself")

(english (new-indv nil {thing})
	 "it" "them" "that" "this" "their" "theirs" "itself" "its"
	 "they" "themselves")

;;; Spanish.

(spanish (new-indv nil {person})
	 "yo" "me" "mi" "mis" "mí" "mío" "mía" "míos" "mías" "tú"
	 "vos" "te" "tu" "tus" "tí" "tuyo" "tuya" "tuyos" "tuyas"
	 "nosotros" "nosotras" "nos" "nuestro" "nuestra" "nuestros"
	 "nuestras" "vosotros" "vosotras" "os" "vuestro" "vuestra"
	 "vuestros" "vuestras" "le" "les" "se" "quien" "su" "sus"
	 "suyo" "suya" "suyos" "suyas" "usted" "ustedes")

(spanish (new-indv nil {male person})
	 "él" "ellos" "lo" "los")

(spanish (new-indv nil {female person})
	 "ella" "ellas" "la" "las")

(spanish (new-indv nil {thing})
	 "ello" "ellos" "lo" "los" "la" "las" "ésto" "éstos" "ésta"
	 "éstas" "eso" "esos" "esa" "esas")

;;; Farsi.

(farsi (new-indv nil {person})
       "من" "تو" "او" "ما" "شما" "انها")

;;; ---------------------------------------------------------------------------
;;; New verbs, with the type restrictions for the subject and object.
;;; Many of these are phrasal verbs.

(new-action-type
 {cough out}
 :agent-type {mammal}
 :object-type {bodily fluid}
 :english '(:verb "cough out" "cough up" "cough"))

(new-action-type {consume}
		 :agent-type {potential agent}
		 :object-type {stuff})

(new-action-type
 {eat}
 :parent {consume}
 :agent-type {animal}
 :object-type {edible material}
 :english '("feed on" "feeding on" "swallow" "swallow up"))

(new-action-type
 {feed}
 :agent-type {animate}
 :object-type {animate}
 :english '("feeding"))

(new-action-type {separate}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)
(new-eq {separate} {separate.v.02})
(new-is-a {separate} {physical action})


(new-action-type {damage}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)
(new-eq {damage} {damage.v.01})
(new-is-a {damage} {physical action})


(new-action-type {restrain}
   :agent-type {tangible}
   :object-type {animal}
   :english :verb)
(new-eq {restrain} {restrain.v.02})

(new-is-a {restrain} {physical action})


(new-action-type {cement (verb)}
		 :agent-type {person}
		 :object-type {tangible}
		 :english '(:verb "cement"))

(new-action-type {grab}
		 :agent-type {animate}
		 :object-type {physical object}
		 :english '(:verb "grab"))

(new-action-type {figure out}
		 :agent-type {person}
		 :object-type {information object}
		 :english '(:verb "figure out" "figure"))

(new-action-type {pour}
		 :agent-type {person}
		 :object-type {liquid}
		 :english '(:verb "pour"))

(new-action-type {crumble}
		 :agent-type {tangible}
		 :english '(:verb "crumble"))

(new-action-type {scour}
		 :agent-type {person}
		 :object-type {physical object}
		 :english '(:verb "scour"))

(new-action-type {soar}
		 :agent-type {physical object}
		 :english '(:verb "soar"))

(new-action-type {anchor (verb)}
		 :agent-type {person}
		 :object-type {physical object}
		 :english '(:no-iname :verb "anchor"))

(new-action-type {decide}
		 :agent-type {person}
		 :object-type {intangible}
		 :english '(:verb "decide"))

(new-action-type {fuel (verb)}
		 :agent-type {person}
		 :object-type {physical object}
		 :english '(:no-iname :verb "fuel"))

(new-action-type {chase}
		 :agent-type {animal}
		 :object-type {physical object}
		 :english '(:verb "chase"))

(new-action-type {creep}
		 :agent-type {animal}
		 :object-type {place}
		 :english '(:verb "creep"))

(new-action-type {stumble}
		 :agent-type {animal}
		 :object-type {animal}
		 :english '(:verb "stumble"))

(new-action-type {fill}
		 :agent-type {person}
		 :object-type {container}
		 :english '(:verb "fill"))

(new-action-type {shout}
		 :agent-type {person}
		 :english '(:verb "shout"))

(new-action-type {die}
		 :agent-type {animate}
		 :english '(:verb "die"))

(new-action-type {enter}
		 :agent-type {person}
		 :object-type {place}
		 :english '(:verb "enter"))

(new-action-type {smear}
		 :agent-type {potential agent}
		 :object-type {substance}
		 ;; "Smears" should be lemmatized.
		 :english '(:verb "smear" "smears"))  

(new-action-type {wake}
		 :agent-type {thing}
		 :object-type {animal}
		 :english '(:verb "wake"))

(new-action-type {lead out}
		 :agent-type {animal}
		 :object-type {animal}
		 :english '(:verb "lead out"))

(new-action-type {take out}
		 :agent-type {animal}
		 :object-type {physical object}
		 :english '(:verb "take out"))

(new-action-type {remove}
		 :agent-type {potential agent}
		 :object-type {tangible}
		 :english '(:verb "remove"))

(new-action-type {push}
		 :agent-type {potential agent}
		 :object-type {physical object}
		 :english '(:verb "push" "shove"))

(new-action-type {cut}
		 :object-type {physical object})

(new-action-type {touch}
		 :agent-type {tangible}
		 :object-type {tangible})

;;; Treat these as two different senses of "fan".
(new-action-type {fan (an object)}
		 :agent-type {person}
		 :object-type {physical object}
		 :english '(:no-iname :verb "fan"))

(new-action-type {fan (a fire)}
		 :agent-type {person}
		 :object-type {fire}
		 :english '(:no-iname :verb "fan"))

(new-action-type {inflate}
		 :agent-type {person}
		 :object-type {physical object})

(new-action-type {erupt}
		 :agent-type {volcano}
		 :english '(:verb "erupt"))

(new-action-type {flare up}
		 :agent-type {fire})

(new-action-type {pull}
		 :agent-type {animal}
		 :object-type {physical object}
		 :english '(:verb "pull"))

(new-action-type {experience}
		 :agent-type {animal}
		 :english '(:verb "experience"))

(new-action-type {depart}
		 :agent-type {person}
		 :english '(:verb "depart" "leave"))

(new-action-type {loom}
		 :agent-type {physical object}
		 :english '(:verb "loom" "hang over"))

(new-action-type {settle}
		 :agent-type {person}
		 :object-type {physical object}
		 :english '(:verb "settle"))

(new-action-type {sit}
		 :agent-type {animal}
		 :english '(:verb "sit" "sit down"))

(new-action-type {pass}
		 :agent-type {physical object}
		 :english '(:verb "pass" "pass through"))

(new-action-type {find}
		 :agent-type {animal}
		 :english '(:verb "find" "discover"
			    "stumble upon" "stumble"))

(new-action-type {roll}
		 :agent-type {person}
		 :object-type {physical object}
		 :english '(:verb "roll"))

(new-action-type {feel}
		 :agent-type {animal}
		 :object-type {object}
		 :english '(:verb "feel"))

(new-action-type {kindle}
		 :agent-type {person}
		 :object-type {fire}
		 :english '(:verb "kindle" "kindling"))

(new-action-type {fly}
		 :agent-type {physical object}
		 :english '(:verb "fly" "fly around" "fly round"))

(new-action-type {stand}
		 :agent-type {animate}
		 :english '(:verb "stand"))

(new-action-type {work}
		 :agent-type {potential agent}
		 :english '(:verb "work"))

(new-action-type {go}
		 :agent-type {person}
		 :english '(:verb "go"))

(new-action-type {mature (verb)}
		 :agent-type {animal}
		 :english '(:verb "mature"))

(new-action-type {reanimate}
		 :agent-type {potential agent}
		 :object-type {animate}
		 :english '(:verb "reanimate"))

(new-action-type {boast}
		 :agent-type {person}
		 :english '(:verb "boast" "brag"))

(new-action-type {shed}
		 :agent-type {animal}
		 :object-type {material}
		 :english '(:verb "shed"))

(new-action-type {slop}
		 :agent-type {person}
		 :object-type {material}
		 :english '(:verb "slop"))

(new-action-type {explode}
		 :agent-type {tangible}
		 :object-type {tangible}
		 :english '(:verb "explode" "blow up"))

(new-action-type {win (contest)}
		 :agent-type {potential agent}
		 :object-type {contest}
		 :english '(:verb "win"))

(new-action-type {win (prize)}
		 :agent-type {potential agent}
		 :object-type {tangible}
		 :english '(:verb "win"))

(new-action-type {conquer}
		 :agent-type {potential agent}
		 :object-type {land area}
		 :english '(:verb "conquer"))

(new-action-type {soak}
		 :agent-type {potential agent}
		 :object-type {tangible}
		 :english '(:verb "soak"))

(new-action-type {suck in}
		 :agent-type {animal}
		 :object-type {tangible}
		 :english '(:verb "suck"  "suck in"))

(new-action-type {lean on}
		 :agent-type {person}
		 :object-type {physical object}
		 :english :verb)

(new-action-type {blossom}
		 :agent-type {plant}
		 :english :verb)

(new-action-type {yell}
		 :agent-type {person}
		 :english '(:verb "bawl"))

(new-action-type {grow}
		 :agent-type {animate}
		 :english :verb)

(new-action-type {paralyze}
		 :agent-type {animate}
		 :object-type {animal}
		 :english :verb)

(new-action-type {move}
		 :agent-type {tangible}
		 :english :verb)

(new-action-type {come}
		 :parent {move}
		 :agent-type {physical object}
		 :english '(:verb "come" "approach"))

(new-action-type {go}
		 :parent {move}
		 :agent-type {physical object}
		 :english '(:verb "go" "go away" "recede"))

(new-split '({come} {go}))

(new-action-type {run}
		 :parent {move}
		 :agent-type {animal}
		 :english '(:verb "run"))

(new-action-type {flow}
		 :parent {move}
		 :agent-type {liquid}
		 :english '(:verb "flow"))

(new-action-type {fly}
		 :parent {move}
		 :agent-type {physical object}
		 :english '(:verb "fly"))

;; "thin" verb, in the sense of an animal losing weight.
(new-action-type {reduce (weight)}
	      :agent-type {animal}
	      :english '(:no-iname :verb "reduce" "thin" "thinning"))

(new-action-type {clean (verb)}
		 :agent-type {person}
		 :object-type {physical object}
		 :english '(:no-iname :verb "clean"))

(new-action-type {shield (verb)}
		 :agent-type {potential agent}
		 :object-type {person}
		 :english '(:verb "shield"))

(new-action-type {brake (verb)}
	      :agent-type {person}
	      :object-type {vehicle}
	      :english '(:no-iname :verb "brake"))

(new-action-type {drive (vehicle)}
		 :agent-type {person}
		 :object-type {vehicle}
		 :english '(:no-iname :verb "drive"))

(new-action-type {pulverize}
		 :agent-type {person}
		 :object-type {physical object}
		 :english '(:verb "pulverize"))

(new-action-type {detonate}
		 :agent-type {person}
		 :object-type {explosive device}
		 :english '(:verb "detonate"))

(new-action-type {undress}
		 :agent-type {person}
		 :object-type {person}
		 :english '(:verb "undress"))

(new-action-type {combat}
	      :agent-type {person}
	      :object-type {person}
	      :english '(:verb "combat"))

(new-action-type {multiply by}
	      :agent-type {person}
	      :object-type {number}
	      :english '(:verb "multiply by""multiply"
			 :noun "multiplication"))

(new-action-type {break}
		 :agent-type {potential agent}
		 :object-type {physical object}
		 :english '(:verb "break"))

(new-action-type {win}
		 :agent-type {potential agent}
		 :object-type {contest}
		 :english '(:verb "win"))

(new-action-type {awaken}
		 :agent-type {thing}
		 :object-type {animal}
		 :english :verb)

(new-action-type {hit}
		 :agent-type {tangible}
		 :object-type {tangible}
		 :english :verb)

(new-action-type {see}
		 :agent-type {animal}
		 :object-type {tangible}
		 :english :verb)

(new-action-type {jump to}
		 :agent-type {animal}
		 :object-type {place}
		 :english :verb)

(new-action-type {balance}
		 :agent-type {animal}
		 :object-type {physical object}
		 :english :verb)

(new-action-type {embrace}
		 :agent-type {person}
		 :object-type {tangible}
		 :english :verb)

(new-action-type {dissect}
		 :agent-type {person}
		 :object-type {physical object}
		 :english :verb)

(new-action-type {compete}
		 :agent-type {potential agent}
		 :object-type {physical object}
		 :english '(:verb "vie"))

(new-action-type {demand}
		 :agent-type {person}
		 :object-type {thing}
		 :english :verb)

(new-action-type {spank}
		 :agent-type {person}
		 :object-type {person}
		 :english :verb)

(new-action-type {jump}
		 :agent-type {animal}
		 :english '(:verb "leap" "hop"))

(new-action-type {kill}
		 :agent-type {potential agent}
		 :object-type {animate}
		 :english '(:verb "kill"))

(new-action-type {confront}
		 :agent-type {person}
		 :object-type {person}
		 :english '(:verb "face"))

(new-action-type {weaken}
		 :agent-type {process}
		 :object-type {animate}
		 :english '(:verb "debilitate"))

(new-action-type {glue (verb)}
		 :agent-type {person}
		 :object-type {tangible}
		 :english '(:no-iname :verb "glue"))

(new-action-type {illuminate}
		 :agent-type {potential agent}
		 :object-type {physical object}
		 :english '(:verb "light" "light up"))

(new-action-type {enslave}
		 :agent-type {person}
		 :object-type {person}
		 :english :verb)

(new-action-type {build}
		 :agent-type {legal-person}
		 :object-type {tangible}
		 :english '(:verb "build" "construct"))

(new-action-type {hang (execute)}
		 :agent-type {person}
		 :object-type {person}
		 :english '(:no-iname :verb "hang"))

(new-action-type {elbow (verb)}
		 :agent-type {person}
		 :object-type {person}
		 :english '(:no-iname :verb "elbow"))

(new-action-type {kick}
		 :agent-type {animal}
		 :object-type {physical object}
		 :english '(:no-iname :verb "kick"))

(new-action-type {say}
		 :agent-type {legal person}
		 :english '(:no-iname :verb "say"))

(new-action-type {attack}
		 :agent-type {animal}
		 :object-type {tangible})

(new-action-type {defend}
		 :agent-type {animal}
		 :object-type {tangible})

(new-action-type {strangle}
		 :agent-type {animal}
		 :object-type {animal})

(new-action-type {kill}
		 :agent-type {animal}
		 :object-type {living_thing.n.01})

(new-action-type {leave}
		 :agent-type {animate}
					; not putting an object-type for this one because "leave prison" and
					; "leave home" are both valid meanings
		 )

(new-action-type {water down}
		 :agent-type {physical_entity.n.01} ; I'm thinking rain can water something, for example
		 :object-type {substance})

(new-action-type {water down}
		 :agent-type {physical_entity.n.01} ; I'm thinking rain can water something, for example
		 :object-type {substance})

(new-action-type {exert force}
		 :agent-type {physical_entity.n.01}
		 :object-type {physical_entity.n.01})

(new-action-type {obstruct}
		 :agent-type {physical_entity.n.01}
		 :object-type {way.n.06})

(new-action-type {fight with}
		 :agent-type {animal}
		 :object-type {physical_entity.n.01})

(new-action-type {fight with}
		 :agent-type {animal}
		 :object-type {animate})

(new-action-type {lessen in force}
		 :agent-type {physical_entity.n.01})

(new-action-type {harden}
		 :agent-type {physical_entity.n.01}
		 :object-type {physical_entity.n.01})

(new-action-type {soften}
		 :agent-type {physical_entity.n.01}
		 :object-type {physical_entity.n.01})

(new-action-type {destroy}
		 :agent-type {physical_entity.n.01}
		 :object-type {physical_entity.n.01})

(new-action-type {capture}
		 :agent-type {animal}
		 :object-type {animate})

(new-action-type {collapse}
		 :agent-type {physical_entity.n.01})

(new-action-type {chain}
		 :agent-type {animate}
		 :object-type {physical_entity.n.01})

(new-action-type {connect}
		 :agent-type {physical_entity.n.01}
		 :object-type {physical_entity.n.01})

(new-action-type {move}
		 :agent-type {physical_entity.n.01}
		 :object-type {physical_entity.n.01})

(new-action-type {enter}
		 :agent-type {physical_entity.n.01}
					; not putting object-type for same reason I didn't put :object-type for {leave}
		 )

(new-action-type {build up}
		 :agent-type {physical_entity.n.01}
		 :object-type {physical_entity.n.01})

(new-action-type {let go of}
		 :agent-type {animate}
		 :object-type {physical_entity.n.01})

(new-action-type {travel}
		 :agent-type {physical_entity.n.01})

(new-action-type {betray}
		 :agent-type {animal}
		 :object-type {animal})

(new-action-type {deceive}
		 :agent-type {animal}
		 :object-type {animal})

(new-action-type {restrain}
		 :agent-type {physical_entity.n.01}
		 :object-type {physical_entity.n.01})

(new-action-type {climb into}
		 :agent-type {animal})

(new-action-type {overthrow}
		 :agent-type {person} ; not sure if {person} is the right concept, more like {person or group of people} or something like that
		 :object-type {person})

(new-action-type {hold down}
		 :agent-type {person}
		 :object-type {physical_entity.n.01})

(new-action-type {sharpen}
		 :agent-type {person}
		 :object-type {physical_entity.n.01})

(new-action-type {dull}
		 :agent-type {person}
		 :object-type {physical_entity.n.01})

(new-action-type {eat}
		 :agent-type {animal}
		 :object-type {food})

(new-action-type {launch}
		 :parent {move}
		 :agent-type {animal}
		 :object-type {physical object})

(new-action-type {suppress}
		 :agent-type {thing}
		 :object-type {event})

;;; New phrasal verbs.  From Amos.

(new-action-type {act out}
  :agent-type {person}
  :english :verb)
(new-eq {act out} {act_out.v.01})


(new-action-type {add up}
  ; you can sum up anything... tangibles, intangibles, anything
  :agent-type {physical object} ; e.g., a human or a computer
  :object-type {thing}
  :english :verb)
(new-eq {add up} {total.v.02})


(new-action-type {adds up to}
  :object-type {number}
  :english :verb)
(new-eq {adds up to} {total.v.01})


(new-action-type {add on}
  :agent-type {physical object}
  :object-type {thing}
  :english :verb)
(new-eq {add on} {add_on.v.01})


(new-action-type {auction off}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {auction off} {auction.v.01})


(new-action-type {make backup of}
  :agent-type {person}
  :object-type {file}
  :english :verb)
(new-eq {make backup of} {back_up.v.04})


(new-action-type {move back}
  :agent-type {animal}
  :english :verb)
(new-eq {move back} {back_up.v.02})


(new-action-type {give support to}
  :agent-type {person}
  :object-type {person}
  :english :verb)
(new-eq {give support to} {support.v.01})


(new-action-type {bail out (of prison)}
  :agent-type {person}
  :object-type {person}
  :english :verb)
(new-eq {bail out (of prison)} {bail_out.v.01})


(new-action-type {bail out (the water)}
  :agent-type {person}
  :object-type {water}
  :english :verb)
(new-eq {bail out (the water)} {bail_out.v.02})


(new-action-type {balance out}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {balance out} {balance.v.04})


(new-action-type {bandy about}
  :agent-type {person}
  :object-type {idea.n.01}
  :english :verb)
(new-eq {bandy about} {bandy_about.v.01})


(new-action-type {bandy about}
  :agent-type {person}
  :object-type {idea.n.01}
  :english :verb)
(new-eq {bandy about} {bandy_about.v.01})


(new-action-type {bank up}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {bank up} {pile_up.v.02})


(new-action-type {bark up the wrong tree}
  :agent-type {person}
  :english :verb) ; right now I can't think of a single word that captures the meaning of this phrase


(new-action-type {bat in} ; apparently something to do with baseball?!
  :agent-type {person}
  :english :verb)


(new-action-type {be like}
  :agent-type {thing}
  :object-type {thing}
  :english :verb)


(new-action-type {bear out}
  :agent-type {person}
  :object-type {idea.n.01}
  :english :verb)
(new-eq {bear out} {corroborate.v.03})


(new-action-type {beat out (win competition)}
  :agent-type {legal person}
  :object-type {legal person}
  :english :verb)
(new-eq {beat out (win competition)} {beat.v.01})
(english {beat out (win competition)} :iname "edge out")


(new-action-type {beat out (a rhythm)}
  :agent-type {animal}
  :object-type {rhythm.n.01}
  :english :verb)
(new-eq {beat out (a rhythm)} {beat_out.v.02})


(new-action-type {beat up}
  :agent-type {animal}
  :object-type {animal}
  :english :verb)
(new-eq {beat up} {beat.v.02})


(new-action-type {bed down}
  :agent-type {person} ; animals don't have beds, so they can't literally "go to bed"
  :english :verb)
(new-eq {bed down} {bed_down.v.01})


(new-action-type {beef up}
  :agent-type {animal}
  :object-type {animal}
  :english :verb)
(new-eq {beef up} {strengthen.v.01})


(new-action-type {belch out}
  :agent-type {animal}
  :object-type {food}
  :english :verb)
; {burp.v.01} does not necessarily imply something is coming OUT other than gas,
; so IMHO that is not eq to {belch out}


(new-action-type {go belly up}
  :agent-type {company}
  :english :verb)
(new-eq {go belly up} {bankrupt.v.01})


(new-action-type {bind up (tie up)}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {bind up (tie up)} {bind.v.03})


(new-action-type {bind up (bandage up)}
  :agent-type {person}
  :object-type {wound.n.01}
  :english :verb)
(new-eq {bind up (bandage up)} {bind.v.04})


(new-action-type {bind up (books)}
  :agent-type {person}
  :object-type {paper.n.04} ; do people make bindings for things other than paper???
  :english :verb)
(new-eq {bind up (books)} {bind.v.07})


(new-action-type {black out (destroy)}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {black out (destroy)} {black_out.v.01})


(new-action-type {black out (make dark)}
  :agent-type {place}
  :english :verb)
(new-eq {black out (make dark)} {black_out.v.02})


(new-action-type {black out (fall unconscious)}
  :agent-type {animal}
  :english :verb)
(new-eq {black out (fall unconscious)} {zonk_out.v.01})


(new-action-type {blast away (weapons firing)}
  :agent-type {weapon.n.01}
  :english :verb)
(new-eq {blast away (weapons firing)} {open_fire.v.01})


(new-action-type {blast away (loud music)}
  :agent-type {sound.n.01}
  :english :verb)
; no single word that means "sound loudly," as far as I know


(new-action-type {blast off}
  :agent-type {vehicle.n.01}
  :english :verb)
(new-eq {blast off} {blast_off.v.01})


(new-action-type {bleed off}
  :object-type {physical object}
  :english :verb)
(new-eq {bleed off} {extract.v.01})


(new-action-type {block up}
  :agent-type {physical object}
  :object-type {way.n.06}
  :english :verb)
(new-eq {block up} {barricade.v.01})

(english {block up} :iname "choke off")
(english {block up} :iname "choke up")
(english {block up} :iname "close off")


(new-action-type {blot out}
  :agent-type {person}
  :object-type {idea.n.01}
  :english :verb)
(new-eq {blot out} {obscure.v.05})


(new-action-type {blow up}
  :agent-type {physical object}
  :english :verb)
(new-eq {blow up} {explode.v.01})


(new-action-type {blow out}
  :agent-type {physical object}
  :english :verb)
(new-eq {blow out} {blow_out.v.01})


(new-action-type {blurt out}
  :agent-type {person}
  :object-type {statement.n.01}
  :english :verb)
(new-eq {blurt out} {blurt_out.v.01})


(new-action-type {bog down (get stuck)}
  :agent-type {physical object}
  :english :verb)
(new-eq {bog down (get stuck)} {bog_down.v.01})


(new-action-type {bog down (get something else stuck)}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {bog down (get something else stuck)} {mire.v.02})


(new-action-type {boil over}
  :agent-type {liquid}
  :english :verb)
(new-eq {boil over} {boil_over.v.01})


(new-action-type {bone up}
  :agent-type {person}
  :object-type {idea.n.01}
  :english :verb)
(new-eq {bone up} {cram.v.03})

; WordNet lists "Her voice booms out the words of the song" as an example, but 
; I decided to put {physical object} down as the agent, since it's not actually
; her voice that is making a deep sound, but rather her vocal cords
(new-action-type {boom out}
  :agent-type {physical object}
  :english :verb)
(new-eq {boom out} {boom.v.04})


(new-action-type {bottle up}
  :agent-type {person}
  :object-type {abstraction.n.06} ; e.g. "smile," "anger," etc.
  :english :verb)
(new-eq {bottle up} {inhibit.v.03})


(new-action-type {bottom out}
  :agent-type {physical object}
  :english :verb)
(new-eq {bottom out} {bottom_out.v.02})


(new-action-type {bounce back}
  :agent-type {person}
  :english :verb)
(new-eq {bounce back} {get_well.v.01})


(new-action-type {bow down}
  :agent-type {person}
  :english :verb)
(new-eq {bow down} {bow.v.01})


(new-action-type {bow out}
  :agent-type {person}
  :object-type {situation.n.03}
  :english :verb)
(new-eq {bow out} {bow_out.v.02})


(new-action-type {bowl over}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {bowl over} {overturn.v.02})


(new-action-type {branch out}
  :agent-type {organization}
  :english :verb)
(new-eq {branch out} {diversify.v.03})


(new-action-type {brazen out}
  :agent-type {animal}
  :object-type {situation.n.03}
  :english :verb)


(new-action-type {break away (from place)}
  :agent-type {animal}
  :object-type {place}
  :english :verb)
(new-eq {break away (from place)} {break.v.07})


(new-action-type {break away (from object)}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {break away (from object)} {chip.v.01})


(new-action-type {break down}
  :agent-type {physical object}
  :english :verb)
(new-eq {break down} {crumble.v.01})


(new-action-type {break in (enter uninvited)}
  :agent-type {animal}
  :object-type {place}
  :english :verb)
(new-eq {break in (enter uninvited)} {break_in.v.01})


(new-action-type {break in (destroy inward)}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)
(new-eq {break in (destroy inward)} {break_in.v.05})


(new-action-type {break up}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {break up} {decompose.v.01})


(new-action-type {break even}
  :agent-type {legal person}
  :english :verb)
(new-eq {break even} {break_even.v.02})


(new-action-type {bring up}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {bring up} {raise.v.02})


(new-action-type {bring down}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {bring down} {lower.v.01})


(new-action-type {bring along}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)
(new-eq {bring along} {bring.v.01})


(new-action-type {bring about (in opposite direction)}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {bring about (in opposite direction)} {bring_about.v.01})


(new-action-type {bring about (make exist)}
  :agent-type {physical object}
  :object-type {thing} ; can be intangible (an idea) or tangible (a landslide), so pretty much anything
  :english :verb)
(new-eq {bring about (make exist)} {produce.v.03})


(new-action-type {brush off}
  :agent-type {person}
  ; e.g. "brush off the dust" -- brushing off intangibles is a more metaphorical meaning
  :object-type {physical object}
  :english :verb)
(new-eq {brush off} {dismiss.v.01})


(new-action-type {brush up}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {brush up} {polish.v.01})


(new-action-type {buckle up}
  :agent-type {person}
  :object-type {seat_belt.n.01}
  :english :verb)


(new-action-type {buckle down}
  :agent-type {person}
  :english :verb)
(new-eq {buckle down} {slave.v.01})


(new-action-type {brush up}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {brush up} {polish.v.01})


(new-action-type {build up}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
; WordNet doesn't seem to have a literal, physical definition for "build up,"
; which is lame because "Pressure built up as the submarine descended" seems
; like an entirely valid way to use this to describe a physical process


(new-action-type {bump up}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {bump up} {push_up.v.01})


(new-action-type {bundle up}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {bundle up} {bundle.v.01})


(new-action-type {buoy up}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {buoy up} {buoy.v.02})


(new-action-type {burn up}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {burn up} {burn_down.v.01})


(new-action-type {burn up}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {burn up} {burn_down.v.01})


; burn out same verb as blow out

(new-action-type {bust out}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {bust out} {bring_out.v.07})


(new-action-type {bust up}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {bust up} {bust_up.v.01})


(new-action-type {butt in}
  :agent-type {person}
  :object-type {conversation.n.01}
  :english :verb)
(new-eq {butt in} {chime_in.v.01})


(new-action-type {buy up}
  :agent-type {legal person}
  :object-type {company}
  :english :verb)
(new-eq {buy up} {take_over.v.05})

; same for buy out

(new-action-type {buzz off}
  :agent-type {person}
  :english :verb)
(new-eq {buzz off} {scram.v.01})
(english {buzz off} :iname "fudge over")


(new-action-type {call up}
  :agent-type {person}
  :object-type {person}
  :english :verb)
(new-eq {call up} {call.v.03})


(new-action-type {call in (summon)}
  :agent-type {person}
  :object-type {animal}
  :english :verb)
(new-eq {call in (summon)} {call_in.v.01})


(new-action-type {call in (make phone call)}
  :agent-type {person}
  :object-type {person}
  :english :verb)
(new-eq {call in (make phone call)} {call_in.v.05})


(new-action-type {call on}
  :agent-type {person}
  :object-type {person}
  :english :verb)
(new-eq {call on} {call_on.v.01})


(new-action-type {call off}
  :agent-type {person}
  :object-type {event}
  :english :verb)
(new-eq {call off} {cancel.v.01})


(new-action-type {calm down}
  :agent-type {person}
  :object-type {person}
  :english :verb)
(new-eq {calm down} {calm.v.01})


(new-action-type {camp out}
  :agent-type {person}
  :english :verb)
(new-eq {camp out} {camp.v.01})


(new-action-type {carry on}
  :agent-type {person}
  :object-type {abstraction.n.06}
  :english :verb)
(new-eq {carry on} {continue.v.02})


(new-action-type {carry out}
  :agent-type {person}
  :object-type {undertaking.n.01}
  :english :verb)
(new-eq {carry on} {follow_through.v.02})


(new-action-type {carry over}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {carry over} {carry_over.v.02})


(new-action-type {carry off}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)
(new-eq {carry off} {take_away.v.01})


(new-action-type {cart off}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {cart off} {cart_off.v.01})


(new-action-type {carve out}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {carve out} {carve_out.v.02})


(new-action-type {cash in}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {cash in} {cash.v.01})


(new-action-type {catch up}
  :agent-type {person}
  :object-type {activity.n.01}
  :english :verb)
(new-eq {catch up} {catch_up.v.01})


(new-action-type {catch on (become popular)}
  :agent-type {abstraction.n.06}
  :english :verb)
(new-eq {catch on (become popular)} {catch_on.v.02})


(new-action-type {catch on (understand)}
  :agent-type {animal}
  :english :verb)
(new-eq {catch on (understand)} {catch_on.v.01})


(new-action-type {cave in}
  :agent-type {physical object}
  :english :verb)
(new-eq {cave in} {collapse.v.01})


(new-action-type {chalk up}
  :agent-type {person}
  :object-type {debt}
  :english :verb)
(new-eq {chalk up} {chalk_up.v.01})


(new-action-type {charge off}
  :agent-type {legal person}
  :object-type {debt}
  :english :verb)


(new-action-type {chart out}
  :agent-type {person}
  :object-type {path.n.04}
  :english :verb)


(new-action-type {check in}
  :agent-type {person}
  :english :verb)
(new-eq {check in} {check_in.v.01})


(new-action-type {check out (leave)}
  :agent-type {person}
  :english :verb)
(new-eq {check out (leave)} {check_out.v.02})


(new-action-type {check out (examine)}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {check out (examine)} {check.v.01})


(english {check out (examine)} :iname "check up")


(new-action-type {cheer up}
  :agent-type {person}
  :object-type {animal}
  :english :verb)
(new-eq {cheer up} {cheer.v.03})


(new-action-type {cheer on}
  :agent-type {person}
  :object-type {person}
  :english :verb)


(new-action-type {chew up}
  :agent-type {animal}
  :object-type {food}
  :english :verb)
(new-eq {chew up} {chew.v.01})


(new-action-type {chip in}
  :agent-type {person}
  :object-type {campaign.n.02}
  :english :verb)
(new-eq {chip in} {contribute.v.02})


(new-action-type {chop down}
  :agent-type {animal}
  :object-type {tree.n.01}
  :english :verb)
(new-eq {chop down} {chop_down.v.01})


(new-action-type {chop up}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)
(new-eq {chop up} {chop.v.01})


(new-action-type {clean up}
  :agent-type {person}
  :object-type {thing}
  :english :verb)
(new-eq {clean up} {tidy.v.01})


(new-action-type {clean out}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {clean out} {clean_out.v.01})


(new-action-type {clear up}
  :agent-type {physical object}
  :english :verb)
(new-eq {clear up} {clear_up.v.04})


(new-action-type {clip off}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)
(new-eq {clip off} {cut_off.v.03})


(new-action-type {close down}
  :agent-type {legal person}
  :object-type {legal person}
  :english :verb)
(new-eq {close down} {close_up.v.01})


(new-action-type {close in}
  :agent-type {animal}
  :object-type {animal}
  :english :verb)
(new-eq {close in} {close_in.v.01})


(new-action-type {clown around}
  :agent-type {person}
  :english :verb)
(new-eq {clown around} {clown.v.01})


(new-action-type {clutch}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)
(new-eq {clutch} {cling_to.v.01})


(new-action-type {coil up}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)
(new-eq {coil up} {roll_up.v.01})


(new-action-type {come about}
  :object-type {event}
  :english :verb)
(new-eq {come about} {happen.v.01})


(new-action-type {come in}
  :agent-type {animal}
  :object-type {place}
  :english :verb)
(new-eq {come in} {enter.v.01})


(new-action-type {come on}
  :agent-type {animal}
  :object-type {event}
  :english :verb)
(new-eq {come on} {come_on.v.01})


(new-action-type {come out}
  :agent-type {animal}
  :object-type {place}
  :english :verb)
(new-eq {come out} {leave.v.01})
(new-type {get away} {come out})
(new-eq {get away} {escape.v.01})


(new-action-type {come up}
  :agent-type {physical object}
  :english :verb)
(new-eq {come up} {surface.v.01})


(new-action-type {come across}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)
(new-eq {come across} {meet.v.01})


(new-action-type {come through}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {come through} {break_through.v.02})


(new-action-type {come along (come into existence)}
  :agent-type {physical object}
  :english :verb)
(new-eq {come along (come into existence)} {appear.v.05})


(new-action-type {come along (progessing)}
  :agent-type {thing} ; not too useful, but pretty much everything can improve...
  :english :verb)
(new-eq {come along (progessing)} {progress.v.01})


(new-action-type {come by}
  :agent-type {physical object}
  :object-type {place}
  :english :verb)
(new-eq {come by} {drop_by.v.01})


(new-action-type {come down}
  :agent-type {physical object}
  :english :verb)
(new-eq {come down} {descend.v.01})


(new-action-type {concrete (cover with concrete)}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {concrete (cover with concrete)} {concrete.v.01})


(new-action-type {concrete (coalesce)}
  :agent-type {physical object}
  :english :verb)
(new-eq {concrete (coalesce)} {concrete.v.02})


(new-action-type {conjure up}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {conjure up} {raise.v.07})


(new-action-type {contract out}
  :agent-type {legal person}
  :object-type {work.n.01}
  :english :verb)
(new-eq {contract out} {contract_out.v.01})
(english {contract out} :iname "farm out")


(new-action-type {cook up}
  :agent-type {person}
  :object-type {soup.n.01}
  :english :verb)
(new-eq {cook up} {concoct.v.02})


(new-action-type {cool down}
  :agent-type {physical object}
  :english :verb)
(new-eq {cool down} {cool.v.01})
; Despite WordNet having no physical definition for "cool off,"
; saying "After baking the pizza, let it cool off for 5 minutes"
; is a perfectly valid, literal meaning, so I am just making it a
; synonym for "cool down"
(english {cool down} :iname "cool off")


(new-action-type {coop up}
  :agent-type {animal}
  :english :verb)
(new-eq {coop up} {coop_up.v.01})


(new-action-type {copy out}
  :agent-type {person}
  :object-type {written_communication.n.01}
  :english :verb)
; "copy out" is apparently a careful version of "copy"
(new-is-a {copy out} {copy.v.01})


(new-action-type {cough up}
  :agent-type {animal}
  :object-type {bodily fluid}
  :english :verb)
(new-eq {cough up} {expectorate.v.02})


(new-action-type {cover up}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)
(new-eq {cover up} {cover.v.01})


(new-action-type {crack up}
  :agent-type {animal}
  :english :verb)
(new-eq {crack up} {break_up.v.19})


(new-action-type {crack down}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)
(new-eq {crack down} {crack.v.04})


(new-action-type {crank up}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {crank up} {crank.v.03})


(new-action-type {crank out}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {crank out} {grind_out.v.01})


(new-action-type {crash out}
  :agent-type {vehicle.n.01}
  :object-type {place}
  :english :verb)
(new-eq {crash out} {crash.v.02})


(new-action-type {creep up}
  :agent-type {animal}
  :english :verb)
(new-eq {creep up} {creep_up.v.01})


(new-action-type {crop up}
  :agent-type {physical object}
  :english :verb)
(new-eq {crop up} {crop_up.v.01})


(new-action-type {cross out}
  :agent-type {person}
  :object-type {statement.n.01}
  :english :verb)
(new-eq {cross out} {cross_off.v.01})


(new-action-type {cry out}
  :agent-type {person}
  :object-type {statement.n.01}
  :english :verb)
(new-eq {cross out} {exclaim.v.01})


(new-action-type {cry out}
  :agent-type {person}
  :object-type {statement.n.01}
  :english :verb)
(new-eq {cry out} {exclaim.v.01})
(english {cry out} :iname "cry down")


(new-action-type {curl up}
  :agent-type {animal}
  :english :verb)
(new-eq {curl up} {curl_up.v.01})


(new-action-type {cut off}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {cut off} {cut_off.v.03})


(new-action-type {cut back}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)
(new-eq {cut back} {snip.v.02})


(new-action-type {cut out}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {cut out} {cut_out.v.01})


(new-action-type {cut loose}
  :agent-type {animal}
  :object-type {animal}
  :english :verb)


(new-action-type {cut up}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)
(new-eq {cut up} {carve.v.03})


(new-action-type {dash off}
  :agent-type {person}
  :object-type {communication.n.02}
  :english :verb)
(new-eq {dash off} {dash_off.v.01})


(new-action-type {deck out}
  :agent-type {person}
  :english :verb)
(new-eq {deck out} {overdress.v.02})


(new-action-type {dig up}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)
(new-eq {dig up} {dig.v.04})


(new-action-type {dine out}
  :agent-type {person}
  :english :verb)
(new-eq {dine out} {eat_out.v.01})


(new-action-type {dish out}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)
(new-eq {dish out} {distribute.v.01})


(new-action-type {dish up}
  :agent-type {person}
  :object-type {food}
  :english :verb)
(new-eq {dish up} {serve.v.06})


(new-action-type {divide up}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)
(new-eq {divide up} {share.v.04})



(new-action-type {do in}
  :agent-type {animal}
  :object-type {animal}
  :english :verb)
(new-eq {do in} {neutralize.v.04})
(english {do in} :iname "do away with")


(new-action-type {doze off}
  :object-type {physical object}
  :english :verb)
(new-eq {doze off} {fall_asleep.v.01})


(new-action-type {drag on}
  :agent-type {event}
  :english :verb)
(new-eq {drag on} {drag_on.v.01})


(new-action-type {draw up (straigthen up)}
  :agent-type {animal}
  :english :verb)
(new-eq {draw up (straigthen up)} {draw_up.v.01})


(new-action-type {draw up (stop car)}
  :agent-type {animal}
  :english :verb)
(new-eq {draw up (stop car)} {draw_up.v.05})


(new-action-type {draw up (drawing)}
  :agent-type {person}
  :english :verb)
(new-eq {draw up (drawing)} {draw.v.06})


(new-action-type {dream up}
  :agent-type {person}
  :object-type {idea.n.01}
  :english :verb)
(new-eq {dream up} {think_up.v.01})


(new-action-type {dream on}
  :agent-type {animal}
  :english :verb)


(new-action-type {dredge up}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)


(new-action-type {dress down (dress informally)}
  :agent-type {person}
  :english :verb)
(new-eq {dress down (dress informally)} {dress_down.v.02})


(new-action-type {dress down (scold)}
  :agent-type {person}
  :object-type {person}
  :english :verb)
(new-eq {dress down (scold)} {call_on_the_carpet.v.01})


(new-action-type {drink up}
  :agent-type {person}
  :object-type {beverage.n.01}
  :english :verb)
(new-eq {drink up} {drain_the_cup.v.01})


(new-action-type {drop off (remove and leave)}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {drop off (remove and leave)} {drop.v.08})


(new-action-type {drop off (retreat)}
  :agent-type {animal}
  :english :verb)
(new-eq {drop off (retreat)} {fall_back.v.04})


(new-action-type {drop out (get away from society)}
  :agent-type {person}
  :english :verb)
(new-eq {drop out (get away from society)} {drop_out.v.02})


(new-action-type {drop out (give up)}
  :agent-type {person}
  :english :verb)
(new-eq {drop out (give up)} {drop.v.08})


(new-action-type {drown out}
  :agent-type {sound.n.01}
  :object-type {sound.n.01}
  :english :verb)
(new-eq {drown out} {drown_out.v.01})


(new-action-type {drum up}
  :agent-type {person}
  :object-type {abstraction.n.06}
  :english :verb)
(new-eq {drum up} {beat_up.v.02})


(new-action-type {dry up}
  :agent-type {physical object}
  :english :verb)
(new-eq {dry up} {exsiccate.v.01})
(english {dry up} :iname "dry out")


(new-action-type {dry out}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {dry out} {dry.v.01})


(new-action-type {dust off}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {dust off} {dust.v.01})


(new-action-type {ease up}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)
(new-eq {ease up} {ease_up.v.03})


(new-action-type {eat up}
  :agent-type {animal}
  :object-type {food}
  :english :verb)
(new-eq {eat up} {eat_up.v.01})


(new-action-type {eat up}
  :agent-type {animal}
  :object-type {food}
  :english :verb)
(new-eq {eat up} {eat_up.v.01})


(new-action-type {eat away}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {eat away} {erode.v.02})


(new-action-type {end up}
  :agent-type {physical object}
  :english :verb)
(new-eq {end up} {finish_up.v.02})


(new-action-type {even out}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {even out} {flush.v.04})


(new-action-type {face off}
  :agent-type {animal}
  :object-type {animal}
  :english :verb)
(new-eq {face off} {confront.v.01})


(new-action-type {fall back (fall down)}
  :agent-type {animal}
  :english :verb)
(new-eq {fall back (fall down)} {fall_back.v.01})


(new-action-type {fall back (retreat)}
  :agent-type {animal}
  :english :verb)
(new-eq {fall back (retreat)} {fall_back.v.03})


(new-action-type {fall off}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {fall off} {fall_off.v.01})


(new-action-type {fall out}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {fall out} {come_out.v.05})


(new-action-type {fall into}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {fall into} {fall.v.01})


(new-action-type {fall through}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {fall through} {fall.v.01})


(new-action-type {fall apart}
  :agent-type {physical object}
  :english :verb)
(new-eq {fall apart} {crumble.v.02})


(new-action-type {fax in}
  :agent-type {person}
  :object-type {communication.n.02}
  :english :verb)
(new-eq {fax in} {fax.v.01})

; didn't do "feed up"

(new-action-type {feel up}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {feel up} {touch.v.01})


(new-action-type {fence off}
  :agent-type {person}
  :object-type {structure.n.01}
  :english :verb)
(new-eq {fence off} {fence.v.01})


(new-action-type {fend off}
  :agent-type {animal}
  :object-type {situation.n.03}
  :english :verb)
(new-eq {fend off} {debar.v.02})


(new-action-type {ferret out}
  :agent-type {animal}
  :object-type {intangible}
  :english :verb)
(new-eq {ferret out} {ferret_out.v.01})


(new-action-type {fess up}
  :agent-type {person}
  :object-type {activity.n.01}
  :english :verb)
(new-eq {fess up} {make_a_clean_breast_of.v.01})


(new-action-type {fight back}
  :agent-type {animal}
  :english :verb)
(new-eq {fight back} {fight_back.v.01})


(new-action-type {fight off}
  :agent-type {animal}
  :object-type {animal}
  :english :verb)
(new-eq {fight off} {repel.v.03})


(new-action-type {fight on}
  :agent-type {animal}
  :english :verb)


(new-action-type {figure out}
  :agent-type {animal}
  :object-type {question.n.02}
  :english :verb)
(new-eq {figure out} {solve.v.01})


(new-action-type {fill up (make full)}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {fill up (make full)} {fill.v.01})
(english {fill up (make full)} :iname "fill in")


(new-action-type {fill up (become full)}
  :agent-type {physical object}
  :english :verb)
(new-eq {fill up (become full)} {fill.v.02})


;;; Commenting this out because {close.v.15} is a type of {repair.v.01}
;;; which conflicts with {fix up}
;(new-action-type {fill up (repair opening)}
;  :agent-type {person}
;  :object-type {space.n.02}
;  :english :verb)
;(new-eq {fill up (repair opening)} {close.v.15})


(new-action-type {fill out}
  :agent-type {person}
  :object-type {written_communication.n.01}
  :english :verb)
(new-eq {fill out} {complete.v.05})


(new-action-type {find out (on purpose)}
  :agent-type {person}
  :object-type {information}
  :english :verb)
(new-eq {find out (on purpose)} {determine.v.01})


(new-action-type {find out (accidentally)}
  :agent-type {person}
  :object-type {information}
  :english :verb)
(new-eq {find out (accidentally)} {learn.v.02})


(new-action-type {finish up}
  :agent-type {person}
  :object-type {undertaking.n.01}
  :english :verb)
(new-eq {finish up} {get_through.v.01})
(english {finish up} :iname "follow through")
(english {finish up} :iname "follow up")


(new-action-type {firm up}
  :agent-type {person}
  :object-type {intangible}
  :english :verb)
(new-eq {firm up} {firm_up.v.01})


(new-action-type {fit in}
  :agent-type {physical object}
  :object-type {space.n.02}
  :english :verb)
(new-eq {fit in} {fit.v.02})


(new-action-type {fix up}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {fix up} {repair.v.01})
(new-type {fill up (repair opening)} {fix up})
(english {fill up (repair opening)} :iname "fill up")
(new-eq {fill up (repair opening)} {close.v.15})


(new-action-type {flare up}
  :agent-type {chemical process}
  :english :verb)
(new-eq {flare up} {flare_up.v.01})


(new-action-type {flash back}
  :agent-type {person}
  :english :verb)
(new-eq {flash back} {cut_back.v.01})


(new-action-type {flatten out}
  :agent-type {physical object}
  :english :verb)
(new-eq {flatten out} {flatten.v.02})


(new-action-type {flesh out}
  :agent-type {animal}
  :object-type {animal}
  :english :verb)
(new-eq {flesh out} {fatten.v.01})


(new-action-type {flush out}
  :agent-type {animal}
  :object-type {physical object}
  :english :verb)
(new-eq {flush out} {flush.v.05})


(new-action-type {fold up}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {fold up} {fold.v.01})


(new-action-type {follow suit}
  :agent-type {person}
  :english :verb)
(new-eq {follow suit} {follow_suit.v.01})


(new-action-type {fool around}
  :agent-type {person}
  :english :verb)
(new-eq {fool around} {horse_around.v.01})


(new-action-type {fork over}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {fork over} {hand_over.v.01})


(new-action-type {fort up}
  :agent-type {person}
  :english :verb)
(new-eq {fort up} {fort.v.01})


(new-action-type {freak out}
  :agent-type {person}
  :english :verb)
(new-eq {freak out} {freak_out.v.01})


(new-action-type {free up}
  :agent-type {person}
  :object-type {intangible}
  :english :verb)
(new-eq {free up} {unblock.v.03})


(new-action-type {freshen up}
  :agent-type {physical object}
  :object-type {physical object}
  :english :verb)
(new-eq {freshen up} {refresh.v.04})


(new-action-type {frighten off}
  :agent-type {physical object}
  :object-type {animal}
  :english :verb)
(new-eq {frighten off} {daunt.v.01})


(new-action-type {fritter away}
  :agent-type {person}
  :object-type {intangible}
  :english :verb)
(new-eq {fritter away} {fritter.v.01})


(new-action-type {fuck up}
  :agent-type {person}
  :object-type {intangible}
  :english :verb)
(new-eq {fuck up} {botch.v.01})


(new-action-type {gas up}
  :agent-type {person}
  :object-type {vehicle.n.01}
  :english :verb)
(new-eq {gas up} {gas_up.v.01})


(new-action-type {gear up}
  :agent-type {person}
  :object-type {physical object}
  :english :verb)
(new-eq {gear up} {fix.v.12})


(new-action-type {get out}
  :agent-type {animal}
  :object-type {place}
  :english :verb)
(new-eq {get out} {exit.v.01})

;;; commenting this out because {escape.v.01} is a type of {leave.v.01}
;;; so it conflicts with {come out}
;(new-action-type {get away}
;  :agent-type {animal}
;  :object-type {structure.n.01}
;  :english :verb)
;(new-eq {get away} {escape.v.01})

;;; From John.

(new-action-type {get back}
   :agent-type {animate}  ;possibly physical object
   :english :verb)

(new-action-type {get off}
   :agent-type {animate}  
   :english :verb)
   
; (new-action-type {get through}
   ; :agent-type {animate}
   ; :object-type {thing}
   ; :english :verb)
   
(new-action-type {get along}
   :agent-type {animate}  
   :english :verb)  
   
; (new-action-type {get by}
   ; :agent-type {thing}
   ; :object-type {thing}
   ; :english :verb)

; (new-action-type {get by (survive)}
   ; :agent-type {thing}
   ; :english :verb)
   
; (new-action type {get down}
	; :agent-type {animate}
	; :english :verb)

; (new-action-type {get even}
   ; :agent-type {person}
   ; :english :verb)
   
(new-action-type {get together}
   :agent-type {group.n.01}
   :english :verb)

; (new-action-type {get on}
   ; :agent-type {thing}
   ; :object-type {thing}
   ; :english :verb)
   
; (new-action-type {give away}
   ; :agent-type {animate}
   ; :object-type {physical object}
   ; :english :verb)
   
(new-action-type {give back}
   :agent-type {animate}
   :object-type {physical object}
   :english :verb)

; (new-action-type {give off}
   ; :agent-type {thing}
   ; :object-type {thing}
   ; :english :verb)  
   
; (new-action-type {give out}
   ; :agent-type {thing}
   ; :object-type {thing}
   ; :english :verb)
   
; (new-action-type {give out (collapse)}
   ; :agent-type {thing}
   ; :english :verb)   

; (new-action-type {give up}
   ; :agent-type {animate}
   ; :english :verb)
   
; (new-action-type {give up (hand over)}
   ; :agent-type {animate}
   ; :object-type {thing}
   ; :english :verb)
   
(new-action-type {give in}
   :agent-type {animate}
   :english :verb)
   
(new-action-type {give over}
   :agent-type {animate}
   :object-type {thing}
   :english :verb)
   
(new-action-type {glaze over}
   :agent-type {physical object}
   :english :verb) 

; (new-action-type {gloss over}
   ; :agent-type {person}
   ; :object-type {thing}
   ; :english :verb)  

; (new-action-type {go on}
   ; :agent-type {thing}
   ; :english :verb)   
   
; (new-action-type {go off}
   ; :agent-type {physical object}
   ; :english :verb) 
   
; (new-action-type {go out}
   ; :agent-type {physical object}
   ; :english :verb) 

; (new-action-type {go back}
   ; :agent-type {physical object}
   ; :english :verb) 
   
(new-action-type {go through}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb) 

(new-action-type {gobble up}
   :agent-type {animate}
   :object-type {physical object}
   :english :verb) 

(new-action-type {grind up}
	:agent-type {physical object}
	:object-type {physical object}
	:english :verb)
	
; (new-action-type {grow up}
	; :agent-type {animate}
	; :english :verb)
	
(new-action-type {gulp down}
   :agent-type {animate}
   :object-type {physical object}
   :english :verb) 	

; (new-action-type {gun down}
   ; :agent-type {person}
   ; :object-type {person}
   ; :english :verb) 
   
(new-action-type {hack away}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb) 
   
(new-action-type {hail down}
   :agent-type {person}
   :object-type {physical object}
   :english :verb) 

(new-action-type {ham up}
   :agent-type {person}
   :english :verb) 

(new-action-type {hammer away}
   :agent-type {animate}
   :english :verb) 

(new-action-type {hammer out}
   :agent-type {group.n.01}
   :object-type {intangible}
   :english :verb) 

; (new-action-type {hand over}
   ; :agent-type {animate}
   ; :object-type {thing}
   ; :english :verb) 

(new-action-type {hand out}
   :agent-type {animate}
   :object-type {physical object}
   :english :verb) 

; (new-action-type {hang out}
   ; :agent-type {group.n.01}
   ; :english :verb) 

; (new-action-type {hang up}
   ; :agent-type {animate}
   ; :object-type {physical object}
   ; :english :verb) 

; (new-action-type {hash out}
   ; :agent-type {group.n.01}
   ; :object-type {intangible}
   ; :english :verb) 

(new-action-type {haul in}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb) 

(new-action-type {haul out}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb) 

(new-action-type {head off}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb)   
   
(new-action-type {head off (depart)}
   :agent-type {physical object}
   :english :verb) 
   
; (new-action-type {heat up}
   ; :agent-type {physical object}
   ; :object-type {physical object}
   ; :english :verb) 
   
; (new-action-type {heat up (intensify)}
   ; :agent-type {thing}
   ; :english :verb) 
   
; (new-action-type {help out}
   ; :agent-type {animate}
   ; :object-type {animate}
   ; :english :verb) 

(new-action-type {hit on}
   :agent-type {person}
   :object-type {person}
   :english :verb) 
   
(new-action-type {hit up}
   :agent-type {person}
   :object-type {person}
   :english :verb) 
   
; (new-action-type {hold on}
   ; :agent-type {physical object}
   ; :english :verb) 

(new-action-type {hold back}
   :agent-type {thing}
   :object-type {thing}
   :english :verb) 
   
(new-action-type {hold back (restrain self)}
   :agent-type {person}
   :english :verb) 

; (new-action-type {hold out}
   ; :agent-type {thing}
   ; :english :verb) 
   
; (new-action-type {hold out (offer)}
   ; :agent-type {person}
   ; :object-type {thing}
   ; :english :verb) 

; (new-action-type {hold off}
   ; :agent-type {animate}
   ; :object-type {physical object}
   ; :english :verb)

; (new-action-type {hold off (wait)}
   ; :agent-type {animate}
   ; :english :verb)   

; (new-action-type {hold up}
   ; :agent-type {thing}
   ; :object-type {thing}
   ; :english :verb) 
   
; (new-action-type {hold up (rob)}
   ; :agent-type {person}
   ; :object-type {thing}
   ; :english :verb) 
   
; (new-action-type {hold up (endure)}
   ; :agent-type {thing}
   ; :english :verb) 

; (new-action-type {hole up}
   ; :agent-type {animate}
   ; :english :verb)    
   
; (new-action-type {hook up}
   ; :agent-type {physical object}
   ; :object-type {physical object}
   ; :english :verb) 
   
; (new-action-type {hook up (have sex)}
   ; :agent-type {group.n.01}
   ; :english :verb) 
   
(new-action-type {hum along}
   :agent-type {person}
   :english :verb) 
   
; (new-action-type {iron out}
   ; :agent-type {group.n.01}
   ; :object-type {intangible}
   ; :english :verb) 
   
(new-action-type {jack up}
   :agent-type {thing}
   :object-type {thing}
   :english :verb) 

(new-action-type {jet off}
   :agent-type {physical object}
   :english :verb) 

(new-action-type {join in}
   :agent-type {thing}
   :english :verb) 
   
(new-action-type {join up}
   :agent-type {person}
   :english :verb)    

(new-action-type {joke around}
   :agent-type {person}
   :english :verb)     
   
(new-action-type {jot down}
   :agent-type {person}
   :object-type {written_communication.n.01}
   :english :verb)  

(new-action-type {jump in}
   :agent-type {animate}
   :english :verb)
   
(new-action-type {jump out}
   :agent-type {animate}
   :english :verb)
   
(new-action-type {jut out}
   :agent-type {physical object}
   :english :verb)   
   
; (new-action-type {keep up}
   ; :agent-type {thing}
   ; :english :verb)
 
; (new-action-type {keep up (maintain)}
   ; :agent-type {person}
   ; :object-type {physical object}
   ; :english :verb) 
   
(new-action-type {keep on}
   :agent-type {thing}
   :english :verb)
   
(new-action-type {keep on (retain)}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

; (new-action-type {kick in}
   ; :agent-type {person}
   ; :english :verb)   

(new-action-type {kick off}
   :agent-type {entity.n.01}
   :object-type {thing}
   :english :verb)
   
(new-action-type {kill off}
   :agent-type {person}
   :object-type {animate}
   :english :verb)   
   
(new-action-type {knock back}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb)   
   
(new-action-type {knock down}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb)

; (new-action-type {knock out}
   ; :agent-type {thing}
   ; :object-type {physical object}
   ; :english :verb)  

(new-action-type {knock off}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {lash out}
   :agent-type {animate}
   :english :verb)

(new-action-type {latch onto}
   :agent-type {physical object}
   :object-type {thing}
   :english :verb)

(new-action-type {lay on}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {lay off}
   :agent-type {person}
   :object-type {person}
   :english :verb)

(new-action-type {leave out}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {let on}
   :agent-type {person}
   :object-type {thing}
   :english :verb)   
   
(new-action-type {let up}
   :agent-type {thing}
   :english :verb)
   
(new-action-type {level off}
   :agent-type {thing}
   :english :verb)
   
(new-action-type {lick up}
   :agent-type {animate}
   :object-type {physical object}
   :english :verb)
   
; (new-action-type {lie down}
   ; :agent-type {animate}
   ; :english :verb)
   
; (new-action-type {light up}
   ; :agent-type {physical object}
   ; :object-type {thing}
   ; :english :verb)
   
; (new-action-type {light up (intransitive)}
   ; :agent-type {thing}
   ; :english :verb)

(new-action-type {lighten up}
   :agent-type {thing}
   :english :verb)

; (new-action-type {limber up}
   ; :agent-type {person}
   ; :english :verb)   
   
; (new-action-type {limber up (make flexible)}
   ; :agent-type {physical object}
   ; :object-type {physical object}
   ; :english :verb)
   
; (new-action-type {line up}
   ; :agent-type {group.n.01}
   ; :english :verb)
   
; (new-action-type {line up (set up)}
   ; :agent-type {person}
   ; :object-type {thing}
   ; :english :verb)
   
(new-action-type {link up}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb)
 
(new-action-type {link up (become joined)}
   :agent-type {group.n.01}
   :english :verb)
   
(new-action-type {live down}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {live on}
   :agent-type {thing}
   :english :verb)
   
(new-action-type {live on (transitive)}  ;ex: "you can't live on love alone"
   :agent-type {thing}
   :object-type {physical object}
   :english :verb)

(new-action-type {live it up}
   :agent-type {person}
   :english :verb)

; (new-action-type {live out}
   ; :agent-type {person}
   ; :english :verb)

(new-action-type {liven up}
   :agent-type {thing}
   :object-type {animate}
   :english :verb)   

(new-action-type {load up}
   :agent-type {person}
   :object-type {physical object}
   :english :verb)

(new-action-type {loan out}
   :agent-type {person}
   :object-type {physical object}
   :english :verb)

; (new-action-type {lock in}
   ; :agent-type {physical object}
   ; :object-type {thing}
   ; :english :verb)

; (new-action-type {lock up}
   ; :agent-type {physical object}
   ; :object-type {thing}
   ; :english :verb)

; (new-action-type {lock out}
   ; :agent-type {physical object}
   ; :object-type {physical object}
   ; :english :verb)

(new-action-type {lock down}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb)   

(new-action-type {log on}
   :agent-type {person}
   :english :verb)
   
; (new-action-type {look forward}
   ; :agent-type {person}
   ; :english :verb)
   
(new-action-type {look up}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {look up (physical action)}
   :agent-type {animate}
   :english :verb)
   
(new-action-type {look over}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

; (new-action-type {look after}
   ; :agent-type {animate}
   ; :object-type {thing}
   ; :english :verb)
   
; (new-action-type {look out}
   ; :agent-type {animate}
   ; :english :verb)
   
(new-action-type {loosen up}
   :agent-type {animate}
   :english :verb)
   
(new-action-type {loosen up (physical action)}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb)
   
(new-action-type {lose out}
   :agent-type {animate}
   :english :verb)
   
; (new-action-type {make up (compose or create)}
   ; :agent-type {thing}
   ; :object-type {thing}
   ; :english :verb)
   
; (new-action-type {make up (resolve conflict)}
   ; :agent-type {person}
   ; :english :verb)
   
; (new-action-type {make out (see)}
   ; :agent-type {animate}
   ; :object-type {physical object}
   ; :english :verb)

; (new-action-type {make out (kiss)}
   ; :agent-type {person}
   ; :english :verb)
   
(new-action-type {make it}
   :agent-type {person}
   :english :verb)

(new-action-type {make off}
   :agent-type {person}
   :english :verb)

; (new-action-type {mark down}
   ; :agent-type {person}
   ; :object-type {thing}
   ; :english :verb)   

; (new-action-type {mark up}
   ; :agent-type {person}
   ; :object-type {thing}
   ; :english :verb)
   
(new-action-type {mess up (make mistake)}
   :agent-type {person}
   :english :verb)
   
(new-action-type {mess up (transitive)}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)

(new-action-type {miss out}
   :agent-type {person}
   :english :verb)
   
(new-action-type {mix up}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)   

(new-action-type {monkey around}
   :agent-type {person}
   :english :verb)

(new-action-type {muck up}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)

; (new-action-type {nail down}
   ; :agent-type {person}
   ; :object-type {thing}
   ; :english :verb)

(new-action-type {neaten up}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {nod off}
   :agent-type {animate}
   :english :verb)

(new-action-type {oil up}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb)

; (new-action-type {open up (physical action)}
   ; :agent-type {physical object}
   ; :object-type {physical object}
   ; :english :verb)
   
; (new-action-type {open up (intransitive)}
   ; :agent-type {physical object}
   ; :english :verb)

(new-action-type {own up}
   :agent-type {person}
   :english :verb)   

(new-action-type {pack up}
   :agent-type {person}
   :object-type {physical object}
   :english :verb)
   
(new-action-type {pack up (intransitive)}
   :agent-type {person}
   :english :verb)
   
(new-action-type {pair up}
   :agent-type {person}
   :object-type {group.n.01}
   :english :verb)

(new-action-type {pair up (intransitive)}
   :agent-type {group.n.01}
   :english :verb)
   
(new-action-type {palm off}
   :agent-type {person}
   :object-type {thing}
   :english :verb)
   
; (new-action-type {pass away}
   ; :agent-type {animate}
   ; :english :verb)

(new-action-type {pass by}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)
   
; (new-action-type {pass off}
   ; :agent-type {person}
   ; :object-type {thing}
   ; :english :verb) 
   
(new-action-type {pass out}
   :agent-type {animate}
   :english :verb)
   
(new-action-type {pass out (distribute)}
   :agent-type {person}
   :object-type {physical object}
   :english :verb)   

(new-action-type {patch up}
   :agent-type {person}
   :object-type {physical object}
   :english :verb)
   
; (new-action-type {pay off}
   ; :agent-type {person}
   ; :object-type {debt.n.02}
   ; :english :verb)
  
   
(new-action-type {pay down}
   :agent-type {person}
   :object-type {thing}
   :english :verb)
   
; (new-action-type {peel off}
   ; :agent-type {physical object}
   ; :english :verb)
   
; (new-action-type {perk up}
   ; :agent-type {person}
   ; :english :verb)

; (new-action-type {perk up (transitive)}
   ; :agent-type {thing}
   ; :object-type {person}
   ; :english :verb)
   
; (new-action-type {pick off}
   ; :agent-type {person}
   ; :object-type {animate}
   ; :english :verb)
   
(new-action-type {pick on}
   :agent-type {person}
   :object-type {person}
   :english :verb)
   
(new-action-type {pick out}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

; (new-action-type {pile up (accumulate)}
   ; :agent-type {thing}
   ; :english :verb)
   
; (new-action-type {pile up (gather)}
   ; :agent-type {animate}
   ; :object-type {thing}
   ; :english :verb)
   
(new-action-type {pile on}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)   

; (new-action-type {pine away}
   ; :agent-type {animate}
   ; :english :verb)

(new-action-type {pipe down}
   :agent-type {person}
   :english :verb)

; (new-action-type {pipe up}
   ; :agent-type {person}
   ; :english :verb)

; (new-action-type {pitch in}
   ; :agent-type {person}
   ; :english :verb)  
   

(new-action-type {play down}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {play up}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {play into}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)

(new-action-type {play on}
   :agent-type {person}
   :object-type {thing}
   :english :verb)  

(new-action-type {play off}
   :agent-type {person}
   :object-type {person}
   :english :verb)

(new-action-type {play to}
   :agent-type {person}
   :object-type {thing}
   :english :verb)   
   
(new-action-type {plot out}
   :agent-type {person}
   :object-type {thing}
   :english :verb)
   
(new-action-type {plug up}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb)
   
; (new-action-type {plug in}
   ; :agent-type {person}
   ; :object-type {appliance.n.01}
   ; :english :verb)

(new-action-type {point out}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {poke around}
   :agent-type {person}
   :object-type {thing}
   :english :verb)
   
(new-action-type {poke around (act aimlessly)}
   :agent-type {person}
   :english :verb)

(new-action-type {polish off}
   :agent-type {animate}
   :object-type {physical object}
   :english :verb)

(new-action-type {polish up}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb)   

(new-action-type {pop up}
   :agent-type {thing}
   :english :verb)
   
(new-action-type {pound out}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb)

; (new-action-type {prop up}
   ; :agent-type {person}
   ; :object-type {physical object}
   ; :english :verb) 

; (new-action-type {puff up}
   ; :agent-type {physical object}
   ; :english :verb)
   
; (new-action-type {puff up (transitive)}
   ; :agent-type {physical object}
   ; :object-type {physical object}
   ; :english :verb)
   
(new-action-type {puke up}
   :agent-type {animate}
   :object-type {physical object}
   :english :verb)

; (new-action-type {pull out}
   ; :agent-type {physical object}
   ; :english :verb) 
   
(new-action-type {pull through}
   :agent-type {thing}
   :english :verb)

; (new-action-type {pull off (succeed)}
   ; :agent-type {person}
   ; :object-type {thing}
   ; :english :verb)

; (new-action-type {pull off (physical action)}
   ; :agent-type {physical object}
   ; :object-type {physical object}
   ; :english :verb)

; (new-action-type {pull over (intransitive)}
   ; :agent-type {vehicle.n.01}
   ; :english :verb) 
   
; (new-action-type {pull over}
   ; :agent-type {physical object}
   ; :object-type {vehicle.n.01}
   ; :english :verb) 
   
(new-action-type {pump out}
   :agent-type {physical object}
   :object-type {thing}
   :english :verb)   
   
(new-action-type {put off}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

; (new-action-type {quiet down}
   ; :agent-type {physical object}
   ; :english :verb)
   
; (new-action-type {rack up}
   ; :agent-type {person}
   ; :object-type {thing}
   ; :english :verb)
   
; (new-action-type {rake in}
   ; :agent-type {person}
   ; :object-type {money}
   ; :english :verb)

(new-action-type {ramp up}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)
   
(new-action-type {ramp up (intransitive)}
   :agent-type {thing}
   :english :verb)
   
(new-action-type {ratchet up}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)

(new-action-type {ratchet up (intransitive)}
   :agent-type {thing}
   :english :verb)
   
(new-action-type {ration out}
   :agent-type {person}
   :object-type {physical object}
   :english :verb)
   
(new-action-type {rattle on}
   :agent-type {person}
   :english :verb)   
   
(new-action-type {rattle off}
   :agent-type {person}
   :english :verb)
   
; (new-action-type {reel off}
   ; :agent-type {person}
   ; :english :verb)   
   
(new-action-type {rein in}
   :agent-type {person}
   :object-type {thing}
   :english :verb)
   
(new-action-type {rent out}
   :agent-type {person}
   :object-type {thing}
   :english :verb)   
   
(new-action-type {ride out}
   :agent-type {physical object}
   :object-type {thing}
   :english :verb)   
   
; (new-action-type {rig up}
   ; :agent-type {animate}
   ; :object-type {physical object}
   ; :english :verb)
   
; (new-action-type {ring up}
   ; :agent-type {person}
   ; :object-type {thing}
   ; :english :verb)   
   
; (new-action-type {rip out}
   ; :agent-type {animate}
   ; :object-type {physical object}
   ; :english :verb)

(new-action-type {rip off}
   :agent-type {person}
   :object-type {person}
   :english :verb)   
   
(new-action-type {rip up}
   :agent-type {person}
   :object-type {physical object}
   :english :verb)
   
; (new-action-type {roll out}
   ; :agent-type {physical object}
   ; :object-type {physical object}
   ; :english :verb)   
   
; (new-action-type {roll up (vehicle)}
   ; :agent-type {person}
   ; :english :verb)

; (new-action-type {roll up}
   ; :agent-type {physical object}
   ; :object-type {thing}
   ; :english :verb)    
   
(new-action-type {root out}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb)   
   
; (new-action-type {rough up}
   ; :agent-type {physical object}
   ; :object-type {physical object}
   ; :english :verb)
   
; (new-action-type {rough in}
   ; :agent-type {person}
   ; :object-type {thing}
   ; :english :verb)   
   
(new-action-type {pump out}
   :agent-type {physical object}
   :object-type {thing}
   :english :verb)   
   
; (new-action-type {round up}
   ; :agent-type {animate}
   ; :object-type {thing}
   ; :english :verb)     

(new-action-type {salt away}
   :agent-type {animate}
   :object-type {thing}
   :english :verb)   
   
(new-action-type {save up}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {saw up}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb)   
   
(new-action-type {scoop up}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb)

(new-action-type {scout out}
   :agent-type {animate}
   :object-type {thing}
   :english :verb)   
   
; (new-action-type {scratch out}
   ; :agent-type {physical object}
   ; :object-type {physical object}
   ; :english :verb)
   
(new-action-type {scrawl out}
   :agent-type {person}
   :object-type {thing}
   :english :verb)   
   
(new-action-type {screen out}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)
   
(new-action-type {scrub up}
   :agent-type {person}
   :english :verb)

(new-action-type {scrunch up}
   :agent-type {physical object}
   :object-type {physical object}
   :english :verb)   
   
(new-action-type {scrunch up (squat)}
   :agent-type {person}
   :english :verb)   
   
; (new-action-type {seek out}
   ; :agent-type {animate}
   ; :object-type {thing}
   ; :english :verb)

; (new-action-type {sell out}
   ; :agent-type {person}
   ; :english :verb)   
   
; (new-action-type {sell off}
   ; :agent-type {person}
   ; :object-type {thing}
   ; :english :verb)

(new-action-type {send out}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)   
   
(new-action-type {serve up}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

; (new-action-type {set off}
   ; :agent-type {physical object}
   ; :object-type {physical object}
   ; :english :verb)   
   
; (new-action-type {set off (journey)}
   ; :agent-type {animate}
   ; :english :verb)     
   
;;; From Thomson.

;; Doesn't really have a direct object, but strongly expects "set out to".
(new-action-type {set out}
   :agent-type {animal}
   :english :verb)

(new-action-type {set off}
   :agent-type {animal}
   :object-type {thing} ; actually {action} or {place}
   :english :verb)

(new-action-type {set up}
   :agent-type {tangible}
   :object-type {tangible} ; in a very literal sense
   :english :verb)

(new-action-type {set down}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

;; This is a bit tricky. Should there be a way to make note of the prisoners in "The guards set their dogs upon the prisoners"? (Maybe we need an indirect-object-type...)
(new-action-type {set upon}
   :agent-type {animal}
   :object-type {animal}
   :english :verb)

(new-action-type {settle down}
   :agent-type {thing}
   :object-type {animal} ; could also apply to a group of animals
   :english :verb)

(new-action-type {settle (oneself) down}
   :agent-type {tangible}
   :english '(:no-iname :verb "settle down"))

(new-action-type {sew up}
   :agent-type {human}
   :object-type {tangible}
   :english :verb)

(new-action-type {shack up}
   :agent-type {animal}
   :english :verb)

(new-action-type {shake off}
   :agent-type {tangible}
   :object-type {tangible} ; figurative usage is pretty common, though
   :english :verb)

(new-action-type {shake up}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

;; No direct object, but this strongly expects "shape up to be".
(new-action-type {shape up}
   :agent-type {thing}
   :english :verb)

(new-action-type {share out}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)

(new-action-type {shell out}
   :agent-type {legal person} ; not sure if "corporations shelling out money" should be considered figurative
   :object-type {tangible}
   :english :verb)

(new-action-type {shine through}
   :agent-type {light}
   :object-type {tangible}
   :english :verb)

(new-action-type {ship out}
   :agent-type {legal person} ; maybe should be just {person} if we want to be more literal
   :object-type {tangible}
   :english :verb)

;; In this case, I would argue that the intransitive usage is more common.
(new-action-type {shoot off (dash away)}
   :agent-type {animal}
   :english '(:no-iname :verb "shoot off"))

(new-action-type {shoot off}
   :agent-type {person}
   :object-type {tangible} ; could be {projectile} or something, I guess
   :english :verb)

(new-action-type {shoot down}
   :agent-type {tangible} ; animal or weapon
   :object-type {tangible} 
   :english :verb)

(new-action-type {shoot back}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

;; The more common meaning, though it's a figure of speech already.
(new-action-type {shoot back (reply)}
   :agent-type {person}
   :english '(:no-iname :verb "shoot back"))

(new-action-type {shore up}
   :agent-type {animal}
   :object-type {tangible}
   :english :verb)

;; Again, this one is already figurative.
(new-action-type {shout down}
   :agent-type {person}
   :object-type {person}
   :english :verb)

(new-type {sound} {intangible})
  (new-type {vocalization} {sound})
    (new-type {speech} {vocalization})
     (english {speech} :iname "utterance")
      (new-type {word (spoken)} {speech})
       (english {word (spoken)} :iname "word")
        (new-type {name} {word})
      (new-type {phrase} {speech})
       (english {phrase} :iname "sentence") ; sorry, grammarians

(new-action-type {shout out}
   :agent-type {animal}
   :object-type {vocalization}
   :english :verb)

(new-action-type {show up}
   :agent-type {thing}
   :english :verb)

;; There's a meaning of "show up" that takes an object, but I won't include it because it approaches slang.

(new-action-type {show off}
   :agent-type {animal}
   :object-type {thing}
   :english :verb)

(new-action-type {shrug off}
   :agent-type {person}
   :object-type {thing} ; won't be so literal as to restrict it to {tangible}
   :english :verb)

(new-action-type {shuffle off}
   :agent-type {animal}
   :english :verb)

(new-action-type {shut out}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {shut off}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {shut down}
   :agent-type {tangible}
   :object-type {machine}
   :english :verb)

(new-action-type {shut (oneself) down}
   :agent-type {machine}
   :english '(:no-iname :verb "shut down"))

(new-action-type {shut up}
   :agent-type {animal}
   :object-type {animal}
   :english :verb)

(new-action-type {shut (oneself) up}
   :agent-type {animal}
   :english '(:no-iname :verb "shut up"))

(new-action-type {shuttle off}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)

(new-action-type {shy away}
   :agent-type {person} ; arguably could be generalized to {animal}
   :english :verb)

(new-action-type {sign up}
   :agent-type {person}
   :english :verb)

(new-action-type {sign on}
   :agent-type {person}
   :english :verb)

(new-action-type {silt up}
   :agent-type {tangible} ; perhaps {aperture} or {opening}
   :english :verb)

(new-action-type {single out}
   :agent-type {legal person} ; this leans toward the figurative side
   :object-type {thing}
   :english :verb)

(new-action-type {siphon off}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {sit down}
   :agent-type {animal} ; obviously not all are capable of sitting...
   :english :verb)

(new-action-type {sit up}
   :agent-type {animal}
   :english :verb)

(new-action-type {sit in}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {size up}
   :agent-type {animal}
   :object-type {thing}
   :english :verb)

(new-action-type {sketch out}
   :agent-type {person}
   :object-type {tangible} ; more like {visible}
   :english :verb)

(new-action-type {skim off}
   :agent-type {animal}
   :object-type {tangible}
   :english :verb)

(new-action-type {skip off}
   :agent-type {animal}
   :english :verb)

(new-action-type {slack off}
   :agent-type {animal} ; {sentient with a task}?
   :english :verb)

(new-action-type {sleep away}
   :agent-type {animal}
   :object-type {intangible}
   :english :verb)

(new-action-type {sleep off}
   :agent-type {animal}
   :object-type {intangible}
   :english :verb)

(new-action-type {sleep over}
   :agent-type {animal}
   :english :verb)

(new-action-type {slice up}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {slim down}
   :agent-type {person}
   :object-type {tangible}
   :english :verb)

(new-action-type {slim (oneself) down}
   :agent-type {animal}
   :english '(:no-iname :verb "slim down"))

(new-action-type {slip in}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {slip (oneself) in}
   :agent-type {tangible}
   :english '(:no-iname :verb "slip in"))

(new-action-type {slow down}
   :agent-type {thing} ; admits figurative meanings
   :object-type {thing}
   :english :verb)

;; The only way I've heard this one is as "slug it out".
(new-action-type {slug out}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {smarten up}
   :agent-type {animal}
   :english :verb)

(new-action-type {smooth out}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {smooth over}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {snap off}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {snap up}
   :agent-type {animal}
   :object-type {tangible} ; usually {food}
   :english :verb)

(new-action-type {snatch away}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {sniff out} ; already an idiom
   :agent-type {animal}
   :object-type {tangible}
   :english :verb)

(new-action-type {soak up}
   :agent-type {tangible}
   :object-type {liquid}
   :english :verb)

(new-action-type {sober up}
   :agent-type {person} ; well, animals could be drunk, but...
   :english :verb)

(new-action-type {sock away}
   :agent-type {animal}
   :object-type {tangible}
   :english :verb)

(new-action-type {sort out}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {sound off}
   :agent-type {person} ; generally {soldier}
   :english :verb)

(new-action-type {speak up}
   :agent-type {person}
   :english :verb)

(new-action-type {speak out}
   :agent-type {person}
   :english :verb)

(new-action-type {speed up}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {spell out}
   :agent-type {tangible}
   :object-type {language_unit.n.01}
   :english :verb)

(new-action-type {spill over}
   :agent-type {tangible} ; actually {container} or {liquid}
   :english :verb)

(new-action-type {spill out}
   :agent-type {liquid}
   :english :verb)

(new-action-type {spin off}
   :agent-type {tangible}
   :english :verb)

(new-action-type {spring up}
   :agent-type {tangible}
   :english :verb)

(new-action-type {spruce up}
   :agent-type {person}
   :object-type {tangible}
   :english :verb)

(new-action-type {square off}
   :agent-type {animal}
   :english :verb)

(new-action-type {squeeze out}
   :agent-type {tangible}
   :object-type {tangible} ; {fluid} or {paste}, maybe
   :english :verb)

(new-action-type {stack up}
   :agent-type {animal}
   :object-type {tangible}
   :english :verb)

;; I'll omit the figurative definition of "stack up".

(new-action-type {stake out}
   :agent-type {animal}
   :object-type {place}
   :english :verb)

(new-action-type {stall out}
   :agent-type {potential agent}
   :object-type {thing}
   :english :verb)

(new-action-type {stall off}
   :agent-type {potential agent}
   :object-type {thing}
   :english :verb)

(new-action-type {rubber stamp}
   :agent-type {person}
   :object-type {document}
   :english :verb)

(new-action-type {stamp out}
   :agent-type {animal}
   :object-type {tangible}
   :english :verb)

(new-action-type {stand by}
   :agent-type {legal person} ; iffy
   :english :verb)

(new-action-type {stand out}
   :agent-type {thing}
   :english :verb)

(new-action-type {stand up}
   :agent-type {animal}
   :english :verb)

(new-action-type {stare down}
   :agent-type {animal}
   :object-type {animal}
   :english :verb)

(new-action-type {start off}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)

(new-action-type {start up}
   :agent-type {tangible}
   :object-type {tangible} ; debatable
   :english :verb)

(new-action-type {start out}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)

(new-action-type {stash away}
   :agent-type {animal}
   :object-type {tangible}
   :english :verb)

(new-action-type {stave off}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {stay on}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {steer clear}
   :agent-type {person}
   :english :verb)

(new-action-type {step in}
   :agent-type {animal}
   :english :verb)

(new-action-type {step up}
   :agent-type {animal}
   :english :verb)

;; I'm just going to ignore "step it up".

(new-action-type {step down}
   :agent-type {animal}
   :english :verb)

(new-action-type {stick out}
   :agent-type {tangible}
   :english :verb)

(new-action-type {stick (it) out}
   :agent-type {animal}
   :object-type {thing}
   :english :verb)

(new-action-type {stir up}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {stock up}
   :agent-type {animal}
   :english :verb)

(new-action-type {stop by}
   :agent-type {animal} ; or group (but not organization)
   :object-type {place}
   :english :verb)

(new-action-type {stop up}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {store up}
   :agent-type {thing}
   :object-type {thing} ; usually {tangible}
   :english :verb)

(new-action-type {straighten out}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {stretch out}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {strike out}
   :agent-type {person}
   :english :verb)

(new-action-type {strike down}
   :agent-type {tangible}
   :object-type {animal}
   :english :verb)

(new-action-type {strike up}
   :agent-type {animal}
   :object-type {thing} ; a {band} or a {fire}, usually
   :english :verb)

(new-action-type {strip away}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {study up}
   :agent-type {person}
   :english :verb)

(new-action-type {suck up}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {suit up}
   :agent-type {person}
   :english :verb)

(new-action-type {sum up}
   :agent-type {thing} ; person or adding machine
   :object-type {intangible} ; actually we would want a {set} of {number}s or other mathematical entities...
   :english :verb)

(new-action-type {sum up (reflexively)}
   :agent-type {intangible}
   :english '(:no-iname :verb "sum up"))

;; The imprecision in the previous two could actually be an issue, as something figurative like "the threats summed up..." could slip by if {intangible} were left there.

(new-action-type {summon forth}
   :agent-type {person}
   :object-type {thing} ; usually {animate}
   :english :verb)

(new-action-type {swear off}
   :agent-type {person}
   :object-type {thing} ; could be a tangible thing or a habit
   :english :verb)

(new-action-type {swear in}
   :agent-type {person}
   :object-type {person}
   :english :verb)

(new-action-type {sweat out}
   :agent-type {animal}
   :object-type {thing}
   :english :verb)

(new-action-type {sweat off}
   :agent-type {animal}
   :object-type {thing}
   :english :verb)

(new-action-type {sweep up}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {swoop up}
   :agent-type {tangible}
   :english :verb)

(new-action-type {tack down}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {tack on}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {take away}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {take in} 
   :agent-type {thing}
   :object-type {thing} ; the figurative meaning is very common
   :english :verb)

(new-action-type {take off}
   :agent-type {tangible} ; usually winged entities or the pilots thereof
   :english :verb)

(new-action-type {take off (remove)}
   :agent-type {tangible}
   :object-type {tangible}
   :english '(:no-iname :verb "take off"))

(new-action-type {take out}
   :agent-type {person}
   :object-type {tangible}
   :english :verb)

(new-action-type {take on}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {take out (in a fight)}
   :agent-type {animal}
   :object-type {animal} ; let's ignore the possibility of robot-battles...
   :english '(:no-iname :verb "take out"))

(new-action-type {take over}
   :agent-type {legal person}
   :object-type {thing}
   :english :verb)

(new-action-type {take up}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {take aback}
   :agent-type {thing}
   :object-type {animal}
   :english :verb)

(new-action-type {take down}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {tamp down}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {tangle up}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {taper off}
   :agent-type {thing}
   :english :verb)

(new-action-type {tax away} ; this is a pretty strange one...
   :agent-type {thing}
   :object-type {thing}
   :english :verb)

(new-action-type {team up}
   :agent-type {organization} ; more generally, {set} of {animal}s
   :english :verb)

(new-action-type {tear up}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {tear down}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {tease out}
   :agent-type {tangible}
   :object-type {tangible} ; the figurative usage is at least as common, though
   :english :verb)

(new-action-type {tee off}
   :agent-type {person}
   :english :verb)

(new-action-type {tell on}
   :agent-type {person}
   :object-type {person}
   :english :verb)

(new-action-type {thaw out (reflexively)}
   :agent-type {tangible}
   :english '(:no-iname :verb "thaw out"))

(new-action-type {thaw out}
   :agent-type {tangible} ; "springtime thawed out the plants" is conceivable, though
   :object-type {tangible}
   :english :verb)

(new-action-type {thin out}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {think up}
   :agent-type {person}
   :object-type {intangible}
   :english :verb)

(new-action-type {thrash out}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {throw away}
   :agent-type {animal}
   :object-type {tangible}
   :english :verb)

(new-action-type {throw in}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {throw out}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {throw up}
   :agent-type {animal}
   :object-type {tangible}
   :english :verb)

(new-action-type {tick off}
   :agent-type {thing}
   :object-type {animal}
   :english :verb)

(new-action-type {tidy up}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {tie in} ; figurative use is very common
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {tie up}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {tie down}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {tighten up}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {tip over}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {tip off}
   :agent-type {person}
   :object-type {person}
   :english :verb)

(new-action-type {tire out}
   :agent-type {animal}
   :english :verb)

(new-action-type {tone down}
   :agent-type {person}
   :object-type {intangible}
   :english :verb)

(new-action-type {top off}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {toss out}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {total up}
   :agent-type {person}
   :object-type {thing} ; usually a set
   :english '(:iname :verb "tote up"))

(new-action-type {total up (reflexively)}
   :agent-type {thing}
   :english '(:no-iname :verb "total up" "tote up"))

(new-action-type {totter around}
   :agent-type {animal}
   :object-type {place}
   :english :verb)

(new-action-type {touch off}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {touch up}
   :agent-type {animal}
   :object-type {tangible}
   :english :verb)

(new-action-type {touch on}
   :agent-type {thing} ; a speech or speaker
   :object-type {thing}
   :english :verb)

(new-action-type {touch upon}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)

(new-action-type {track down}
   :agent-type {animal}
   :object-type {tangible}
   :english :verb)

(new-action-type {trail off}
   :agent-type {thing} ; usually {intangible}
   :english :verb)

(new-action-type {trigger off}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)

(new-action-type {trim down}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {trot out}
   :agent-type {person}
   :object-type {tangible}
   :english :verb)

(new-action-type {try out}
   :agent-type {animal}
   :object-type {thing}
   :english :verb)

(new-action-type {tuck away}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {tuck in}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {tune in}
   :agent-type {tangible} ; {person} or {radio}, usually
   :object-type {frequency}
   :english :verb)

(new-action-type {tune out}
   :agent-type {person}
   :object-type {intangible}
   :english :verb)

(new-action-type {turn away}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {turn down}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {turn in}
   :agent-type {person}
   :object-type {person}
   :english :verb)

(new-action-type {turn on}
   :agent-type {tangible}
   :object-type {machine}
   :english :verb)

(new-action-type {turn off}
   :agent-type {tangible}
   :object-type {machine}
   :english :verb)

(new-action-type {turn out}
   :agent-type {thing}
   :english :verb)

(new-action-type {turn over}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {turn up (show up)}
   :agent-type {tangible}
   :english '(:no-iname :verb "turn up"))

(new-action-type {turn up (find)}
   :agent-type {animal}
   :object-type {tangible}
   :english '(:no-iname :verb "turn up"))

(new-action-type {type up}
   :agent-type {person}
   :object-type {document}
   :english :verb)

(new-action-type {use to}
   :agent-type {thing}
   :object-type {action}
   :english :verb)

(new-action-type {usher in}
   :agent-type {tangible} ; usually sentient
   :object-type {tangible}
   :english :verb)

(new-action-type {vomit up}
   :agent-type {animal}
   :object-type {tangible}
   :english :verb)

(new-action-type {vote down}
   :agent-type {organization}
   :object-type {intangible}
   :english :verb)

(new-action-type {wait out}
   :agent-type {animal}
   :object-type {thing}
   :english :verb)

(new-action-type {wake up (reflexively)}
   :agent-type {animal}
   :english '(:no-iname :verb "wake up"))

(new-action-type {wake up}
   :agent-type {thing}
   :object-type {animal}
   :english '(:no-iname :verb "wake up"))

(new-action-type {wall off}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {ward off}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)

(new-action-type {warm up}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {warm over}
   :agent-type {legal person}
   :object-type {thing} ; usually intangible
   :english :verb)

(new-action-type {wash up}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {watch out}
   :agent-type {animal}
   :english :verb)

(new-action-type {water down}
   :agent-type {tangible}
   :object-type {liquid}
   :english :verb)

(new-action-type {wear off}
   :agent-type {thing}
   :english :verb)

(new-action-type {wear out}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {wear on}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {wear down}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {weigh in}
   :agent-type {person}
   :english :verb)

(new-action-type {well up}
   :agent-type {liquid}
   :english :verb)

(new-action-type {whack off}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {while away}
   :agent-type {animal}
   :object-type {intangible}
   :english :verb)

(new-action-type {whip out}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {whip up}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {whittle down}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {win over}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)

(new-action-type {wind up}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {wind up (end up)}
   :agent-type {thing}
   :english '(:no-iname :verb "wind up"))

(new-action-type {wind down}
   :agent-type {tangible}
   :english :verb)

(new-action-type {wipe out}
   :agent-type {tangible}
   :object-type {tangible} ; taking things very literally
   :english :verb)

(new-action-type {wipe out (crash)}
   :agent-type {tangible}
   :english '(:no-iname :verb "wipe out"))

(new-action-type {wipe off}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {wipe up}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {wolf down}
   :agent-type {animal}
   :object-type {food}
   :english :verb)

(new-action-type {work out}
   :agent-type {thing} ; usually {situation}, {condition}, or other {intangible}
   :english :verb)

(new-action-type {work out (exercise)}
   :agent-type {animal}
   :object-type {thing}
   :english '(:no-iname :verb "work out"))

(new-action-type {work up}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)

(new-action-type {wrap up}
   :agent-type {tangible}
   :object-type {tangible} ;; again, taking this one literally
   :english :verb)

(new-action-type {wring out}
   :agent-type {tangible}
   :object-type {tangible}
   :english :verb)

(new-action-type {write off}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {write down}
   :agent-type {person}
   :object-type {writing.n.04}
   :english :verb)

(new-action-type {write in}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {write out}
   :agent-type {person}
   :object-type {writing.n.04}
   :english :verb)

(new-action-type {zero in}
   :agent-type {animal}
   :object-type {thing}
   :english :verb)

(new-action-type {zero out}
   :agent-type {thing}
   :object-type {thing}
   :english :verb) 

;;; Added by John later, with prepositions included.

(new-action-type {get even with}
   :agent-type {person}
   :object-type {person}
   :english :verb)
   
(new-action-type {give in to}
   :agent-type {person}
   :object-type {thing}
   :english :verb)
   
(new-action-type {go on about}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

; (new-action-type {go out (dating)}
   ; :agent-type {group.n.01}
   ; :english :verb)

(new-action-type {go out with}
   :agent-type {person}
   :object-type {person}
   :english :verb)
   
(new-action-type {hang up on}
   :agent-type {person}
   :object-type {person}
   :english :verb)
   
(new-action-type {hold off on}
   :agent-type {person}
   :object-type {thing}
   :english :verb)

(new-action-type {hum along with}
   :agent-type {person}
   :object-type {thing}
   :english :verb)   

(new-action-type {keep up with}
   :agent-type {physical object}
   :object-type {thing}
   :english :verb)  

(new-action-type {live up to}
   :agent-type {person}
   :object-type {thing}
   :english :verb)  

(new-action-type {load up on}
   :agent-type {person}
   :object-type {physical object}
   :english :verb)  
   
(new-action-type {look up to}
   :agent-type {person}
   :object-type {person}
   :english :verb)  
   
(new-action-type {pay out in}
   :agent-type {thing}
   :object-type {thing}
   :english :verb)  
   
(new-action-type {put off by}
   :agent-type {person}
   :object-type {thing}
   :english :verb)  
   
(new-action-type {put up with}
   :agent-type {person}
   :object-type {thing}
   :english :verb)  
   
(new-action-type {run out of}
   :agent-type {entity.n.01}
   :object-type {thing}
   :english :verb)  
   
(new-action-type {sell out of}
   :agent-type {entity.n.01}
   :object-type {thing}
   :english :verb)

(new-action-type {measure (verb)}
		 :agent-type {person}
		 :object-type {thing}
		 :english '(:no-iname :verb "measure"))


;;; ---------------------------------------------------------------------------
;;; Assorted knowledge added for METAL, not in any particular order.

;;; %%% When time allows, orgaized this stuff better and move a lot of it
;;; into the appropriate file in COzRE-COMPONENTS.

;; For some reason, "people" is not always being lemmatized to "person",
;; so add it as an English name.
(english {person} "people")

;; Some attributes that often show up in metaphors
(new-split-subtypes
 {physical attribute}
 '({density}
   {stability}
   {hardness}))

(new-split-subtypes
 {abstract attribute}
 '({liberty}
   {security}
   {status}
   ({economic status} :adj "economic")
   {intolerance}
   ({political power} "power")))

;; Some processes that show up in governance.

(new-type {mediation} {process})
(new-type {intervention} {process})
(new-type {development} {process})

;; Some subtypes of {condition}.

(new-type {wealth} {good economic condition})
(new-type {poverty} {bad economic condition})

;; {money} refers to the intangible measurable quality.
(new-is-a {measurable quality} {intangible})

;; New category of valuable stuff: money, wealth, riches...
(new-type {valuable stuff} {stuff})
(new-is-a {money} {valuable stuff})

;; This is physcial currency.
(new-type {currency} {physical object}
	  :english '("money" "cash"))

(new-is-a {currency} {valuable stuff})

(new-split-subtypes {material}
		    '({edible material}
		      {inedible material}))

(new-is-a {currency} {inedible material})

(new-is-a {physical object} {solid})

(new-type {food} {edible material})

(new-split-subtypes {food}
 '({pie} {meat} {bread} {garnish} {broth} {soup} {stew}))

(new-type {body part} {physical object})

(new-type-role {body part of animal}
	       {animal}
	       {body part})

(new-split-subtypes
 {body part}
 '({head} 
   ;; What we normally call your "body" is actually a part.
   {body}
   {arm}
   {leg}
   {muscle}
   {shoulder}
   {heart}
   {stomach}
   {eye}
   {ear}
   {brain}))

(new-is-a {body part} {naturally occurring})
(new-is-a {arm of person} {body part})
(new-is-a {head of animal} {body part})

;; The part of an animal is not the whole animal.
(new-split '({body part} {animal}))

(new-type {fight} {action})

(new-type {hyena} {mammal})

(new-type {proud} {person})

(new-type {ambitious} {person})

(new-split-subtypes {man-made}
		    '({architectural structure}
		      {organization structure}
		      {document}))

(new-split-subtypes {document}
		    '({constitution}
		      {blueprint}))

(new-type {bodily fluid} {natural material})

(new-split-subtypes {bodily fluid}
		    '({phlegm}
		      {saliva}
		      {blood}))

(new-split '({currency} {natural material}))

(new-type {highland} {geographical area})
(new-eq {highland} {mountainous})

;;; Not always getting lemmatized correctly.
(english {dollar} "dollars")

(new-type {zone} {geographical area})

(new-type {half-hearted} {figurative})

(new-type {governance} {process})

(new-indv {France} {country})

(new-indv {Afghanistan} {country}
	  :english "Afghan")

(new-indv {Greece} {country}
	  :english "Greek")

(new-type {rocky} {tangible}
	  :english '(:adj "rocky"))

(new-type {shaky} {tangible}
	  :english '(:adj "shaky"))

(new-type {budget} {information object})

(new-type {resolution} {information object})

(new-type {hollow} {physical object})

(new-type {position} {intangible})

(new-type {spineless} {animal})

(new-indv {Kardashian} {woman})

(new-type {grilled} {meat})

(new-split '({person} {meat}))

(new-split-subtypes {intangible}
		    '({visual phenomenon}
		      {mental phenomenon}))

(new-type {emotional} {person}
	  :english '(:adj "emotional"))

(new-type {landscape (scene)} {visual phenomenon}
	  :english '(:no-iname "landscape"))

(new-type {landscape (painting)} {man-made object}
	  :english '(:no-iname "landscape"))

(new-type {mystery} {mental phenomenon})

(new-type {engine} {man-made object})

(new-type {phone} {man-made object})

(new-type {attention} {intangible})

(new-indv {Facebook} {organization})

(new-type {flower} {plant}
	  :english "bloom")

(new-type {blanket} {man-made object})

(new-type {investment} {intangible})

(new-type {albatross} {bird})

(new-type {debt} {intangible})

(new-type {system} {intangible})

(new-type {festival} {event})

(new-type {sale} {event})

(new-is-a {event} {intangible})

(new-type {set (comedy)} {event}
	  :english '(:no-iname "set"))

(new-type {life} {process})

(new-type {competition} {activity})

(new-type {business} {activity})

(new-type {test} {event})

(new-split '({information object} {place}))

(new-type {equation} {information object})

(new-type {foundation} {organiztion})
		 
(new-type {container} {physical object})

;;; NOTE: This one may have unintended consquences.
(new-is-a {action} {intangbile})

(new-type {economy} {intangible})

(new-type {department} {organization})

(english {remove} :verb "take out")

(new-type {arena} {building})

(new-split-subtypes {process}
		    '({information process}
		      {physical process}))

(new-type {communication process} {information process})

(new-type {political process} {information process})

(new-split-subtypes {physical process}
		    '({disease process}
		      {chemical process}
		      {physical motion process}))

(new-type {political} {political process}
	  :english '(:adj "political"))

(new-is-a {time} {intangible})

(new-type {infection} {disease process})

(new-type {contagion} {disease process})

(new-type {democratic} {political process})

(new-type {lie (untruth)} {action}
	  :english '(:no-iname "lie"))

(new-type {clean} {tangible}
	  :english '(:adj "clean"))

(new-type {clear} {tangible}
	  :english '(:adj "clear"))

(new-type {pure} {tangible}
	  :english '(:adj "pure"))

(new-split '({place} {process}))

(new-type {quagmire} {wetland area})

(new-type {bureaucratic} {political process}
	  :english '(:adj "bureaucratic"))

(new-type {bureaucracy} {organization})

(new-split '({condition} {place}))

(new-type {crisis} {event})

(new-indv {Armenia} {country})

(new-type {subject} {intangible})

(new-type {field} {land area})

(new-type {loss} {event})

(new-type {death} {event})

(new-type {fire} {chemical process}
	  :english '("flame" "flames"))

(new-type {criticism} {communication process})

(new-type {mountain} {physical object})

(new-type {volcano} {mountain})

(new-type {scandal} {event})

(new-type {segment} {area})

(new-type {summer} {time-period})

(new-type {parliament} {organization})

(new-type {conscience} {information process})

(new-split '({organization} {process}))

(new-type {threat} {action})

(new-type {emotion} {condition})

(new-split-subtypes
 {emotion}
 '({fear} {anger} {happiness} {lust}))

(new-type {rigid} {physical object}
	  :english '(:adj "rigid"))

(new-type {hard} {physical object}
	  :english '(:adj "hard"))

(new-type {airline} {organization})

(new-type {rendering} {action})

(new-type {support} {relation})

(new-type {thesis} {intangible})

(new-type {ruble} {money})

(new-type {article} {text object})

(new-indv {Russia} {country})

(new-type {regime} {organization})

(new-type {society} {organization})

(new-indv {Tajikistan} {country})

(new-type {party (political)} {organization}
	  :english '(:no-iname "party"))

(new-type {party (event)} {event}
	  :english '(:no-iname "party"))

(new-type {light} {stuff})

(new-is-a {light} {intangible})

(new-type {news} {information})

(new-is-a {information} {intangible})

(new-type {peace} {condition})

(new-type {contest} {activity}
	  :english "competition")

(new-type {game} {contest})

(new-split-subtypes {game}
		    '({chess}
		      {poker}
		      {Monopoly (game)}
		      {bridge (game)}
		      {football}
		      {wrestling}
		      {boxing}))

(new-type {negotiation} {activity})

(new-type {commercial activity} {activity})
(new-type {trading} {commercial activity})
(new-type {bargaining} {trading})

(new-type {discussion} {negotiation})

(new-type {debate} {negotiation})

(new-type {battle} {combat})

(new-type {fighting} {combat})

(new-type {problem solving} {activity})

(new-type {market (activity)} {activity}
	  :english '(:no-iname "market"))

(new-is-a {activity} {intangible})

(new-type {score} {information})

(new-type {account} {information})

(new-type {count (number)} {information}
	  :english '(:no-iname "count"))

(new-indv {Moscow} {city})
(new-indv {Washington DC} {city})

(new-members {building}
	     '({White House}
	       {Buckingham Palace}
	       {Pentagon}
	       {Kremlin}))

(new-type {ring} {tangible})
(new-type {cirque} {tangible})

(english {lean on} :verb "lean")

(new-type {army} {organization})

(english {blossom} "bloom")

(new-type {meeting} {event})

(english {meeting} "assemblage")

(new-type {young} {physical object}
	  :english :adj)
(english {young} :adj "youthful" "kid")

(new-type {infectious disease} {disease process}
	  :english '("infection" "contagion"))

(english {pure} :adj "neat")

(new-split '({place} {organization}))

(new-split '({potential agent} {action}))

(english {die} :noun "death")

(new-type {speed} {physical quality}
	  :english '("velocity" "quickness"))

(new-type {stopper} {physical object}
	  :english '("plug" "cork"))
(new-is-a {stopper} {inanimate})

(new-type {framework} {tangible})

(new-type {scientific} {abstract quality}
	  :english :adj)

(new-type {machine} {man-made object})

(new-type {car} {machine}
	  :english '("automobile" "auto"))

(new-type {sports car} {car})

(new-split-subtypes {sports car}
		    '({Ferrari}
		      {Lamborghini}
		      {Corvette}))

(new-type {bulldozer} {machine})

(new-type {computer} {machine})

(new-type {government} {organization})

(new-type {legislative body} {organization})
(new-indv {Senate} {legislative body})
(new-indv {House of Representatives} {legislative body})
(new-is-a {Parliament} {legislative body})

(new-split-subtypes {time reference}
		    '({present time}
		      {past time}
		      {future time}))

(new-type {yesterday} {time interval})
(new-is-a {yesterday} {past time})
(new-type {today} {time interval})
(new-is-a {today} {present time})
(new-type {tomorrow} {time interval})
(new-is-a {tomorrow} {future time})

(new-type {inversion} {event})

(new-type {crack} {tangible})

(new-type {alliance} {organization})

(new-type {consultant} {person})

(new-type {sector} {area})

(new-split '({measurable quality} {action}))

(new-type {vehicle} {physical object})

(new-type {creation} {process})

;; "Administration" is either a process or a synonym for
;; "government".

(new-type {administration} {process})
(english {government} "administration")

(new-indv "U_N_A_M" {university})
(new-indv "P_R_D" {political party})

(new-indv "Chiapas" {state})

(new-type {production} {process})

(new-type {education} {process})

(new-type {explosive device}
	  {physical object})

(new-type {economic growth}
	  {process})

(new-type {plan} {information object})

(new-type {stone (material)} {natural material})
(english {stone (material)} "stone")
 
(new-type {stone (object)} {physical object})
(english {stone (object)} "stone")

(new-type {keystone} {stone (object)})

(new-type {cornerstone} {stone (object)})

(new-type {building} {physical object})

(new-indv-role {foundation (building)}
	       {building}
	       {physical object})
(english {foundation (building)} "foundation")

(new-type {intention} {intangible})

(new-type {democracy} {government})

(new-type {republic} {government})

(new-type {adolescent} {person})

(new-type {boat} {vehicle})

(new-indv {México} {country})

(new-type {tax} {intangible})

(new-type {criminal} {person})

(new-indv-role {jugular} {mammal} {physical object})

(new-type {habit} {action})

(new-type {preference} {intangible})

(new-type {war} {activity})

(new-split-subtypes {process}
		    '({natural process}
		      {artificial process}
		      {mental process}))

(new-split '({contest} {natural process}))
(new-is-a {mental process} {intangible})

(new-type {pulse} {natural process})

(new-type {interpretation} {information object})

(new-indv {Partido_Acción_Nacional_Sonora} {organization})

(new-type {decision} {mental process})

(new-indv {Amnistía} {organization})

(new-type {fame} {condition})

(new-split '({place} {condition}))

(new-type {obesity} {condition})

(new-type {world} {physical object})

(new-is-a {world} {inanimate})

(new-type {music} {intangible})

(new-type {bloated} {tangible}
	  :english :adj)

(new-type {adrift} {boat} :english :adj)

(english {elephant} "elephants")
(english {donkey} "donkeys")

(new-type {balloon} {physical object})

(new-type {payment} {event})

(new-type {underfunding} {event})

(new-type {law} {intangible})

(new-type {agenda} {information object})

(new-type {structured} {physical object})

(new-indv {SOPA} {law})

(new-type {planning} {activity})

(new-type {statement}
	  {information object})

(new-split-subtypes {say}
		    '({ask}
		      {deny}
		      {declare}
		      {proclaim}
		      {announce}))

(new-type {inequality} {condition})
(new-eq {inequality.n.01} {inequality})

;;; Dummy entities.
(new-indv {Senator Smith} {person})
(new-indv {Senator Jones} {person})
(new-indv {John Doe} {person})
(new-indv {Mary Doe} {person})

;;; ---------------------------------------------------------------------------
;;; Knowledge added for CM-patterns, mostly on "economic inequality".

;;; From John.

(new-split-subtypes
 {physical_condition.n.01}
 '({physical burden}
   {confinement}))

(new-type {low point} {thing})
(english {low point} "nadir")

(new-type {access to education} {intangible})

;;; ???
(new-members {measure}
	     '({vertical scale}
	       {horizontal scale}))
	
(new-type {socio-economic class} {intangible})
(new-type {economic class} {socio-economic class})
(new-type {social class} {socio-economic class})

(new-eq {confinement} {confinement.n.02})
(new-eq {machine} {machine.n.01})
(new-eq {fight} {fight.n.02})
(new-eq {game} {game.n.01})

(new-type {diploma} {education}) ; questionable, as a diploma is a physical object

(new-eq {academic degree} {academic_degree.n.01})
(english {academic degree} :iname "degree")
(english {master's degree} :iname "master's")
(new-type {PhD} {educational degree})

(new-type {item for access} {tangible})

(new-is-a {key.n.01} {item for access})
(new-is-a {ticket.n.01} {item for access})
(new-type {pass.n.01} {item for access}) ; not sure exactly what to call this one, as {pass} is taken
  (english {pass.n.01} "pass")


;;; ---------------------------------------------------------------------------
;;; From Thomson

(new-type {social equality} {good social condition})
(english {social equality} :iname "equality")

(new-type {social inequality} {bad social condition})
(english {social inequality} :iname "inequality" "social disparity")

(new-is-a {poverty} {bad economic condition})
(new-eq {wealth} {wealth.n.01})

(new-eq {emotion.n.01} {emotion})

(new-is-a {disease} {bad health condition})
(english {disease} :iname "sickness" "illness")
(new-is-a {health.n.01} {health condition})
(new-is-a {death.n.01} {health condition})

(new-type {demise} {death.n.01})

(new-type {physical distance} {intangible})

(new-is-a {gap.n.01} {physical distance})
(new-type {break (disconnect)} {physical distance})
  (english {break (disconnect)} "break"  "disconnect")
(new-is-a {span.n.02} {physical distance})
(new-type {spread.n.02} {physical distance})
  (english {spread.n.02} "spread")

(new-is-a {launch.v.02} {launch})
(new-is-a {throw.v.01} {launch})
(new-type {toss} {launch})
(new-is-a {throw out} {launch})
(new-is-a {toss out} {launch})

(new-type {worker} {person})
(new-eq {worker} {worker.n.01})

; (new-eq {economic growth} {economic_growth.n.01})
; once the funny split thing is resolved

(english {economic growth} :iname "growth")

(new-is-not-a {economic_growth.n.01} {tangible})
(new-eq {economic_growth.n.01} {economic growth})

(new-type {physically suppress} {suppress})

(new-is-a {choke.v.04} {physically suppress})
(new-is-a {strangle.v.01} {physically suppress})
(new-type {stifle} {physically suppress}) ; questionable, as "stifle" also has the figurative meaning
(new-type {suffocate} {physically suppress})
(new-is-a {smother.v.02} {physically suppress})

; I can only think of synonyms for "choke" right now... TODO

(new-eq {food} {food.n.01})

(new-type {fiscal} {thing}) ; I'll think later about how to elegantly deal with adjectives

(new-is-a {fiscal.a.01} {fiscal})
(new-is-a {monetary.a.01} {fiscal})
(new-is-a {economic.a.01} {fiscal})

(new-is-a {social class} {set})
(the-x-of-y-is-a-z {member} {social class} {person})

(new-members {social class}
 '({very wealthy (class)} {wealthy (class)} {upper middle class} {middle class} {lower middle class} {poor (class)} {very poor (class)}))
  (english {very wealthy (class)} :iname "the very wealthy" "very rich" "the very rich" "the richest")
  (english {wealthy (class)} :iname "the wealthy" "rich" "the rich")
  (english {upper middle class} :iname "upper-middle class")
  (english {middle class} :iname "middle-class" "working class")
  (english {lower middle class} :iname "lower-middle class")
  (english {poor (class)} :iname "the poor")
  (english {very poor (class)} :iname "the very poor" "the poorest")

(new-type {move in relation to} {move})

(new-type {move to} {move in relation to})
  (english {move to} :iname "move toward" "move closer to" "pull toward" "pull closer to")
(new-type {move from} {move in relation to})
  (english {move from} :iname "move away" "move away from" "move farther from" "pull away" "pull away from" "pull ahead" "pull ahead of" "distance" "distance from")

(new-type {socioeconomic} {thing})
  (english {socioeconomic} :iname "societal" "social" "economic")

(new-type {rigidity} {thing})
  (english {rigidity} :iname "difficulty of motion")

(new-is-a {distribution.n.03} {division.n.03})

(new-type {ail} {action})
(new-type {plague (verb)} {ail})
  (english {plague (verb)} "plague")

(new-is-a {leave_behind.v.01} {leave.v.01})

(new-type {gov-as-state} {thing}) ; kludgy, but the existing {government} and {state.n.01} don't have much overlap
  (english {gov-as-state} "government" "administration" "province" "nation" "state")
(new-is-a {government} {gov-as-state})
(new-is-a {state.n.01} {gov-as-state})
(new-is-a {state.n.04} {gov-as-state})

(new-type {standstill} {stagnation.n.01})

(new-type {draw from} {action})
  (english {draw from} :iname "extract from")

(new-is-a {tap.v.11} {draw from})
  (english {tap.v.11} "tap")

(new-type {competitor} {legal person})
(new-members {competitor}
 '({winner} {loser}))

;;; ---------------------------------------------------------------------------
;;; From Amos

(new-eq {poverty} {poverty.n.01})
(new-eq {education} {education.n.03})
(new-complete-split-subtypes {education}
  '({public education} {private education}))

(new-is-a {facility.n.01} {structure.n.01})

(new-is-a {condition.n.01} {condition})
(new-type {bodily condition} {condition})

(new-is-a {crippling.s.01} {bodily condition})
(new-type {mind-numbing} {bodily condition})
(new-type {paralyzing} {bodily condition})
(new-is-a {debilitating} {bodily condition})
(new-is-a {terminal.s.05} {bodily condition})

(new-is-a {deep.s.05} {intense.a.01})
(new-is-a {systemic.s.01} {intense.a.01})
(new-is-a {entrenched.s.02} {intense.a.01})
(new-is-a {structural.a.01} {intense.a.01})
(new-type {negatively intense} {intense.a.01})
(new-is-a {acute.s.02} {negatively intense})
(new-is-a {severe.s.01} {negatively intense})
(new-is-a {baneful.s.01} {negatively intense})

(new-eq {tax} {tax.n.01})

; equating this sense of "weight" and "burden" allows the metaphor
; "weight of taxation" to be detected just like "burden of taxation"
(new-is-a {weight.n.05} {burden.n.01})

; (new-type {economic abstraction} {abstraction.n.06})
(new-is-a {tax} {economic abstraction})
(new-is-a {poverty} {economic abstraction})
;(new-is-a {education} {economic abstraction})
; I think financial aid is an economic abstraction, because money
; is an abstraction, and money is related to economics
(new-is-a {aid.n.03} {economic abstraction})

(new-type {physical action} {action})
(new-is-a {attack.v.03} {physical action})
(new-is-a {defend.v.03} {physical action})
(new-is-a {strangle.v.03} {physical action})
(new-is-a {kill.v.01} {physical action})
; {scram.v.01} is a tyep of {leave.v.01}, but one of the words which
; are linked to {scram.v.01} is "get", which is not a physical action
; all the time (e.g. "getting money")
(new-is-a {leave.v.01} {physical action})
; I want to say "dilute" since this sense of dilute means to 
; "lessen the strength or flavor of a solution or mixture,"
; which of course is a physical action that cannot happen literally
; to things like poverty, because poverty is not a solution or
; mixture.
;
; However, the word "reduce" also goes to this meaning of "dilute."
; and we can very literally reduce taxes, albeit in a non-physical way,
; so I don't think we can this sense to {physical action}.
;
; The other sense of "dilute" is to corrupt or make something impure;
; I don't think anyone is trying to make misery impure, so that
; meaning of the metaphor is erroneous.
;
; I'm just going to say 'water down' in place of 'dilute'
;(new-is-a {dilute.v.01} {physical action})
(new-is-a {water_down.v.01} {physical action})

(new-type {Pell Grant} {grant.n.01})
(new-type {research grant} {grant.n.01})

; New elements for June 6, 2013

(new-is-a {structure.n.01} {physical_entity.n.01})

(new-type {hell hole} {place})

(new-is-a {press.v.01} {physical action})
(new-is-a {impede.v.01} {physical action})
(new-is-a {contend.v.06} {physical action})
(new-is-a {dampen.v.07} {physical action})
(new-is-a {soften.v.06} {physical action})
(new-is-a {harden.v.02} {physical action})
(new-is-a {destroy.v.01} {physical action})
(new-is-a {capture.v.06} {physical action})
; sense 1 of alleviate (providing physical relief) is in fact a
; physical action, but sense 2 (making something easier) is not,
; so I'm not going to make alleviate a physical action... even though
; that would make a lot of CM patterns easier

; +1 false positive "economy gets [better]", +0 true positives, so commenting out
;(new-is-a {economy.n.01} {economic abstraction})

(new-is-a {house_of_cards.n.01} {economic abstraction})
(new-is-a {asset.n.01} {economic abstraction})
(new-eq {fiscal} {fiscal.a.01})
(new-is-a {fiscal} {economic abstraction})


(new-is-a {wage.n.01} {income.n.01})

(new-eq {political} {political.a.01})
(new-is-a {political process} {abstraction.n.06})

(new-eq {healthy} {healthy.a.01})

(english {top.a.01} :iname "top")

; 1% often used as a synonym for rich people
(english {the rich} :iname "1%")

(english {house_of_cards.n.01} :iname "financial bubble")

(new-is-a {united_states.n.01} {country})

(new-is-a {dollar} {money.n.01})


(new-type {physical description} {thing})
(new-is-a {high.a.02} {physical description})
(new-is-a {large.a.01} {physical description})
(new-is-a {high.r.01} {physical description})
(english {high.a.02} :iname "higher")
(new-is-a {explosive.a.01} {physical description})
(new-is-a {strong.a.01} {physical description})

(new-type {economic inequality} {inequality.n.01})
(new-type {income inequality} {economic inequality})
(new-is-a {economic inequality} {economic abstraction})
(new-is-a {class.n.03} {economic abstraction})
(new-is-a {wealth.n.01} {economic abstraction})
(new-is-a {unemployment.n.01} {economic abstraction})

(new-is-a {disadvantage.n.01} {inequality.n.01})
(new-type {economic disadvantage} {disadvantage.n.01})
(new-is-a {economic disadvantage} {economic inequality})
(new-type {economic equality} {equality.n.01})
(new-is-a {economic equality} {economic abstraction})
(new-is-a {social equality} {equality.n.01})
(new-is-a {social inequality} {inequality.n.01})
; not making {social inequality} an {economic abstraction}...

(new-eq {attack} {attack.v.03})

(new-eq {defend} {defend.v.03})

(new-eq {strangle} {strangle.v.03})

(new-eq {kill} {kill.v.01})

(new-eq {leave} {leave.v.01})

(new-eq {water down} {water_down.v.01})

(new-eq {water down} {water_down.v.01})

(new-eq {exert force} {press.v.01})

(new-eq {obstruct} {impede.v.01})

(new-eq {fight with} {contend.v.06})

(new-eq {fight with} {contend.v.06})

(new-eq {lessen in force} {dampen.v.07})

(new-eq {soften} {soften.v.06})

(new-eq {harden} {harden.v.02})

(new-eq {destroy} {destroy.v.01})


(new-eq {academic degree} {educational degree})

(new-eq {wealth} {wealth.n.01})

(new-type {the poor} {thing})
(new-eq {the poor} {poor_people.n.01})

(new-is-a {the poor} {economic abstraction})

(new-type {the rich} {thing})
(english {the rich} :iname "wealthy") ; as in "the wealthy"
(new-eq {the rich} {rich_people.n.01})
(new-is-a {rich_people.n.01} {upper_class.n.01}) ; the upper class may perhaps also consist of destitute nobility...

; {the rich} is already an {economic abstraction} by being a socio-economic class
;(new-is-a {the rich} {economic abstraction})

;;; A bunch more verbs
(new-eq {collapse} {collapse.v.01})

(new-is-a {collapse.v.01} {physical action}) ; e.g. "the building collapsed"

(new-type {physical event} {event})
(new-is-a {collapse.n.02} {physical event})

; objects are not spontaneously chained to each other, I think...
(new-eq {chain} {chain.v.02})

; ... but I think it may be possible for them to be spontaneously connected, e.g. with magnets
(new-eq {connect} {connect.v.01})

(new-is-a {connect} {physical action})

(new-eq {move} {move.v.02})

;(new-is-a {move} {physical action})

(new-is-a {rise.n.02} {physical event})

(new-eq {enter} {enter.v.01})

; {get.v.27} is a type of {enter}, but "get" does not always refer to a physical action
; (e.g., "getting money")
(new-is-a {enter} {physical action})

(new-eq {build up} {build_up.v.04})

(new-is-a {build up} {physical action})

(new-eq {let go of} {let_go_of.v.01})

(new-is-a {let go of} {physical action})

(new-eq {travel} {travel.v.01})

(new-is-a {travel} {physical action})

;;; More stuff with education
(new-type {educational degree} {education})

(new-eq {master's degree} {master's_degree.n.01})
(english {bachelor's_degree.n.01} :iname "bachelor's")

(new-eq {PhD} {ph.d..n.01})
(new-eq {PhD} {doctor_of_philosophy.n.01})

(new-type {college education} {education})
(new-is-a {master's_degree.n.01} {college education})
(new-is-a {bachelor's_degree.n.01} {college education})
(new-is-a {PhD} {college education})

(new-is-a {cut_off.v.03} {physical action})


(new-is-a {income.n.01} {economic abstraction})
(new-is-a {occupation.n.01} {economic abstraction})
(new-is-a {corporation.n.01} {economic abstraction})
(new-is-a {money.n.01} {wealth.n.01})
(new-is-a {money.n.02} {economic abstraction})
(new-eq {economy} {economy.n.01})

(new-is-a {misery.n.02} {bad happiness condition})
; (new-is-a {shrink.v.03} {physical action})

;;; ---------------------------------------------------------------------------

;;; Spanish Vocabulary For Some Concepts Defined Here Or Already
;;; In Scone.

;;; %%% Check Whether Some Of These Are Redundant With Words Defined
;;; In Spanish Wordnet.  If So, See If We Need To Eq-Link Some Wordnet
;;; Meaning To The Native-Scone Meaning.

(spanish {stability} "estabilidad")		    
(spanish {liberty} "libertad")
(spanish {security} "seguridad")
(spanish {mediation} "mediación")
(spanish {economic status} "económico")
(spanish {eye} "ojo")
(spanish {head of animal} "cabeza")
(spanish {arm of person} "brazo")
(spanish {head} "cabeza")
(spanish {company} "compañía")
(spanish {organization leader} "líder")
(spanish {muscle} "músculo")
(spanish {intolerance} "intolerancia")
(spanish {inversion} "inversión" "inversiones")
(spanish {crack} "grieta" "grietas")
(spanish {consultant} "consultor")
(spanish {inversion} "inversión" "inversiones")
(spanish {brake (verb)} "frenar")
(spanish {shield (verb)} :verb "blindar")
(spanish {process} "proceso")
(spanish {arm} "brazo")
(spanish {brake (verb)} "frenar")
(spanish {clean (verb)} "limpiar")
(spanish {country} "país")
(spanish {pulverize} "pulverizar")
(spanish {education} "educación")
(spanish {production} "producción")
(spanish {family} "familia" )
(spanish {government}  "administración" "gobierno")
(spanish {administration} "administración")
(spanish {creation} "creación")
(spanish {cartel} "cártel")
(spanish {SOPA} "SOPA")
(spanish {hang (execute)} :verb "ahorcar")
(spanish {build} "construir")
(spanish {political party} "partido")
(spanish {enslave} "esclavizar")
(spanish {illuminate} :verb "iluminar")
(spanish {structured} :adj "estructurada")
(spanish {agenda} "agenda")
(spanish {glue (verb)} "colar")
(spanish {crisis} "crisis")
(spanish {weaken} "debilitar")
(spanish {law} "ley")
(spanish {confront} :verb "enfrentar")
(spanish {music} "música")
(spanish {embrace} "abrazar")
(spanish {consume} "consumar")
(spanish {world} "mundo")
(spanish {obesity} "obesidad")
(spanish {life} "vida")
(spanish {balance} "equilibrar")
(spanish {fame} "fama")
(spanish {jump to} "saltar a" "saltar")
(spanish {see} "ver")
(spanish {decision} "decisión")
(spanish {hit} "golpear")
(spanish {interpretation} "interpretación" "interpretaciones")
(spanish {awaken} "despertar")
(spanish {win} "ganar")
(spanish {pulse} "pulso")
(spanish {war} "guerra")
(spanish {pour} "verter")
(spanish {preference} "preferencia" "preferencias")
(spanish {habit} "hábito" "hábitos")
(spanish {break} "romper")
(spanish {jugular} "yugular")
(spanish {criminal} "criminales")
(spanish {multiply by} "multiplicación")
(spanish {tax} "impuesto")
(spanish {poverty} "pobreza")
(spanish {combat} "combatir")
(spanish {boat} "barco")
(spanish {adolescent} "adolescente")
(spanish {democracy} "democracia")
(spanish {intention} "intención" "intenciónes")
(spanish {undress} :verb "desnudar")
(spanish {keystone} "piedra angular")
(spanish {stone (object)} "piedra")
(spanish {stone (material)} "piedra")
(spanish {plan} "plan")
(spanish {economic growth} "crecimiento")
(spanish {detonate} "detonar")

(spanish {criminalize.v.01} "criminalizar")
(spanish {reproduction.n.01} "reproduccion")
(spanish {geography.n.01} "geografia")
(spanish {collapse.v.07} "desplomar")
(spanish {drown.v.04} "ahogarse")
(spanish {bet.v.02} "apostarle")
(spanish {cusp.n.01} "cuspide")
(spanish {touch.v.05} "tocarse")
(spanish {resurrection.n.02} "resureccion")
(spanish {thrill.v.01} "conmoverse")
(spanish {swell.v.03} "engrosar")
(spanish {rest.v.05} "descansar")
(spanish {locate.v.01} "ubicar")
(spanish {repent.v.01} "repuntar")
(spanish {linger.v.01} "rezagar")
(spanish {linger.v.01} "rezago")
(spanish {unemployment.n.01} "desempleo")
(spanish {misery.n.01} "miseria")
(spanish {burden.n.01} "lastre")

;;; ---------------------------------------------------------------------------

;;; Farsi vocabulary for some concepts defined here or already
;;; in Scone.

;;; %%% Check whether some of these are redundant with words defined
;;; in Farsi wordnet.   If so, see if we need to eq-link some Wordnet
;;; meaning to the native-Scone meaning.

(farsi {Iran} :noun "ايران")
(farsi {victims} :noun "قربانيان")
(farsi {victim} :noun "قرباني")
(farsi {direction} :noun "مسیر")
(farsi {shadow} :noun "سایه")
(farsi {disagreement} :noun "اختلاف")
(farsi {heaviness} "سنگینی")
(farsi {sanctions} "تحریم‌ها")
(farsi {sanction} "تحریم‌")
(farsi {stress} :noun "تنش")
(farsi {hand} "دست")
(farsi {political power} "قدرت")
(farsi {intention} "عزم")
(farsi {society} "جامعه")
(farsi {food} "خوراک")
(farsi {media} "خبری")
(farsi {wheel} :noun "چرخه")
(farsi {economic} :adj "اقتصادی")
(farsi {country} :noun "کشور")
(farsi {blossom} "شکوفه")
(farsi {revolution} "انقلاب")
(farsi {heart} "قلب")
(farsi {Pakistan} "پاکستان")
(farsi {injection} "تزریق")
(farsi {money} "پول")
(farsi {impeachment} :noun "استيضاح")
(farsi {pocket} :noun "جیب")
(farsi {government} :noun "دولت")
(farsi {framework} :noun "چهارچوب")
(farsi {scientific} :adj "علمی")

