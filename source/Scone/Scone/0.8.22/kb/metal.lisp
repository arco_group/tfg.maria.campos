;;; -*- Mode:Lisp -*-
;;; ***************************************************************************
;;; Knowledge specific to the METAL (IARPA METAPHOR) Project.
;;;
;;; Author & Maintainer: Scott E. Fahlman
;;; ***************************************************************************
;;; Copyright (C) 2012, Scott E. Fahlman
;;; 
;;; The Scone software is made available to the public under the Apache
;;; License, Version 2.0 (the "License"); you may not use the Scone
;;; software except in compliance with the License.  You may obtain a copy
;;; of the License at
;;; 
;;;   http://www.apache.org/licenses/LICENSE-2.0
;;; 
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
;;; implied.  See the License for the specific language governing
;;; permissions and limitations under the License.
;;; 
;;; Development of Scone since January, 2012, has been supported in part
;;; the Intelligence Advanced Research Projects Activity (IARPA) via
;;; Department of Defense US Army Research Laboratory contract number
;;; W911NF-12-C-0020.  The U.S. Government is authorized to reproduce and
;;; distribute reprints for Governmental purposes notwithstanding any
;;; copyright annotation thereon. Disclaimer: The views and conclusions
;;; contained herein are those of the authors and should not be
;;; interpreted as necessarily representing the official policies or
;;; endorsements, either expressed or implied, of IARPA, DoD/ARL, or the
;;; U.S. Government, or any of our other sponsors.
;;; 
;;; Development of Scone from 2003 through 2008 was supported in part by
;;; the Defense Advanced Research Projects Agency (DARPA) under contract
;;; numbers NBCHD030010 and FA8750-07-D-0185.
;;; 
;;; Additional support for Scone development has been provided by generous
;;; research grants from Cisco Systems Inc. and from Google Inc.
;;; 
;;; Scone may incoporate some fragements of the NETL2 system, developed by
;;; Scott E.  Fahlman for IBM Coporation between June 2001 and May 2003.
;;; IBM holds the copyright on NETL2 and has made that software available
;;; to the author under the Apache 2.0 Open Source license.
;;; ***************************************************************************

;;; Load the "core" knowledge base if not already present.
(unless (lookup-element {person})
  (load-kb "core"))

;;; Load the metal-specific code.
(load (merge-pathnames "metal-components/metal-code.fasl"
                       *default-kb-pathname*))
(format t "~&Loaded metal-code.~%")

;;; Load the WordNet-derived knowledge and lexical items for various
;;; languages.
(load-kb "lexical-components/wordnet-concepts")
(load-kb "lexical-components/wordnet-map")
(load-kb "lexical-components/wordnet-english-names")
(load-kb "lexical-components/wordnet-spanish-names")
(load-kb "lexical-components/wordnet-farsi-names")
; (load-kb "lexical-components/wordnet-russian-names")

;;; Some addtional knowledge sources.  They don't seem to help at
;; present, but may get into the game as we add other things.
(load-kb "lexical-components/antonym-splits")
; (load-kb "lexical-components/james-adjs")
; (load-kb "lexical-components/james-verbs")

;;; Load the metal-specific KB.
(load-kb "metal-components/metal-kb")
(load-kb "metal-components/cm-patterns")

;;; Allocate a marker and put it on all CM-patterns for the duration.
(setq *cm-pattern-marker* (get-marker))
(downscan *cm-pattern* *cm-pattern-marker*)
;;; The {CM pattern} node itself doesn't get marked.
(unmark *cm-pattern* *cm-pattern-marker*)


