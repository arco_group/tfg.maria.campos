#-*- coding:utf-8; -*-

(new-type-role {handlesBackendDevice} {eventManager} {backendDevice})
(new-type-role {hasAbility} {actor} {abilityDomain})
(new-type-role {hasAbilityDomain} {ability} {abilityDomain})
(new-type-role {hasActivity} {actor} {activity})
(new-type-role {hasActivityPriority} {activity} {priority})
(new-type-role {hasActorID} {actor} {uniqueIdentifier})
(new-type-role {hasActuatorCommand} {actuator} {command})
(new-type-role {hasAssociatedRawEvent} {alertEvent} {rawEvent})

(new-type-role {hasBasicInformation} {place} {IDN})
(new-is-a {hasBasicInformation} {activity})
(new-is-a {hasBasicInformation} {location})
(new-is-a {hasBasicInformation} {deviceType})
(new-is-a {hasBasicInformation} {abilityDomain})
(new-is-a {hasBasicInformation} {outputType})
(new-is-a {hasBasicInformation} {team})
(new-is-a {hasBasicInformation} {deviceClass})
(new-is-a {hasBasicInformation} {item})
(new-is-a {hasBasicInformation} {role})
(new-is-a {hasBasicInformation} {command})
(new-is-a {hasBasicInformation} {deviceStatus})
(new-is-a {hasBasicInformation} {multimediaDomain})
(new-is-a {hasBasicInformation} {ability})
(new-is-a {hasBasicInformation} {inputType})

(new-type-role {hasCodec} {multimediaCapability} {codec})

(new-complete-split {sofiaDevice} '({sofiaWP3MiddlewareServer} {device}))
(new-type-role {hasCommandDestination} {command} {sofiaDevice})

(new-type-role {hasCommandSource} {command} {sofiaWP3ApplicationClient})
(new-type-role {hasConnectionProtocol} {connectivityCapability} {protocol})
(new-type-role {hasContactInformation} {person} {contact})
(new-type-role {hasDeviceCapability} {device} {capability})
(new-type-role {hasDeviceClass} {device} {deviceClass})
(new-type-role {hasDeviceLocation} {device} {place})

(new-type-role {hasDeviceOwner} {device} {actorTypes})
(new-is-a {hasDeviceOwner} {actor})

(new-type-role {hasDevicePlatform} {device} {deviceInformation})
(new-type-role {hasDeviceStatus} {device} {deviceStatus})
(new-type-role {hasDeviceType} {device} {deviceType})
(new-type-role {hasFileFormat} {multimediaCapability} {fileFormat})
(new-type-role {hasInputType} {inputCapability} {inputType})
(new-type-role {hasItemLocation} {item} {place})
(new-type-role {hasItemNotes} {item} {notes})

(new-type-role {hasModel} {map} {thing})
(new-type-role {hasMovementRange} {motionCapability} {range})
(new-type-role {hasMultimediaDomain} {multimediaCapability} {multimediaDomain})
(new-type-role {hasNoteAuthor} {note} {actor})
(new-type-role {hasOutputType} {outputCapability} {outputType})
(new-type-role {hasPathEnd} {path} {place})
(new-type-role {hasPathStart} {path} {step})
(new-type-role {hasPathUID} {path} {uniqueIdentifier})


(new-union-type {locationAddress}  {thing} '({location} {address}))
(new-type-role {hasPlaceLocation} {place} {locationAddress})

(new-union-type {pathMapa} {thing} '({path} {map}))
(new-type-role {hasReferencePoint} {relativeCoordinates} {pathMap})

(new-type-role {hasRemoteType} {remote} {remoteType})
(new-type-role {hasStepPlace} {step} {place})
(new-type-role {hasTeamMember} {team} {actor})
(new-type-role {hasTeamMission} {team} {activity})

(new-type-role {hasThreshold} {range} {threshold})
(new-type-role {hasUID} {IDN} {uniqueIdentifier})
(new-type-role {isAssociatedToEvent} {compositeEvent} {rawEvent})
(new-type-role {isAssociatedToLocation} {event} {place})
(new-type-role {isAssociatedToPerson} {actor} {person})
(new-type-role {isContactOf} {contact} {person})

(new-type-role {isDetectedBy} {event} {deviceSofia})

(new-type-role {isLocatedIn} {place} {location})
(new-type-role {isMemberOf} {actor} {team})
(new-type-role {isNextStep} {step} {step})
(new-type-role {isOwnedBy} {personalUserDevice} {person})

(new-type-role {isPartOfSystem} {sofiaWP3ApplicationServer} {sofiaWP3System})
;(new-is-a {hasBasicInformation} {device})
(new-is-a {hasBasicInformation} {sofiaWP3ApplicationClient})

(new-type-role {isRunningOnUserDevice} {sofiaWP3ApplicationClient} {userDevice})
(new-type-role {isUsedBy} {userDevice} {person})
(new-type-role {missionOf} {activity} {team})
(new-type-role {playsRole} {actor} {role})

(new-type-role {queriesFor} {thing} {synchronousEvent})

(new-type-role {subscribesFor} {persistentQuery} {event})
