#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import Ice
import IceGrid

from civitas_slice_clt import ServicePrx, Thing, TypeCode



class Client(Ice.Application):
    def get_query(self):
        proxy = self.communicator().stringToProxy('IceGrid/Query')
        return IceGrid.QueryPrx.checkedCast(proxy)

    def run(self, argv):
        query = self.get_query()
        if not query:
            raise RuntimeError('Invalid proxy')

        # get object by identity
        proxy = query.findObjectById(self.communicator().stringToIdentity('GetNextCameraService'))
        if not proxy:
            raise RuntimeError('Object not found')

        theThingDict = {}
        t = Thing()
        t.type = TypeCode.stringT
        t.value = "Ronda de Calatrava"
        theThingDict["StreetName"] = t

        lat = Thing()
        lat.type = TypeCode.stringT
        lat.value = "38.992067"
        theThingDict["latitude"] = lat

        lon = Thing()
        lon.type = TypeCode.stringT
        lon.value = "-3.922270"
        theThingDict["longitude"] = lon


        # t = Thing()
        # t.type = TypeCode.stringT
        # t.value = "Calle Calatrava"
        # theThingDict["StreetName"] = t

        # lat = Thing()
        # lat.type = TypeCode.stringT
        # lat.value = "38.991541"
        # theThingDict["latitude"] = lat

        # lon = Thing()
        # lon.type = TypeCode.stringT
        # lon.value = "-3.923107"
        # theThingDict["longitude"] = lon



        # t = Thing()
        # t.type = TypeCode.stringT
        # t.value = "Calle Estrella"
        # theThingDict["StreetName"] = t
        # lat = Thing()
        # lat.type = TypeCode.stringT

        # lat.value = "38.991293"
        # theThingDict["latitude"] = lat

        # lon = Thing()
        # lon.type = TypeCode.stringT
        # lon.value = "-3.923439"
        # theThingDict["longitude"] = lon



        # t = Thing()
        # t.type = TypeCode.stringT
        # t.value = "Calle San Antonio"
        # theThingDict["StreetName"] = t
        # lat = Thing()
        # lat.type = TypeCode.stringT

        # lat.value = "38.990849"
        # theThingDict["latitude"] = lat

        # lon = Thing()
        # lon.type = TypeCode.stringT
        # lon.value = "-3.924759"
        # theThingDict["longitude"] = lon


        service = ServicePrx.checkedCast(proxy)
                
        streets = []
        service.performAction('getNextCamera', theThingDict, None)
        sys.stdout.flush()

        return 0


sys.exit(Client().main(sys.argv))
