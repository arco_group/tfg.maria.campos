#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
import Ice
import IceGrid
from civitas_slice import SconeServicePrx




class client(Ice.Application):
    def get_query(self):
        proxy = self.communicator().stringToProxy('IceGrid/Query')
        return IceGrid.QueryPrx.checkedCast(proxy)

    def run(self, argv):
        query = self.get_query()
        if not query:
            raise RuntimeError('Invalid proxy')

        # get object by identity
        proxy = query.findObjectById(self.communicator().stringToIdentity('sconeService'))



        if not proxy:
            raise RuntimeError('Object not found')

        scone = SconeServicePrx.uncheckedCast(proxy)
        #cad = " (incoming-parent-elements  (lookup-element {camera}))\n"
        cad = "(the-x-of-y {proxy} (car (incoming-parent-elements  (lookup-element {camera}))))\n"
        str = scone.sconeRequest(cad)
        print(str)
        sys.stdout.flush()


        return 0


sys.exit(client().main(sys.argv))
